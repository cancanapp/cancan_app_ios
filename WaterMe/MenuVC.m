//
//  MenuVC.m
//  CanCan
//
//  Created by AC on 21/08/15.
//  Copyright (c) 2015 CanCan Pvt. Ltd.,. All rights reserved.
//

#import "MenuVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "SimpleLocalityVC.h"
#import "LoginVC.h"
#import "OrderHistoryVC.h"
#import "SWRevealViewController.h"
#import "iRate.h"
#import "AFNHelper.h"
#import "AboutUsVC.h"
#import <CleverTapSDK/CleverTap.h>
#import "Branch.h"
#import "ShareVC.h"

@interface MenuVC ()

@property (strong, nonatomic) IBOutlet UIButton *btnPhone;
@property (strong, nonatomic) IBOutlet UIButton *btnLocality;
@property (strong, nonatomic) IBOutlet UIButton *btnMyOrders;
@property (strong, nonatomic) IBOutlet UIButton *btnNotifications;
@property (strong, nonatomic) IBOutlet UIButton *btnAboutUs;
@property (strong, nonatomic) IBOutlet UIButton *btnHelp;
@property (strong, nonatomic) IBOutlet UIButton *btnRate;
@property (strong, nonatomic) IBOutlet UIButton *btnShare;
@property (strong, nonatomic) IBOutlet UIImageView *shareImgVw;
@property (strong, nonatomic) IBOutlet UIButton *btnLogOut;

@property (strong, nonatomic) IBOutlet UIImageView *phoneImgVw;
@property (strong, nonatomic) IBOutlet UIImageView *logoutImgVw;
@property (strong, nonatomic) IBOutlet UIImageView *myOrdersImgVw;
@property (strong, nonatomic) IBOutlet UIImageView *notifsImgVw;

@property (strong, nonatomic) IBOutlet UIScrollView *menuSclVw;

@end

@implementation MenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self customLayout];
    
    if (self.view.bounds.size.height < self.menuSclVw.contentSize.height) {
        [self.menuSclVw setScrollEnabled:YES];
    }
    else {
        [self.menuSclVw setScrollEnabled:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)customLayout {
    if ([USER_DEFAULTS objectForKey:PARAM_PHONE_NUM]) {
        [self.btnPhone setTitle:[NSString stringWithFormat:@"+91 %@",[USER_DEFAULTS objectForKey:PARAM_PHONE_NUM]] forState:UIControlStateNormal];
        [self.btnPhone setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        
        [self.btnPhone setUserInteractionEnabled:NO];
        [self.phoneImgVw setHidden:NO];
    }
    
    if ([USER_DEFAULTS objectForKey:PARAM_LOCALITY]) {
        [self.btnLocality setTitle:[[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_LOCALITY]] capitalizedString] forState:UIControlStateNormal];
    }
    
    if (![USER_DEFAULTS objectForKey:PARAM_USER_TOKEN]) {
        [self.btnPhone setTitle:@"Login" forState:UIControlStateNormal];
        [self.btnPhone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.btnPhone setUserInteractionEnabled:YES];
        [self.phoneImgVw setHidden:YES];
        
        [self.btnMyOrders setAlpha:0.5f];
        [self.myOrdersImgVw setAlpha:0.5f];
        [self.btnMyOrders setUserInteractionEnabled:NO];
        
        [self.btnNotifications setAlpha:0.5f];
        [self.notifsImgVw setAlpha:0.5f];
        [self.btnNotifications setUserInteractionEnabled:NO];

        [self.btnShare setAlpha:0.5f];
        [self.shareImgVw setAlpha:0.5f];
        [self.btnShare setUserInteractionEnabled:NO];
        
        [self.btnLogOut setHidden:YES];
        [self.logoutImgVw setHidden:YES];
    }
    else {
        [self.btnMyOrders setAlpha:1.0f];
        [self.myOrdersImgVw setAlpha:1.0f];
        [self.btnMyOrders setUserInteractionEnabled:YES];
        
        [self.btnNotifications setAlpha:1.0f];
        [self.notifsImgVw setAlpha:1.0f];
        [self.btnNotifications setUserInteractionEnabled:YES];
        
        [self.btnShare setAlpha:1.0f];
        [self.shareImgVw setAlpha:1.0f];
        [self.btnShare setUserInteractionEnabled:YES];

        [self.btnLogOut setHidden:NO];
        [self.logoutImgVw setHidden:NO];
    }

    self.btnPhone.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.btnPhone.contentEdgeInsets = UIEdgeInsetsMake(0, 40, 0, 0);
    
    self.btnLocality.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.btnLocality.contentEdgeInsets = UIEdgeInsetsMake(0, 40, 0, 0);
    
    self.btnMyOrders.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.btnMyOrders.contentEdgeInsets = UIEdgeInsetsMake(0, 40, 0, 0);
    
    self.btnNotifications.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.btnNotifications.contentEdgeInsets = UIEdgeInsetsMake(0, 40, 0, 0);
    
    self.btnAboutUs.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.btnAboutUs.contentEdgeInsets = UIEdgeInsetsMake(0, 40, 0, 0);
    
    self.btnHelp.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.btnHelp.contentEdgeInsets = UIEdgeInsetsMake(0, 40, 0, 0);
    
    self.btnRate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.btnRate.contentEdgeInsets = UIEdgeInsetsMake(0, 40, 0, 0);
    
    self.btnShare.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.btnShare.contentEdgeInsets = UIEdgeInsetsMake(0, 40, 0, 0);
    
    self.btnLogOut.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.btnLogOut.contentEdgeInsets = UIEdgeInsetsMake(0, 40, 0, 0);
    
    // Create Share Link
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"article_id"] = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_USER_ID]];
    params[@"$og_title"] = @"Hey Download the CanCan App!!!";
    //params[@"$og_image_url"] = @"http://yoursite.com/pics/987666.png";
    params[@"$desktop_url"] = @"http://onelink.to/cancan";
    
    [[Branch getInstance] getShortURLWithParams:params andChannel:@"sms" andFeature:BRANCH_FEATURE_TAG_SHARE andCallback:^(NSString *url, NSError *error) {
        if (!error) //NSLog(@"got my Branch link to share: %@", url);
        
        [USER_DEFAULTS setObject:url forKey:@"shareUrl"];
    }];
    
    // Get Credits
    [[Branch getInstance] loadRewardsWithCallback:^(BOOL changed, NSError *err) {
        if (!err) {
            //NSLog(@"credit: %lu", (long)[[Branch getInstance] getCredits]);
        }
    }];
    
    //BranchReferralController *referralController = [BranchReferralController branchReferralControllerWithDelegate:self];

}

- (IBAction)changeLocalityEvent:(id)sender {
    //NSLog(@"%@",self.navigationController.viewControllers);
    
    // CleverTap - Change Locality
    NSDictionary *props = @{@"Current Locality":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_LOCALITY]]};
    [[CleverTap push] eventName:@"Change Locality" eventProps:props];

    [USER_DEFAULTS setBool:YES forKey:@"isChangeLocality"];
    [USER_DEFAULTS setObject:@"" forKey:PARAM_CITY];
    [USER_DEFAULTS setObject:@"" forKey:PARAM_LOCALITY];
    [USER_DEFAULTS synchronize];
    
//    if ([USER_DEFAULTS objectForKey:@"isLogin"]) {
        UIViewController *nextWindow = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SimpleLocalityVC"];
        [self.navigationController setViewControllers:[NSArray arrayWithObject:nextWindow] animated:YES];
//    }
//    else {
//        [self.navigationController popToViewController:[self.navigationController.viewControllers firstObject] animated:YES];
//    }
}

-(void)showSameVC {
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
    
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"CanCan" message:@"YES !!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
    
//    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    LoginVC *loginView = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
//    [self presentViewController:loginView
//                       animated:YES
//                     completion:nil];

}

- (IBAction)shouldPromptForRating:(id)sender {
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];

    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"How's the app for you?" message:@"Rate & Write us a review on App Store" delegate:self cancelButtonTitle:@"No Thanks" otherButtonTitles:@"Rate Now", @"Remind Me Later", nil];
    alertView.tag = 0;
    
    [alertView show];
    
    // CleverTap - Rate Us
    [[CleverTap push] eventName:@"Rate Us"];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 0) {
        if (buttonIndex == alertView.cancelButtonIndex) { //ignore this version
            [iRate sharedInstance].declinedThisVersion = YES;
        }
        else if (buttonIndex == 1) {  // rate now
            //mark as rated
            [iRate sharedInstance].ratedThisVersion = YES;
            
            //launch app store
            [[iRate sharedInstance] openRatingsPageInAppStore];
        }
        else if (buttonIndex == 2) {  // maybe later
            [iRate sharedInstance].lastReminded = [NSDate date];
        }
        else if (buttonIndex == 3) {  // open web page
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.apple.com"]];
        }
    }
    else if (alertView.tag == 1) {
        if (buttonIndex == 0) {
            [self clearCurrentSession];
        }
        else if (buttonIndex == 1) {
            //NSLog(@"No");
        }
    }
}


- (IBAction)shareEvent:(id)sender {
    //[self showSameVC];
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];

    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ShareVC *shareVw = [storyboard instantiateViewControllerWithIdentifier:@"ShareVC"];
    [self presentViewController:shareVw
                       animated:YES
                     completion:nil];
}

- (IBAction)showOrderHistory:(id)sender {
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];

    [USER_DEFAULTS setObject:@"0" forKey:@"my_information_status"];
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    OrderHistoryVC *orderHistoryVw = [storyboard instantiateViewControllerWithIdentifier:@"OrderHistoryVC"];
    [self presentViewController:orderHistoryVw
                       animated:YES
                     completion:^{
                         // CleverTap - Order History
                         [[CleverTap push] eventName:@"Order History"];
                     }];
}

- (IBAction)showNotifications:(id)sender {
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
    
    [USER_DEFAULTS setObject:@"1" forKey:@"my_information_status"];
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    OrderHistoryVC *orderHistoryVw = [storyboard instantiateViewControllerWithIdentifier:@"OrderHistoryVC"];
    [self presentViewController:orderHistoryVw
                       animated:YES
                     completion:^{
                         // CleverTap - My Notifications
                         [[CleverTap push] eventName:@"My Notifications"];
                     }];
}

- (IBAction)logOutEvent:(id)sender {
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];

    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"CanCan" message:@"Are you sure you want to Log out?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
    [alert show];
    
    alert.tag = 1;
}

-(void)clearCurrentSession {
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Logging out..."];
    
    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
    
    NSString *userID = [USER_DEFAULTS objectForKey:PARAM_USER_ID];
    NSString *usertoken = [USER_DEFAULTS objectForKey:PARAM_USER_TOKEN];
    
    [dictParams setObject:userID forKey:PARAM_USER_ID];
    [dictParams setObject:usertoken forKey:PARAM_USER_TOKEN];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_SESSION_LOGOUT withParamData:dictParams withBlock:^(id response, NSError *error) {
        if([[response valueForKey:@"status"] boolValue] == true) {
            [USER_DEFAULTS removeObjectForKey:@"isLogin"];
            [USER_DEFAULTS removeObjectForKey:PARAM_USER_ID];
            [USER_DEFAULTS removeObjectForKey:PARAM_USER_TOKEN];
            
            [USER_DEFAULTS setObject:@"" forKey:PARAM_CITY];
            [USER_DEFAULTS setObject:@"" forKey:PARAM_LOCALITY];
            
            [USER_DEFAULTS removeObjectForKey:PARAM_PHONE_NUM];
            [USER_DEFAULTS removeObjectForKey:PARAM_ADDRESS_ID];
            [USER_DEFAULTS removeObjectForKey:PARAM_TIME_SLOT];
            
            [USER_DEFAULTS synchronize];
            
            // CleverTap - Logout Event
            [[CleverTap push] eventName:@"Logout"];
            
            // Branch - Logout
            [[Branch getInstance] logout];

            [[AppDelegate sharedAppDelegate] hideLoadingView];

            //[self.navigationController popToRootViewControllerAnimated:YES];
            UIViewController *firstWindow = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SimpleLocalityVC"];
            [self.navigationController setViewControllers:[NSArray arrayWithObject:firstWindow] animated:YES];
        }
        else {
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            [[AppDelegate sharedAppDelegate] showAlertWithTitle:@"CanCan" andMessage:[response valueForKey:@"statusMessage"]];
        }
    }];
}

- (IBAction)loginEvent:(id)sender {
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];

    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LoginVC *loginView = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    
    [self presentViewController:loginView
                       animated:YES
                     completion:^{
                         // CleverTap - Side Menu Login
                         [[CleverTap push] eventName:@"Side Menu Login"];
                     }];
}

- (IBAction)showAboutUsView:(id)sender {
    UIButton *btn = (UIButton *)sender;
    
    [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
    
    if ([btn tag] == 0) {
        [USER_DEFAULTS setObject:@"0" forKey:@"aboutUsType"];
        // CleverTap - About Us
        [[CleverTap push] eventName:@"About Us"];
    }
    else {
        [USER_DEFAULTS setObject:@"1" forKey:@"aboutUsType"];
        // CleverTap - Help
        [[CleverTap push] eventName:@"Help"];
    }
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AboutUsVC *aboutUsVw = [storyboard instantiateViewControllerWithIdentifier:@"AboutUsVC"];
    [self presentViewController:aboutUsVw
                       animated:YES
                     completion:nil];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
