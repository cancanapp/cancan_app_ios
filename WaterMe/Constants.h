//
//  Constants.h
//  WaterMe
//
//  Created by Dev on 03/07/15.
//  Copyright (c) 2015 WaterMe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

#define USER_DEFAULTS [NSUserDefaults standardUserDefaults]

// Production Server
#define API_URL     @"http://52.74.138.240/user/"
#define SERVICE_URL @"http://52.74.138.240/"

//// Staging Server
//#define API_URL     @"http://188.166.252.120/user/"
//#define SERVICE_URL @"http://188.166.252.120/"

#pragma mark - API METHODS
extern NSString *const FILE_REGISTER;
extern NSString *const FILE_USER_CHECK;
extern NSString *const FILE_GET_ADDRESS;
extern NSString *const FILE_DELETE_ADDRESS;
extern NSString *const FILE_ADD_ADDRESS;
extern NSString *const FILE_SAVE_ORDER;
extern NSString *const FILE_SESSION_LOGOUT;
extern NSString *const FILE_WAITING_LIST;
extern NSString *const FILE_CHECK_PROMO;
extern NSString *const FILE_SAVE_FEEDBACK;
extern NSString *const FILE_GET_ORDER_HISTORY;
extern NSString *const FILE_GET_NEWS;

#pragma mark - API METHODS
extern NSString *const PARAM_PHONE_NUM;
extern NSString *const PARAM_VERIFICATION_CODE;
extern NSString *const PARAM_USER_ID;
extern NSString *const PARAM_USER_TOKEN;

extern NSString *const PARAM_FLAT_NO;
extern NSString *const PARAM_STREET_ADDRESS;
extern NSString *const PARAM_LANDMARK;
extern NSString *const PARAM_NICKNAME;

extern NSString *const PARAM_CITY;
extern NSString *const PARAM_LOCALITY;
extern NSString *const PARAM_ZIPCODE;

extern NSString *const PARAM_TIME_SLOT;
extern NSString *const PARAM_SLOT_TEXT;
extern NSString *const PARAM_DELIVERY_DATE;
extern NSString *const PARAM_DELIVERY_DAY;

extern NSString *const PARAM_PRODUCT_COUNT;
extern NSString *const PARAM_PRODUCT_ID;
extern NSString *const PARAM_PRODUCT_AMOUNT;
extern NSString *const PARAM_BRANCH_CREDIT;

extern NSString *const PARAM_PRODUCT_IMAGE;
extern NSString *const PARAM_PRODUCT_DEPOSIT;
extern NSString *const PARAM_PRODUCT_NAME;

extern NSString *const PARAM_DEVICE_TOKEN;
extern NSString *const PARAM_DEVICE_TYPE;

extern NSString *const PARAM_EMAIL;
extern NSString *const PARAM_PROMO_CODE;
extern NSString *const PARAM_PROMO_NAME;
extern NSString *const PARAM_PROMO_ID;

extern NSString *const PARAM_ADDRESS_ID;
extern NSString *const PARAM_ADDRESS_ID_1;
extern NSString *const PARAM_ADDRESS_ID_2;
extern NSString *const PARAM_ADDRESS_ID_3;
extern NSString *const PARAM_ADDRESS_ID_4;
extern NSString *const PARAM_ADDRESS_ID_5;
extern NSString *const PARAM_SERVICE_HEADER_TITLE;

extern NSString *const PARAM_PAYMENT_METHOD;
extern NSString *const PARAM_PAYMENT_ID;

extern NSString *const PARAM_ORDER_ID;
extern NSString *const PARAM_ORDER_RATING;
extern NSString *const PARAM_ORDER_FEEDBACK;

extern NSString *device_token;

@end
