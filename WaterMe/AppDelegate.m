//
//  AppDelegate.m
//  WaterMe
//
//  Created by Dev on 07/05/15.
//  Copyright (c) 2015 CanCan Pvt Ltd. All rights reserved.
//

#import "AppDelegate.h"
#import "Constants.h"
#import "SWRevealViewController.h"
#import <Crashlytics/Crashlytics.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "PQFCustomLoaders.h"
#import "iRate.h"
#import <CleverTapSDK/CleverTap.h>
#import "AFNetworkReachabilityManager.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "Branch.h"
#import "ShareVC.h"
#import "OrderHistoryVC.h"
#import "Konotor.h"
#import "KonotorEventHandler.h"

//@import Batch;

@interface AppDelegate () <SWRevealViewControllerDelegate,iRateDelegate,UIAlertViewDelegate>
{
    BOOL isReachable;
}

@property (nonatomic, strong) PQFBallDrop *ballDrop;
@property (nonatomic, strong) UIAlertView *alertView;

@end

@implementation AppDelegate
@synthesize viewLoading, loader;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];

    // Fabric - Crashlytics
    [Fabric with:@[CrashlyticsKit]];
    
    // CleverTap - Mobile Analytics
    [CleverTap notifyApplicationLaunchedWithOptions:launchOptions];
    
    // CleverTap
    NSDictionary *push = [launchOptions valueForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (push) {
        [self handlePush:push];
    }
    
    // Konotor
    if(push!=nil)
        [Konotor handleRemoteNotification:push withShowScreen:YES];
    
    // Branch - Referrals
    Branch *branch = [Branch getInstance];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        // params are the deep linked params associated with the link that the user clicked before showing up.
        //NSLog(@"deep link data: %@", [params description]);
    }];
    
    
    // Initialize Konotor
    [Konotor InitWithAppID:@"6b4602a6-c815-484b-a5d6-de8b497a0b42" AppKey:@"f86199b4-610e-415e-ab73-1725a447d29f" withDelegate:[KonotorEventHandler sharedInstance]];
    
    // Set welcome message as deemed fit
    [Konotor setUnreadWelcomeMessage:@"Thanks for trying our app! Do reach out to us if you have any inputs or feedback, and we will respond to you as soon as we can!"];

    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge
                                                                                             |UIUserNotificationTypeAlert
                                                                                             |UIUserNotificationTypeSound) categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    if ([USER_DEFAULTS boolForKey:@"isChangeLocality"] || ![USER_DEFAULTS boolForKey:@"isLogin"]) {
        UIViewController *frontVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SimpleLocalityVC"];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:frontVC];
        [navigationController setNavigationBarHidden:YES animated:YES];
        
        self.window.rootViewController = navigationController;
    }
    
    /* Reset badge app count if so desired */
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    [[Branch getInstance] handleDeepLink:url];
    
    [CleverTap handleOpenURL:url sourceApplication:sourceApplication];
    
//    NSString *deepLinkPath = [url path];
//    NSString *deepLinkQuery = [url query];
//    Take an action based on the path and query data

    return YES;
}

- (void) handlePush:(NSDictionary*)notification {
    
    // check the notfication for the Deep Link key:value and let the system open the Deep Link, this will invoke your Deep LInk handler
    
    if ([USER_DEFAULTS boolForKey:@"isLogin"]) {
        if (notification || [notification objectForKey:@"wzrk_dl"] != nil || [[notification objectForKey:@"wzrk_dl"] length] > 0 ) {
            NSString *urlLink=[NSString stringWithFormat:@"%@",[notification objectForKey:@"wzrk_dl"]];
            
            if ([urlLink isEqualToString:@"referAndEarnCleverTap"]) {
                UIViewController *rootController = (UIViewController *)self.window.rootViewController;
                UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                ShareVC *shareVw = [storyboard instantiateViewControllerWithIdentifier:@"ShareVC"];
                
                [rootController presentViewController:shareVw animated:YES completion:NULL];
            }
            else if ([urlLink isEqualToString:@"showNotificationsCleverTap"]) {
                [USER_DEFAULTS setObject:@"1" forKey:@"my_information_status"];
                
                UIViewController *rootController = (UIViewController *)self.window.rootViewController;
                UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                OrderHistoryVC *shareVw = [storyboard instantiateViewControllerWithIdentifier:@"OrderHistoryVC"];
                
                [rootController presentViewController:shareVw animated:YES completion:NULL];
            }
        }
        
        if (notification || [notification objectForKey:@"com.batch"] != nil || [[notification objectForKey:@"com.batch"] length] > 0 ) {
            NSString *urlLink=[NSString stringWithFormat:@"%@",[[notification objectForKey:@"com.batch"] objectForKey:@"l"]];

            if ([urlLink isEqualToString:@"referAndEarnBatch"]) {
                UIViewController *rootController = (UIViewController *)self.window.rootViewController;
                UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                ShareVC *shareVw = [storyboard instantiateViewControllerWithIdentifier:@"ShareVC"];
                
                [rootController presentViewController:shareVw animated:YES completion:NULL];
            }
            else if ([urlLink isEqualToString:@"showNotificationsBatch"]) {
                [USER_DEFAULTS setObject:@"1" forKey:@"my_information_status"];
                
                UIViewController *rootController = (UIViewController *)self.window.rootViewController;
                UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                OrderHistoryVC *shareVw = [storyboard instantiateViewControllerWithIdentifier:@"OrderHistoryVC"];
                
                [rootController presentViewController:shareVw animated:YES completion:NULL];
            }
        }
    }
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    [CleverTap handleNotificationWithData:userInfo];
    
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {

    // Clevertap
    [CleverTap setPushToken:deviceToken];

    // Konotor
    [Konotor addDeviceToken:deviceToken];
    
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    device_token=token;
    
    if(token==nil) {
        device_token=@"123456789";
    }
    
    [USER_DEFAULTS setObject:device_token forKey:PARAM_DEVICE_TOKEN];
    [USER_DEFAULTS synchronize];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error {
    //NSLog(@"My token is error: %@", error);
    if (device_token==nil) {
        device_token=@"123456789";
    }
    
    [USER_DEFAULTS setObject:device_token forKey:PARAM_DEVICE_TOKEN];
    [USER_DEFAULTS synchronize];
}

- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [self handlePush:userInfo];

    //Clevertap
    [CleverTap handleNotificationWithData:userInfo];
    
    // Batch
    //[BatchPush dismissNotifications];
    
    // Konotor
    if([application applicationState]==UIApplicationStateActive)
        [Konotor handleRemoteNotification:userInfo withShowScreen:NO];
    else
        [Konotor handleRemoteNotification:userInfo withShowScreen:YES];
}

- (void) application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification {
    [CleverTap handleNotificationWithData:notification];
}

// As of iOS 8 and above
- (void) application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier
forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler {
    [CleverTap handleNotificationWithData:notification];
    if (completionHandler) completionHandler();
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    // Fb
    [FBSDKAppEvents activateApp];
    
    // Konotor
    [Konotor newSession];
    
    /* Reset badge app count if so desired */
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:
}

#pragma mark state preservation / restoration

- (BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder
{
    return YES;
}

- (BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder
{
    return YES;
}

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //[self.window makeKeyAndVisible];
    return YES;
}

#pragma mark - sharedAppDelegate
+(AppDelegate *)sharedAppDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

#pragma mark - Alert Helper
-(void)showAlertWithTitle:(NSString *)strTitle andMessage:(NSString *)strMsg {
    UIAlertView *alret=[[UIAlertView alloc]initWithTitle:strTitle message:strMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alret show];
}

#pragma mark - View Helper
-(void)addShadowto:(UIView *)view {
    // drop shadow
    [view.layer setShadowColor:[UIColor blackColor].CGColor];
    [view.layer setShadowOpacity:0.8];
    [view.layer setShadowRadius:2.0];
    [view.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

#pragma mark - Loading View

-(void)showLoadingWithTitle:(NSString *)title
{
    if (viewLoading==nil) {
        UIVisualEffect *blurEffect;
        blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        
        viewLoading = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        viewLoading.frame =self.window.bounds;
        viewLoading.backgroundColor = [UIColor clearColor];
        
        //        loader = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake((viewLoading.frame.size.width-44)/2, ((viewLoading.frame.size.height-30)/2)-44, 44, 44)];
        //        loader.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        //        loader.color = [UIColor colorWithRed:0.1765 green:0.6039 blue:1.0 alpha:1.0];
        //        [loader startAnimating];
        //        [viewLoading addSubview:loader];
        
        self.ballDrop = [PQFBallDrop createLoaderOnView:viewLoading];
        [self.ballDrop showLoader];
        self.ballDrop.loaderColor = [UIColor colorWithRed:0.4264 green:0.8331 blue:0.4944 alpha:1.0]; //[UIColor colorWithRed:0.2268 green:0.5995 blue:0.9825 alpha:1.0]
        self.ballDrop.delay = 0.5f;
        
        UITextView *txt=[[UITextView alloc]initWithFrame:CGRectMake((viewLoading.frame.size.width-250)/2, ((viewLoading.frame.size.height-60)/2)+25, 250, 60)];
        txt.textAlignment=NSTextAlignmentCenter;
        txt.backgroundColor=[UIColor clearColor];
        txt.text=[title uppercaseString];
        txt.font=[UIFont systemFontOfSize:18.0f weight:UIFontWeightLight];
        txt.userInteractionEnabled=FALSE;
        txt.scrollEnabled=FALSE;
        txt.textColor=[UIColor blackColor];
        [viewLoading addSubview:txt];
    }
    
    [self.window addSubview:viewLoading];
    [self.window bringSubviewToFront:viewLoading];
}

-(void)hideLoadingView
{
    if (viewLoading) {
        [viewLoading removeFromSuperview];
        [self.ballDrop removeLoader];
        //[loader stopAnimating];
        viewLoading=nil;
    }
}

// Check Internet Connectivity
-(BOOL)Connected {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status){
        //NSLog(@"status changed");
        //check for isReachable here
        isReachable = status;
    }];
    return isReachable;
}

#pragma mark - Check email Validity
-(BOOL)isValidEmailAddress:(NSString *)email
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString:laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

@end
