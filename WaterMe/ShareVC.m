//
//  ShareVC.m
//  CanCan
//
//  Created by AC on 25/09/15.
//  Copyright © 2015 CanCan Pvt. Ltd.,. All rights reserved.
//

#import "ShareVC.h"
#import <CleverTapSDK/CleverTap.h>
#import "Branch.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "UICountingLabel.h"

@interface ShareVC ()

@property (strong, nonatomic) IBOutlet UICountingLabel *lblCredits;

@property (strong, nonatomic) IBOutlet UIView *smartTipsView;
@property (strong, nonatomic) IBOutlet UIView *tipsView;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;

@end

@implementation ShareVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self loadBranchCredits];
    [self customLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)customLayout {
    [self.smartTipsView setHidden:YES];

    //self.smartTipsView.frame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    //[self.view addSubview:self.smartTipsView];
    //[self.view bringSubviewToFront:self.smartTipsView];
    
    self.tipsView.layer.cornerRadius = 5.0f;
    [[AppDelegate sharedAppDelegate]addShadowto:self.tipsView];
    
    self.btnClose.layer.cornerRadius = 27;
    [[AppDelegate sharedAppDelegate]addShadowto:self.btnClose];
}

-(void)loadBranchCredits {
    [[Branch getInstance] loadRewardsWithCallback:^(BOOL changed, NSError *err) {
        if (!err) {
            //NSLog(@"credit: %lu", (long)[[Branch getInstance] getCredits]);
            int currentCredits = (int)[[Branch getInstance] getCredits];
            
            self.lblCredits.format = @"%d";
            self.lblCredits.method = UILabelCountingMethodEaseInOut;
            
            [self.lblCredits countFrom:0 to:currentCredits];
        }
    }];
}

- (void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text) {
        [sharingItems addObject:text];
    }
    if (image) {
        [sharingItems addObject:image];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:^{
        // CleverTap - Share
        [[CleverTap push] eventName:@"Share"];
    }];
    
}

- (IBAction)shareEvent:(id)sender {
    [self shareText:@"Hey Download the CanCan App and get your first water can FREE!!! By using this link," andImage:nil andUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:@"shareUrl"]]]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)smartTipsEvent:(id)sender {
    [self.smartTipsView setHidden:NO];

    [UIView animateWithDuration:0.5f animations:^{
        CGRect newRect = self.smartTipsView.frame;
        newRect.origin.y = 0;
        [self.smartTipsView setFrame:newRect];
    } completion:nil];
}

- (IBAction)smartTipsCloseEvent:(id)sender {
    [UIView animateWithDuration:0.5f animations:^{
        CGRect newRect = self.smartTipsView.frame;
        newRect.origin.y = self.view.bounds.size.height;
        [self.smartTipsView setFrame:newRect];
    } completion:nil];
}

- (IBAction)closeEvent:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
