//
//  AboutUsVC.h
//  CanCan
//
//  Created by AC on 29/08/15.
//  Copyright (c) 2015 CanCan Pvt. Ltd.,. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface AboutUsVC : UIViewController <MFMailComposeViewControllerDelegate,UIAlertViewDelegate>
{
    MFMailComposeViewController *mailComposer;
}

@end
