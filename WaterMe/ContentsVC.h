//
//  ContentsVC.h
//  WaterMe
//
//  Created by Dev on 14/05/15.
//  Copyright (c) 2015 WaterMe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentsVC : UIViewController <UIScrollViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIAlertViewDelegate>

@end
