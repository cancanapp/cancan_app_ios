//
//  Constants.m
//  WaterMe
//
//  Created by Dev on 03/07/15.
//  Copyright (c) 2015 WaterMe. All rights reserved.
//

#import "Constants.h"

@implementation Constants

#pragma mark - API METHODS
NSString *const FILE_REGISTER=@"login";
NSString *const FILE_USER_CHECK=@"check";
NSString *const FILE_GET_ADDRESS=@"getuserinfo";
NSString *const FILE_DELETE_ADDRESS=@"delete_address";
NSString *const FILE_ADD_ADDRESS=@"address";
NSString *const FILE_SAVE_ORDER=@"saveorder";
NSString *const FILE_SESSION_LOGOUT=@"logout";
NSString *const FILE_WAITING_LIST=@"waitinglist";
NSString *const FILE_CHECK_PROMO=@"check_promo";
NSString *const FILE_SAVE_FEEDBACK=@"savefeedback";
NSString *const FILE_GET_ORDER_HISTORY=@"getorders";
NSString *const FILE_GET_NEWS=@"get_news";

#pragma mark - API METHODS
NSString *const PARAM_PHONE_NUM=@"phone_number";
NSString *const PARAM_VERIFICATION_CODE=@"verification_code";
NSString *const PARAM_USER_ID=@"id";
NSString *const PARAM_USER_TOKEN=@"token";

NSString *const PARAM_FLAT_NO=@"door_no";
NSString *const PARAM_STREET_ADDRESS=@"street_name";
NSString *const PARAM_LANDMARK=@"landmark";
NSString *const PARAM_NICKNAME=@"label";

NSString *const PARAM_CITY=@"city";
NSString *const PARAM_LOCALITY=@"locality";
NSString *const PARAM_ZIPCODE=@"zipcode";

NSString *const PARAM_TIME_SLOT=@"time_slot";
NSString *const PARAM_SLOT_TEXT=@"slot_text";
NSString *const PARAM_DELIVERY_DATE=@"deliver_date";
NSString *const PARAM_DELIVERY_DAY=@"deliver_day";

NSString *const PARAM_PRODUCT_COUNT=@"no_of_cans";
NSString *const PARAM_PRODUCT_ID=@"product_id";
NSString *const PARAM_PRODUCT_AMOUNT=@"amount"; //branch_credit
NSString *const PARAM_BRANCH_CREDIT=@"branch_credits";

NSString *const PARAM_PRODUCT_IMAGE=@"product_image";
NSString *const PARAM_PRODUCT_DEPOSIT=@"product_deposit";
NSString *const PARAM_PRODUCT_NAME=@"product_name";

NSString *const PARAM_DEVICE_TOKEN=@"device_token";
NSString *const PARAM_DEVICE_TYPE=@"device_type";
NSString *const PARAM_EMAIL=@"email";
NSString *const PARAM_PROMO_CODE=@"promo_code";

NSString *const PARAM_PROMO_NAME=@"promoCode";
NSString *const PARAM_PROMO_ID=@"promoID";

NSString *const PARAM_ADDRESS_ID=@"address_id";
NSString *const PARAM_ADDRESS_ID_1=@"address_id_1"; //address_id
NSString *const PARAM_ADDRESS_ID_2=@"address_id_2";
NSString *const PARAM_ADDRESS_ID_3=@"address_id_3";
NSString *const PARAM_ADDRESS_ID_4=@"address_id_4";
NSString *const PARAM_ADDRESS_ID_5=@"address_id_5";
NSString *const PARAM_SERVICE_HEADER_TITLE=@"serviceAreaHeader";

NSString *const PARAM_PAYMENT_METHOD=@"payment_method";
NSString *const PARAM_PAYMENT_ID = @"payment_id";

NSString *const PARAM_ORDER_ID=@"order_id";
NSString *const PARAM_ORDER_RATING=@"rating";
NSString *const PARAM_ORDER_FEEDBACK=@"feedback";

NSString *device_token;


@end
