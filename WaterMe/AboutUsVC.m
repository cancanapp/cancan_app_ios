//
//  AboutUsVC.m
//  CanCan
//
//  Created by AC on 29/08/15.
//  Copyright (c) 2015 CanCan Pvt. Ltd.,. All rights reserved.
//

#import "AboutUsVC.h"
#import "Constants.h"
#import <CleverTapSDK/CleverTap.h>
#import "Branch.h"
#import "KonotorUI.h"

@interface AboutUsVC ()

@property (strong, nonatomic) IBOutlet UILabel *headerLbl;
@property (strong, nonatomic) IBOutlet UILabel *aboutUsLbl;
@property (strong, nonatomic) IBOutlet UIImageView *logoImgVw;

@property (strong, nonatomic) IBOutlet UIButton *btnCall;
@property (strong, nonatomic) IBOutlet UIButton *btnEmail;
@property (weak, nonatomic) IBOutlet UIImageView *termsIcon;
@property (weak, nonatomic) IBOutlet UIButton *termsBtn;
@property (weak, nonatomic) IBOutlet UIImageView *privacyIcon;
@property (weak, nonatomic) IBOutlet UIButton *privacyBtn;

@end

@implementation AboutUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([[USER_DEFAULTS objectForKey:@"aboutUsType"] isEqualToString:@"0"]) {
        self.headerLbl.text = @"ABOUT US";
        
        self.aboutUsLbl.hidden = NO;
        self.aboutUsLbl.adjustsFontSizeToFitWidth = YES;
        self.aboutUsLbl.minimumScaleFactor = 12.0/[UIFont labelFontSize];
        self.aboutUsLbl.textAlignment = NSTextAlignmentJustified;
        
        self.termsIcon.hidden = NO;
        self.termsBtn.hidden = NO;
        self.privacyIcon.hidden = NO;
        self.privacyBtn.hidden = NO;

        self.btnCall.hidden = YES;
        self.btnEmail.hidden = YES;
    }
    else {
        self.headerLbl.text = @"HELP";
        
        self.aboutUsLbl.hidden = YES;
        
        self.termsIcon.hidden = YES;
        self.termsBtn.hidden = YES;
        self.privacyIcon.hidden = YES;
        self.privacyBtn.hidden = YES;
        
        self.btnCall.hidden = NO;
        self.btnEmail.hidden = NO;
    }
    
    UITapGestureRecognizer *singletap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shakeItShakeIt)];
    singletap.numberOfTapsRequired = 1;
    
    [self.logoImgVw setUserInteractionEnabled:YES];
    [self.logoImgVw addGestureRecognizer:singletap];
}

-(void)shakeItShakeIt {
    [self startShaketoView:self.logoImgVw];
    
    if ([USER_DEFAULTS objectForKey:@"isLogin"]) {
        // CleverTap - Surprise
        NSDictionary *props = @{@"Screen Name":self.headerLbl.text,
                                @"Phone":[NSString stringWithFormat:@"%@",PARAM_PHONE_NUM]};
        
        [[CleverTap push] eventName:@"Surprise" eventProps:props];

        // Branch - Surprise Event
        [[Branch getInstance] userCompletedAction:@"surprise_Event"];
        
        /*
        // Get Credits after Surprise
        [[Branch getInstance] loadRewardsWithCallback:^(BOOL changed, NSError *err) {
            if (!err) {
                //NSLog(@"After surprise credit: %lu", (long)[[Branch getInstance] getCredits]);
                long currentCredits = (long)[[Branch getInstance] getCredits];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"CanCan" message:[NSString stringWithFormat:@"You just found 50 CanCan Credits !!! Your Total Credits: %lu",currentCredits] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
        */
    }
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backEvent:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)startShaketoView:(UIView *)view {
    
    CGAffineTransform leftShake = CGAffineTransformMakeTranslation(-5, 0);
    CGAffineTransform rightShake = CGAffineTransformMakeTranslation(5, 0);
    
    view.transform = leftShake;  // starting point
    
    [UIView beginAnimations:@"shake_button" context:(__bridge void *)(view)];
    [UIView setAnimationRepeatAutoreverses:YES]; // important
    [UIView setAnimationRepeatCount:5];
    [UIView setAnimationDuration:0.06];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(shakeEnded:finished:context:)];
    
    view.transform = rightShake; // end here & auto-reverse
    
    [UIView commitAnimations];
}

- (void) shakeEnded:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if ([finished boolValue]) {
        UIView* item = (__bridge UIView *)context;
        item.transform = CGAffineTransformIdentity;
    }
}

- (IBAction)termsAndConditionsEvent:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://www.cancanapp.com/terms.html"];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (IBAction)privacyPolicyEvent:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://www.cancanapp.com/privacy.html"];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}



- (IBAction)callEvent:(id)sender {
    UIButton *callBtn = (UIButton *)sender;
    [self startShaketoView:callBtn];
    
//    UIAlertView *callPrompt = [[UIAlertView alloc]initWithTitle:@"8939969191" message:@"" delegate:self cancelButtonTitle:@"CALL" otherButtonTitles:@"CANCEL", nil];
//    [callPrompt show];
    
    NSString *phNo = @"+918144437777";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else {
        UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [callAlert show];
    }

}

- (IBAction)emailEvent:(id)sender {
    // Send Feedback mail
    UIButton *emailBtn = (UIButton *)sender;
    [self sendFeedbackMail:emailBtn];
    
    /*
    UIButton *chatBtn = (UIButton *)sender;
    [self startShaketoView:chatBtn];
    
    // Konotor Support Chat
    //set Title text for Konotor inbox window
    [[KonotorUIParameters sharedInstance] setTitleText:@"Chat With Us"];
    [[KonotorUIParameters sharedInstance] setTitleTextFont:[UIFont systemFontOfSize:17.0f weight:UIFontWeightLight]];
    [[KonotorUIParameters sharedInstance] setMessageTextFont:[UIFont systemFontOfSize:15.0f weight:UIFontWeightLight]];
    

    // set to YES to turn off option for user to send picture messages
    [[KonotorUIParameters sharedInstance] setNoPhotoOption:YES];
    // set to NO to turn off option for user to send voice messages
    [[KonotorUIParameters sharedInstance] setVoiceInputEnabled:NO];
    // set to color of your choice for the top navbar
    [[KonotorUIParameters sharedInstance] setHeaderViewColor:[UIColor colorWithRed:0.8466 green:0.8778 blue:0.9312 alpha:1.0]];
    // set style of toast to display if a new message is received while in the app
    [[KonotorUIParameters sharedInstance] setToastStyle:KonotorToastStyleBarOnRootView backgroundColor:[UIColor darkGrayColor] textColor:[UIColor whiteColor]];
    // set this to change the color fo the title text
    [[KonotorUIParameters sharedInstance] setTitleTextColor:[UIColor blackColor]];
    // set this to change the color of the send button for the text input field
    [[KonotorUIParameters sharedInstance] setSendButtonColor:[UIColor colorWithRed:0.4275 green:0.8314 blue:0.4941 alpha:1.0]];
    // set this to change the color of the Done button to dismiss the Konotor view
    [[KonotorUIParameters sharedInstance] setDoneButtonColor:[UIColor colorWithRed:0.2268 green:0.5995 blue:0.9825 alpha:1.0]];
//    // set this to your app icon's image name to show your app icon to the left of a chat bubble
//    [[KonotorUIParameters sharedInstance] setOtherProfileImage:[UIImage imageNamed:@"can_can_text"]]; //can_can_text
    // set this to YES to show a loading animation while new messages are being downloaded
    [[KonotorUIParameters sharedInstance] setDontShowLoadingAnimation:YES];
    // set this to change the color of the action button
    [[KonotorUIParameters sharedInstance] setActionButtonColor:[UIColor darkGrayColor]];
    
    [KonotorFeedbackScreen showFeedbackScreen];
     */
}

-(void)sendFeedbackMail:(UIButton *)onClickBtn {
    [self startShaketoView:onClickBtn];
    
    if ([MFMailComposeViewController canSendMail]) {
        // Show the composer
        mailComposer = [[MFMailComposeViewController alloc]init];
        mailComposer.mailComposeDelegate = self;
        [mailComposer setToRecipients:@[@"help@cancanapp.com"]];
        //[mailComposer setSubject:@"Test mail"];
        //[mailComposer setMessageBody:@"Test mail" isHTML:NO];
        
        [self presentViewController:mailComposer animated:YES completion:nil];
    } else {
        // Handle the error
        UIAlertView *mailAlert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Email facility is not available!!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [mailAlert show];
    }

}

#pragma mark - mail compose delegate

-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    NSString *msg1;
    switch (result)
    {
//        case MFMailComposeResultCancelled:
//            msg1 =@"Sending Mail is cancelled";
//            break;
        case MFMailComposeResultSaved:
            msg1=@"Mail saved on Drafts";
            break;
        case MFMailComposeResultSent:
            msg1 =@"Your Mail has been sent successfully";
            break;
//        case MFMailComposeResultFailed:
//            msg1 =@"Message sending failed";
//            break;
            
        default:
            msg1 =@"Your Mail is not Sent";
            break;
    }
    
    UIAlertView *mailResuletAlert = [[UIAlertView alloc]initWithTitle:@"CanCan" message:msg1 delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [mailResuletAlert show];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
