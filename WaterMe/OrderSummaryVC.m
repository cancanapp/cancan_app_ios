//
//  OrderSummaryVC.m
//  WaterMe
//
//  Created by Dev on 20/07/15.
//  Copyright (c) 2015 WaterMe. All rights reserved.
//

#import "OrderSummaryVC.h"
#import "Constants.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "ChooseAddressVC.h"
#import <CleverTapSDK/CleverTap.h>
#import "Branch.h"
#import "UICountingLabel.h"
#import <Razorpay/RazorpayCheckout.h>
#import <CleverTapSDK/CleverTap.h>

@interface OrderSummaryVC ()
{
    int currentCredits;
    NSString *creditStr;
}

@property (weak, nonatomic) IBOutlet UILabel *productCountLbl;
@property (weak, nonatomic) IBOutlet UICountingLabel *productPriceLbl;
@property (weak, nonatomic) IBOutlet UIImageView *productImgView;

@property (weak, nonatomic) IBOutlet UIButton *promoBtn;
@property (weak, nonatomic) IBOutlet UIView *promoBtnView;
@property (weak, nonatomic) IBOutlet UIButton *promoEventBtn;
@property (weak, nonatomic) IBOutlet UITextField *promoTxt;
@property (weak, nonatomic) IBOutlet UIView *promoView;
@property (weak, nonatomic) IBOutlet UILabel *refundableDepositLbl;
@property (weak, nonatomic) IBOutlet UILabel *deliveryTimeLbl;
@property (strong, nonatomic) IBOutlet UICountingLabel *creditsLbl;

@property (strong, nonatomic) IBOutlet UIView *paymentView;
@property (strong, nonatomic) IBOutlet UILabel *seperatorLbl1;
@property (strong, nonatomic) IBOutlet UIButton *confirmOrderBtn;

@property (strong, nonatomic) IBOutlet UIButton *payCodBtn;
@property (strong, nonatomic) IBOutlet UIButton *payOnlineBtn;
@property (strong, nonatomic) IBOutlet UIImageView *payCodImgVw;
@property (strong, nonatomic) IBOutlet UIImageView *payOnlineImgVw;

@property (strong, nonatomic) IBOutlet UIView *enterEmailView;
@property (weak, nonatomic) IBOutlet UIView *enterEmailSubView;
@property (strong, nonatomic) IBOutlet UITextField *emailTxtField;


@end

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

@implementation OrderSummaryVC

@synthesize payCodBtn, payOnlineBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.enterEmailView setAlpha:0.2f];
    [self.enterEmailView setHidden:YES];

    [self customLayout];
    [self customPaymentView];
    
    if (self.view.bounds.size.height < 568) {
        [self.seperatorLbl1 setHidden:YES];
        [self.promoView setHidden:YES];
        [self.refundableDepositLbl setHidden:YES];
        [self.paymentView setHidden:YES];
    }
    
    UITapGestureRecognizer *removeKeyboardTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeKeyboard)];
    [removeKeyboardTap setNumberOfTapsRequired:1];
    [self.view addGestureRecognizer:removeKeyboardTap];
}

-(void)removeKeyboard {
    [self.view endEditing:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }

    [self checkSessionValidity];
    
}

-(void)checkSessionValidity {
    if(![USER_DEFAULTS objectForKey:PARAM_ADDRESS_ID]) {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ChooseAddressVC *chooseAddressView = [storyboard instantiateViewControllerWithIdentifier:@"ChooseAddressVC"];
        
        [self presentViewController:chooseAddressView
                           animated:NO
                         completion:nil];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    [self viewResetBasedOnHeight];
}

-(void)viewResetBasedOnHeight {
    //NSLog(@"Size height ==== %f",self.view.bounds.size.height);
    
    if (self.view.bounds.size.height < 548) {
        [UIView animateWithDuration:0.3f animations:^{
            [self.seperatorLbl1 setHidden:NO];
            CGRect newSeperatorRect = self.seperatorLbl1.frame;
            newSeperatorRect.origin.y = self.seperatorLbl1.frame.origin.y - 18;
            [self.seperatorLbl1 setFrame:newSeperatorRect];
            
            [self.promoView setHidden:NO];
            CGRect newPromoRect = self.promoView.frame;
            newPromoRect.origin.y = self.promoView.frame.origin.y - 35;
            [self.promoView setFrame:newPromoRect];
            
            [self.refundableDepositLbl setHidden:NO];
            CGRect newDepositLblRect = self.refundableDepositLbl.frame;
            newDepositLblRect.origin.y = self.refundableDepositLbl.frame.origin.y - 85;
            [self.refundableDepositLbl setFrame:newDepositLblRect];
            
            [self.paymentView setHidden:NO];
            CGRect newRect = self.paymentView.frame;
            newRect.origin.y = self.paymentView.frame.origin.y - 80;
            [self.paymentView setFrame:newRect];
        } completion:nil];
    }
    else if (self.view.bounds.size.height == 548) {
        [UIView animateWithDuration:0.3f animations:^{
            if ([self.seperatorLbl1 isHidden]) {
                [self.seperatorLbl1 setHidden:NO];
                CGRect newSeperatorRect = self.seperatorLbl1.frame;
                newSeperatorRect.origin.y = self.seperatorLbl1.frame.origin.y;
                [self.seperatorLbl1 setFrame:newSeperatorRect];
                
                [self.promoView setHidden:NO];
                CGRect newPromoRect = self.promoView.frame;
                newPromoRect.origin.y = self.promoView.frame.origin.y - 10;
                [self.promoView setFrame:newPromoRect];
                
                [self.refundableDepositLbl setHidden:NO];
                CGRect newDepositLblRect = self.refundableDepositLbl.frame;
                newDepositLblRect.origin.y = self.refundableDepositLbl.frame.origin.y - 15;
                [self.refundableDepositLbl setFrame:newDepositLblRect];
                
                [self.paymentView setHidden:NO];
                CGRect newRect = self.paymentView.frame;
                newRect.origin.y = self.paymentView.frame.origin.y - 15;
                [self.paymentView setFrame:newRect];
            }
        } completion:nil];
    }
    else {
        [self.seperatorLbl1 setHidden:NO];
        [self.promoView setHidden:NO];
        [self.refundableDepositLbl setHidden:NO];
        [self.paymentView setHidden:NO];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [USER_DEFAULTS setObject:@"" forKey:PARAM_PROMO_NAME];
}

-(void)customLayout {
    NSString *imgUrl = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_IMAGE]];
    [self.productImgView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:nil];
    
    if ([[USER_DEFAULTS objectForKey:PARAM_PRODUCT_COUNT] isEqualToString:@"1"]) {
        self.productCountLbl.text = [NSString stringWithFormat:@"%@ Can",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_COUNT]];
    }
    else {
        self.productCountLbl.text = [NSString stringWithFormat:@"%@ Cans",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_COUNT]];
    }
    
    self.productPriceLbl.text = [NSString stringWithFormat:@"₹%@",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_AMOUNT]];
    
    self.refundableDepositLbl.text = [NSString stringWithFormat:@"P.S:If you are a first time %@ can user a refundable deposit of ₹%@ is applicable for each can.",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_NAME],[USER_DEFAULTS objectForKey:PARAM_PRODUCT_DEPOSIT]];
    
    NSString *selectedTimeSlot = [USER_DEFAULTS objectForKey:PARAM_SLOT_TEXT];
    
    self.deliveryTimeLbl.text = [NSString stringWithFormat:@"Delivery by, %@ between %@",[USER_DEFAULTS objectForKey:PARAM_DELIVERY_DAY],selectedTimeSlot];
    
    //NSLog(@"%@ - %@",[NSString stringWithFormat:@"Delivery by, %@ between %@",[USER_DEFAULTS objectForKey:PARAM_DELIVERY_DAY],selectedTimeSlot],[USER_DEFAULTS objectForKey:PARAM_DELIVERY_DATE]);
    
    self.creditsLbl.layer.cornerRadius = 5.0f;
    
    creditStr = @"0";

    [[Branch getInstance] loadRewardsWithCallback:^(BOOL changed, NSError *err) {
        
        if (!err) {
            //NSLog(@"credit: %lu", (long)[[Branch getInstance] getCredits]);
            currentCredits = (int)[[Branch getInstance] getCredits];
            creditStr = [NSString stringWithFormat:@"%d",currentCredits];
            
            NSString *priceStr = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_AMOUNT]];
            NSInteger priceVal = [priceStr integerValue];
            
            if (currentCredits > priceVal) {
                creditStr = [NSString stringWithFormat:@"%d",priceVal];
                currentCredits = priceVal;
            }
            else {
                creditStr = [NSString stringWithFormat:@"%d",currentCredits];
            }
        }
        else {
            creditStr = @"0";
            currentCredits = 0;
        }
        
        self.productPriceLbl.format = @"₹%d";
        self.productPriceLbl.method = UILabelCountingMethodLinear; //UILabelCountingMethodLinear
        
        NSString *priceStr = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_AMOUNT]];
        NSInteger priceVal = [priceStr integerValue];
        
        [self.productPriceLbl countFrom:priceVal to:priceVal-currentCredits];

        self.creditsLbl.format = @"CanCan Credits : %d";
        self.creditsLbl.method = UILabelCountingMethodLinear; //UILabelCountingMethodLinear
        
        [self.creditsLbl countFrom:0 to:currentCredits];

        [USER_DEFAULTS setObject:[NSString stringWithFormat:@"%d",priceVal-currentCredits] forKey:PARAM_PRODUCT_AMOUNT];
        [USER_DEFAULTS synchronize];
    }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)clearOrderEvent:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)confirmOrderEvent:(id)sender {
    UIButton *btn = (UIButton *)sender;
    
    if ([btn.titleLabel.text isEqualToString:@"CONFIRM ORDER"]) {
        [self saveCurrentOrderFor:@"COD" withPaymentID:@""];
//        [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"COD" andMessage:@""];
    }
    else {
        if (![USER_DEFAULTS objectForKey:PARAM_EMAIL]) {
            //[[AppDelegate sharedAppDelegate] showAlertWithTitle:@"No Email" andMessage:@"Please add an email so tht we can send you reciepts"];
//            [UIView animateWithDuration:0.6f animations:^{
//                [self.enterEmailView setAlpha:1.0f];
//                [[AppDelegate sharedAppDelegate] addShadowto:self.enterEmailSubView];
//            } completion:^(BOOL finished) {
//                [self.enterEmailView setHidden:NO];
//            }];
            
            [UIView animateWithDuration:0.3f animations:^{
                [self.enterEmailView setAlpha:1.0f];
                [self.enterEmailView setHidden:NO];
                [self.emailTxtField becomeFirstResponder];
                
                [[AppDelegate sharedAppDelegate] addShadowto:self.enterEmailSubView];
            }];
            
            UITapGestureRecognizer *tapGest = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeEmailEnterView)];
            [tapGest setNumberOfTapsRequired:1];
            [self.enterEmailView addGestureRecognizer:tapGest];

        }
        else {
            [self saveCurrentOrderFor:@"ONLINE_INITIATE" withPaymentID:@""];
        }
    }
    //[self performSegueWithIdentifier:@"showThanksView" sender:self];
}

-(void)removeEmailEnterView {
    [UIView animateWithDuration:0.3f animations:^{
        [self.view endEditing:YES];
        [self.enterEmailView setAlpha:0.2f];
    } completion:^(BOOL finished) {
        [self.enterEmailView setHidden:YES];
    }];
    
//    [UIView animateWithDuration:0.6f animations:^{
//        [self.view endEditing:YES];
//        [self.enterEmailView setAlpha:0.2f];
//        [self.enterEmailView setHidden:YES];
//    }];
}

- (IBAction)addPromoEvent:(id)sender {
    if ([self.promoBtn.titleLabel.text isEqualToString:@"Got a Promo Code?"]) {
        [UIView animateWithDuration:0.3f animations:^{
            CGRect newRect = self.promoBtnView.frame;
            newRect.origin.y -= 50;
            [self.promoBtnView setFrame:newRect];
        }];
        
        [self.promoTxt becomeFirstResponder];
        [self.promoTxt setText:@""];
        [self.promoEventBtn setTitle:@"CANCEL" forState:UIControlStateNormal];
    }
    else {
        [UIView animateWithDuration:0.3f animations:^{
            CGRect newRect = self.promoBtnView.frame;
            newRect.origin.y -= 50;
            [self.promoBtnView setFrame:newRect];
        }];
        
        [self.promoTxt becomeFirstResponder];
        [self.promoEventBtn setTitle:@"CANCEL" forState:UIControlStateNormal];
    }
}

- (IBAction)promoEvent:(id)sender {
    UIButton *btn = (UIButton *)sender;
    
    if ([btn.titleLabel.text isEqualToString:@"CANCEL"]) {
        [UIView animateWithDuration:0.3f animations:^{
            CGRect newRect = self.promoBtnView.frame;
            newRect.origin.y += 50;
            [self.promoBtnView setFrame:newRect];
        }];
        
        [self.promoTxt setText:@""];
        [USER_DEFAULTS setObject:@"" forKey:PARAM_PROMO_NAME];
        
        if (currentCredits == 0) {
            [[Branch getInstance] loadRewardsWithCallback:^(BOOL changed, NSError *err) {
                
                if (!err) {
                    //NSLog(@"credit: %lu", (long)[[Branch getInstance] getCredits]);
                    currentCredits = (int)[[Branch getInstance] getCredits];
                    
                    creditStr = [NSString stringWithFormat:@"%d",currentCredits];
                    
                    NSInteger total_value = [[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_AMOUNT]] integerValue];

                    if (currentCredits > total_value) {
                        creditStr = [NSString stringWithFormat:@"%d",total_value];
                        currentCredits = total_value;
                    }
                    else {
                        creditStr = [NSString stringWithFormat:@"%d",currentCredits];
                    }
                }
                else {
                    creditStr = @"0";
                    currentCredits = 0;
                }
                
                self.creditsLbl.format = @"CanCan Credits : %d";
                self.creditsLbl.method = UILabelCountingMethodLinear; //UILabelCountingMethodLinear
                
                float total_value = [[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_AMOUNT]] floatValue];
                
                [USER_DEFAULTS setObject:[NSString stringWithFormat:@"%f",total_value] forKey:PARAM_PRODUCT_AMOUNT];
                [USER_DEFAULTS synchronize];
                
                self.productPriceLbl.format = @"₹%.0f";
                self.productPriceLbl.method = UILabelCountingMethodLinear; //UILabelCountingMethodLinear
                
                NSString *priceStr = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_AMOUNT]];
                NSInteger pirceVal = [priceStr integerValue];
                
                [self.creditsLbl countFrom:0 to:currentCredits];
                [self.productPriceLbl countFrom:pirceVal to:pirceVal-currentCredits];
                
                [USER_DEFAULTS setObject:[NSString stringWithFormat:@"%d",pirceVal-currentCredits] forKey:PARAM_PRODUCT_AMOUNT];
                [USER_DEFAULTS synchronize];
            }];
        }

        [self.promoBtn setTitle:@"Got a Promo Code?" forState:UIControlStateNormal];
        
        [self.promoTxt resignFirstResponder];
    }
    else if ([btn.titleLabel.text isEqualToString:@"APPLY"]){

        [self checkPromo];
    }
}

-(void)checkPromo {
    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
    
    [USER_DEFAULTS synchronize];
    
    NSString *userID = [USER_DEFAULTS objectForKey:PARAM_USER_ID];
    NSString *usertoken = [USER_DEFAULTS objectForKey:PARAM_USER_TOKEN];
    
    [dictParams setObject:userID forKey:PARAM_USER_ID];
    [dictParams setObject:usertoken forKey:PARAM_USER_TOKEN];
    [dictParams setObject:self.promoTxt.text forKey:PARAM_PROMO_CODE];
    [dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_PRODUCT_ID] forKey:@"brand_id"];
    
    [self.view endEditing:YES];
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Validating..."];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_CHECK_PROMO withParamData:dictParams withBlock:^(id response, NSError *error) {
        if([[response valueForKey:@"status"] boolValue] == true) {
            
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            
            self.creditsLbl.format = @"CanCan Credits : %d";
            self.creditsLbl.method = UILabelCountingMethodLinear; //UILabelCountingMethodLinear
            
            self.productPriceLbl.format = @"₹%d";
            self.productPriceLbl.method = UILabelCountingMethodLinear; //UILabelCountingMethodLinear
            
            NSString *priceStr = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_AMOUNT]];
            NSInteger pirceVal = [priceStr integerValue];
            
            [self.creditsLbl countFrom:currentCredits to:0];
            [self.productPriceLbl countFrom:pirceVal to:pirceVal+currentCredits];
            
            [USER_DEFAULTS setObject:[NSString stringWithFormat:@"%d",pirceVal+currentCredits] forKey:PARAM_PRODUCT_AMOUNT];
            [USER_DEFAULTS synchronize];
            
            creditStr = @"0";
            currentCredits = 0;
            
            NSDictionary *promoDetails = [response valueForKey:@"data"];
            [USER_DEFAULTS setObject:[promoDetails valueForKey:@"code"] forKey:PARAM_PROMO_NAME];
            [USER_DEFAULTS setObject:[promoDetails valueForKey:@"promo_id"] forKey:PARAM_PROMO_ID];
            
            float discount_value = [[promoDetails valueForKey:@"promo_value"] floatValue];
            float total_value = [[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_AMOUNT]] floatValue];
            
            float promo_price = total_value - discount_value;
            
            self.productPriceLbl.format = @"₹%.0f";
            [self.productPriceLbl countFrom:total_value to:promo_price];

            //self.productPriceLbl.text = [NSString stringWithFormat:@"₹%.0f",promo_price];
            [USER_DEFAULTS setObject:[NSString stringWithFormat:@"%.0f",promo_price] forKey:@"reducedPrice"];
            
            [self.promoBtn setTitle:[NSString stringWithFormat:@"Edit Promo: %@",[[promoDetails valueForKey:@"code"] uppercaseString]] forState:UIControlStateNormal];
            [UIView animateWithDuration:0.3f animations:^{
                CGRect newRect = self.promoBtnView.frame;
                newRect.origin.y += 50;
                [self.promoBtnView setFrame:newRect];
            }];
            
            // CleverTap - Promo Success
            NSDictionary *props = @{@"Offer name":[NSString stringWithFormat:@"%@",PARAM_PROMO_NAME],
                                    @"Offer status":@"Success",
                                    @"Status Message":[NSString stringWithFormat:@"%@",[promoDetails valueForKey:@"promo_description"]]};
            [[CleverTap push] eventName:@"Promo Applied" eventProps:props];

            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[promoDetails valueForKey:@"promo_description"]];
        }
        else {
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            [self.promoBtn setTitle:@"Got a Promo Code?" forState:UIControlStateNormal];
            [self.promoEventBtn setTitle:@"CANCEL" forState:UIControlStateNormal];

            [UIView animateWithDuration:0.3f animations:^{
                CGRect newRect = self.promoBtnView.frame;
                newRect.origin.y += 50;
                [self.promoBtnView setFrame:newRect];
            }];

            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[response valueForKey:@"statusMessage"]];
            
            // CleverTap - Promo Failure
            NSDictionary *failureProps = @{@"Offer name":[NSString stringWithFormat:@"%@",PARAM_PROMO_NAME],
                                    @"Offer status":@"Failure",
                                    @"Status Message":[NSString stringWithFormat:@"%@",[response valueForKey:@"statusMessage"]]};
            [[CleverTap push] eventName:@"Promo Applied" eventProps:failureProps];
        }
    }];
}

- (IBAction)changePromoBtnNameEvent:(id)sender {
    UITextField *txtField = (UITextField *)sender;
    
    if (txtField.text.length < 1) {
        [self.promoEventBtn setTitle:@"CANCEL" forState:UIControlStateNormal];
    }
    else {
        [self.promoEventBtn setTitle:@"APPLY" forState:UIControlStateNormal];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.promoTxt resignFirstResponder];
    return YES;
}


- (IBAction)paymentSelectionEvent:(id)sender {
    UIButton *paymentBtn = (UIButton *)sender;
    
    if (paymentBtn.tag == 0) {
        [_payCodImgVw setImage:[UIImage imageNamed:@"cod_filled"]];
        [_payOnlineImgVw setImage:[UIImage imageNamed:@"card_empty"]];
        
        [payCodBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [payOnlineBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];

        [self.confirmOrderBtn setTitle:@"CONFIRM ORDER" forState:UIControlStateNormal];

        [USER_DEFAULTS setObject:@"1" forKey:PARAM_PAYMENT_METHOD]; //COD
    }
    else if (paymentBtn.tag == 1) {
        [_payCodImgVw setImage:[UIImage imageNamed:@"cod_empty"]];
        [_payOnlineImgVw setImage:[UIImage imageNamed:@"card_filled"]];
        
        [payCodBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [payOnlineBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

        [self.confirmOrderBtn setTitle:@"PROCEED TO PAYMENT" forState:UIControlStateNormal];
        
        [USER_DEFAULTS setObject:@"2" forKey:PARAM_PAYMENT_METHOD]; //ONLINE
    }
    
    [USER_DEFAULTS synchronize];
}

-(void)saveCurrentOrderFor:(NSString *)paymentType withPaymentID:(NSString *)paymentId {
    if([paymentType isEqualToString:@"COD"]) {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Placing your order..."];
        
        [USER_DEFAULTS synchronize];
        
        NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
        
        NSString *userID = [USER_DEFAULTS objectForKey:PARAM_USER_ID];
        NSString *usertoken = [USER_DEFAULTS objectForKey:PARAM_USER_TOKEN];
        NSString *promoName = [USER_DEFAULTS objectForKey:PARAM_PROMO_NAME];
        NSString *paymentMethod = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PAYMENT_METHOD]];
        
        if (![USER_DEFAULTS objectForKey:PARAM_PAYMENT_METHOD]) {
            paymentMethod = @"1";
        }
        
        [dictParams setObject:userID forKey:PARAM_USER_ID];
        [dictParams setObject:usertoken forKey:PARAM_USER_TOKEN];
        [dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_ADDRESS_ID] forKey:PARAM_ADDRESS_ID]; //PARAM_ADDRESS_ID
        [dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_PRODUCT_ID] forKey:PARAM_PRODUCT_ID]; //PARAM_PRODUCT_ID
        if (promoName.length > 1) {
            [dictParams setObject:[USER_DEFAULTS objectForKey:@"reducedPrice"] forKey:PARAM_PRODUCT_AMOUNT]; //PARAM_PRODUCT_AMOUNT
        }
        else {
            [dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_PRODUCT_AMOUNT] forKey:PARAM_PRODUCT_AMOUNT]; //PARAM_PRODUCT_AMOUNT
            [USER_DEFAULTS setObject:@"" forKey:PARAM_PROMO_NAME];
        }
        
        [dictParams setObject:creditStr forKey:PARAM_BRANCH_CREDIT]; // branch_credit
        
        [dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_PRODUCT_COUNT] forKey:PARAM_PRODUCT_COUNT]; //PARAM_PRODUCT_COUNT
        [dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_TIME_SLOT] forKey:PARAM_TIME_SLOT]; //PARAM_TIME_SLOT
        [dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_DELIVERY_DATE] forKey:PARAM_DELIVERY_DATE]; //PARAM_DELIVERY_DATE
        [dictParams setObject:paymentMethod forKey:PARAM_PAYMENT_METHOD]; //PARAM_PAYMENT_METHOD
        [dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_PROMO_NAME] forKey:@"promo"]; //PARAM_PROMO_CODE
        [dictParams setObject:paymentId forKey:PARAM_PAYMENT_ID];
        //[dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_CHANGE] forKey:PARAM_CHANGE]; //PARAM_CHANGE
        
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_SAVE_ORDER withParamData:dictParams withBlock:^(id response, NSError *error) {
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            
            if([[response valueForKey:@"status"] boolValue] == true) {
                [self performSegueWithIdentifier:@"showThanksView" sender:self];
                
                // Branch - Order Completion Event
                [[Branch getInstance] userCompletedAction:@"order_complete"];
                
                //Branch - Redeem Credits
                if (currentCredits > 0) {
                    [[Branch getInstance] redeemRewards:currentCredits callback:^(BOOL success, NSError *error) {
                        if (success) {
                            //NSLog(@"Redeemed %d credits!",currentCredits);
                        }
                        else {
                            //NSLog(@"Failed to redeem credits: %@", error);
                        }
                    }];
                }
                
                // CleverTap - Charged
                NSDictionary *props = @{@"Amount":[NSString stringWithFormat:@"%@",PARAM_PRODUCT_AMOUNT],
                                        @"Currency":@"INR",
                                        @"Payment mode":@"COD",
                                        @"Time Slot":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_TIME_SLOT]],
                                        @"Quantity":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_COUNT]],
                                        @"Product":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_NAME]],
                                        @"City":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_CITY]],
                                        @"Locality":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_LOCALITY]]};
                [[CleverTap push] eventName:@"Charged" eventProps:props];
            }
            else {
                [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[response valueForKey:@"statusMessage"]];
            }
        }];
    }
    else if([paymentType isEqualToString:@"ONLINE_INITIATE"]) {
        [USER_DEFAULTS synchronize];
        
        NSString *promoName = [USER_DEFAULTS objectForKey:PARAM_PROMO_NAME];
        NSString *paymentMethod = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PAYMENT_METHOD]];
        NSString *paymentAmount = @"";
        NSString *paymentDescription = @"";
        
        if (![USER_DEFAULTS objectForKey:PARAM_PAYMENT_METHOD]) {
            paymentMethod = @"2";
        }
        
        if (promoName.length > 1) {
            [USER_DEFAULTS setObject:[USER_DEFAULTS objectForKey:@"reducedPrice"] forKey:PARAM_PRODUCT_AMOUNT];
        }
        
        paymentAmount = [USER_DEFAULTS objectForKey:PARAM_PRODUCT_AMOUNT];
        
        paymentDescription = [NSString stringWithFormat:@"%@ (%@) - INR %@",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_NAME],[USER_DEFAULTS objectForKey:PARAM_PRODUCT_COUNT],paymentAmount];

        [self showPaymentForAmount:paymentAmount andDescription:paymentDescription];
    }
    else if ([paymentType isEqualToString:@"ONLINE_SUCCESS"]) {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Placing your order..."];
        
        [USER_DEFAULTS synchronize];
        
        NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
        
        NSString *userID = [USER_DEFAULTS objectForKey:PARAM_USER_ID];
        NSString *usertoken = [USER_DEFAULTS objectForKey:PARAM_USER_TOKEN];
        NSString *promoName = [USER_DEFAULTS objectForKey:PARAM_PROMO_NAME];
        NSString *paymentMethod = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PAYMENT_METHOD]];
        
        if (![USER_DEFAULTS objectForKey:PARAM_PAYMENT_METHOD]) {
            paymentMethod = @"2";
        }
        
        [dictParams setObject:userID forKey:PARAM_USER_ID];
        [dictParams setObject:usertoken forKey:PARAM_USER_TOKEN];
        [dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_ADDRESS_ID] forKey:PARAM_ADDRESS_ID]; //PARAM_ADDRESS_ID
        [dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_PRODUCT_ID] forKey:PARAM_PRODUCT_ID]; //PARAM_PRODUCT_ID
        if (promoName.length > 1) {
            [dictParams setObject:[USER_DEFAULTS objectForKey:@"reducedPrice"] forKey:PARAM_PRODUCT_AMOUNT]; //PARAM_PRODUCT_AMOUNT
        }
        else {
            [dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_PRODUCT_AMOUNT] forKey:PARAM_PRODUCT_AMOUNT]; //PARAM_PRODUCT_AMOUNT
            [USER_DEFAULTS setObject:@"" forKey:PARAM_PROMO_NAME];
        }
        
        [dictParams setObject:creditStr forKey:PARAM_BRANCH_CREDIT]; // branch_credit
        
        [dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_PRODUCT_COUNT] forKey:PARAM_PRODUCT_COUNT]; //PARAM_PRODUCT_COUNT
        [dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_TIME_SLOT] forKey:PARAM_TIME_SLOT]; //PARAM_TIME_SLOT
        [dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_DELIVERY_DATE] forKey:PARAM_DELIVERY_DATE]; //PARAM_DELIVERY_DATE
        [dictParams setObject:paymentMethod forKey:PARAM_PAYMENT_METHOD]; //PARAM_PAYMENT_METHOD
        [dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_PROMO_NAME] forKey:@"promo"]; //PARAM_PROMO_CODE
        [dictParams setObject:paymentId forKey:PARAM_PAYMENT_ID];
        //[dictParams setObject:[USER_DEFAULTS objectForKey:PARAM_CHANGE] forKey:PARAM_CHANGE]; //PARAM_CHANGE
        
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_SAVE_ORDER withParamData:dictParams withBlock:^(id response, NSError *error) {
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            
            if([[response valueForKey:@"status"] boolValue] == true) {
                [self performSegueWithIdentifier:@"showThanksView" sender:self];
                
                // Branch - Order Completion Event
                [[Branch getInstance] userCompletedAction:@"order_complete"];
                
                //Branch - Redeem Credits
                if (currentCredits > 0) {
                    [[Branch getInstance] redeemRewards:currentCredits callback:^(BOOL success, NSError *error) {
                        if (success) {
                            //NSLog(@"Redeemed %d credits!",currentCredits);
                        }
                        else {
                            //NSLog(@"Failed to redeem credits: %@", error);
                        }
                    }];
                }
                
                // CleverTap - Charged
                NSDictionary *props = @{@"Amount":[NSString stringWithFormat:@"%@",PARAM_PRODUCT_AMOUNT],
                                        @"Currency":@"INR",
                                        @"Payment mode":@"ONLINE",
                                        @"Time Slot":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_TIME_SLOT]],
                                        @"Quantity":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_COUNT]],
                                        @"Product":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_NAME]],
                                        @"City":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_CITY]],
                                        @"Locality":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_LOCALITY]]};
                [[CleverTap push] eventName:@"Charged" eventProps:props];
            }
            else {
                [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[response valueForKey:@"statusMessage"]];
            }
        }];
    }
}

-(void)customPaymentView {
    _payCodImgVw = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, 30, 30)];
    [_payCodImgVw setTag:0];
    [_payCodImgVw setImage:[UIImage imageNamed:@"cod_filled"]];
    [_payCodImgVw setContentMode:UIViewContentModeScaleAspectFit];
    [self.paymentView addSubview:_payCodImgVw];
    
    _payOnlineImgVw = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width-45, 10, 30, 30)];
    [_payOnlineImgVw setTag:0];
    [_payOnlineImgVw setImage:[UIImage imageNamed:@"card_empty"]];
    [_payOnlineImgVw setContentMode:UIViewContentModeScaleAspectFit];
    [self.paymentView addSubview:_payOnlineImgVw];
    
    payCodBtn = [[UIButton alloc]initWithFrame:CGRectMake(1, 1, (self.view.bounds.size.width/2), 48)];
    [payCodBtn addTarget:self action:@selector(paymentSelectionEvent:) forControlEvents:UIControlEventTouchUpInside];
    [payCodBtn setTag:0];
    [payCodBtn setBackgroundColor:[UIColor clearColor]];
    [payCodBtn setTitle:@"Pay Via COD" forState:UIControlStateNormal];
    [payCodBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    payCodBtn.titleLabel.font = [UIFont systemFontOfSize:18.0f  weight:UIFontWeightLight];
    payCodBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0);
    [self.paymentView addSubview:payCodBtn];
    
    UILabel *seperatorLbl = [[UILabel alloc]initWithFrame:CGRectMake((self.view.bounds.size.width/2)+1, 5, 1, 40)];
    [seperatorLbl setBackgroundColor:[UIColor lightGrayColor]];
    [self.paymentView addSubview:seperatorLbl];
    
    payOnlineBtn = [[UIButton alloc]initWithFrame:CGRectMake((self.view.bounds.size.width/2)+2, 1, (self.view.bounds.size.width/2)-3, 48)];
    [payOnlineBtn addTarget:self action:@selector(paymentSelectionEvent:) forControlEvents:UIControlEventTouchUpInside];
    [payOnlineBtn setTag:1];
    [payOnlineBtn setBackgroundColor:[UIColor clearColor]];
    [payOnlineBtn setTitle:@"Pay Online" forState:UIControlStateNormal];
    [payOnlineBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    payOnlineBtn.titleLabel.font = [UIFont systemFontOfSize:18.0  weight:UIFontWeightLight];
    payOnlineBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 30);
    [self.paymentView addSubview:payOnlineBtn];
    
    [USER_DEFAULTS setObject:@"1" forKey:PARAM_PAYMENT_METHOD]; //COD
    [USER_DEFAULTS synchronize];
}

// RazorPay

- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];//UIImageJPEGRepresentation(image, 1.0)
}

- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

- (void) showPaymentForAmount:(NSString *)amount andDescription:(NSString *)description  { // called by your app
//    UIImage *logoImg = [UIImage imageNamed:@"cancan_240"];
//    NSString *logoImgStr = [self encodeToBase64String:logoImg];
//    logoImgStr = [logoImgStr stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    RazorpayCheckout * checkout = [[RazorpayCheckout alloc] initWithKey:@"rzp_live_FFNW581hItYIdK"]; //rzp_test_5d8TBfNgXf18Vr
    NSDictionary * options = @{
                               @"amount": [NSString stringWithFormat:@"%@00",amount],
                               @"name":@"CanCan",
                               @"currency": @"INR",
                               @"description":description,
                               @"image":@"http://www.cancanapp.com/img/logo-grey.png",
                               @"prefill" : @{
                                       @"contact": [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PHONE_NUM]],
                                       @"email": [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_EMAIL]]
                                       },
                               @"theme": @{
                                       @"color": @"#39d47d" // #2d9aff(Blue) #39d47d(Green)
                                       }
                               };
    
    [checkout setDelegate:self];
    [checkout open: options];
}

- (void)onPaymentSuccess:(nonnull NSString*) payment_id{
//    [[[UIAlertView alloc] initWithTitle:@"Payment Successful" message:payment_id delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
    [self saveCurrentOrderFor:@"ONLINE_SUCCESS" withPaymentID:payment_id];
}

- (void)onPaymentError:(nonnull NSString *) code description:(nonnull NSString *) str{
    // Custom messages for different use cases
    [[[UIAlertView alloc] initWithTitle:@"Error" message:str delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (IBAction)proceedFromEmail:(id)sender {
    if ([[AppDelegate sharedAppDelegate]isValidEmailAddress:self.emailTxtField.text]) {
        [self.view endEditing:YES];
        [self.enterEmailView setAlpha:0.2f];
        [self.enterEmailView setHidden:YES];
        
        [USER_DEFAULTS setObject:self.emailTxtField.text forKey:PARAM_EMAIL];
        [[CleverTap push] profile:@{@"Email":self.emailTxtField.text}];

        [self saveCurrentOrderFor:@"ONLINE_INITIATE" withPaymentID:@""];
    }
    else {
        [self startShaketoView:self.enterEmailSubView];
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:@"Invalid email" andMessage:@"Please enter a valie email"];
    }
}

- (void)startShaketoView:(UIView *)view {
    
    CGAffineTransform leftShake = CGAffineTransformMakeTranslation(-5, 0);
    CGAffineTransform rightShake = CGAffineTransformMakeTranslation(5, 0);
    
    view.transform = leftShake;  // starting point
    
    [UIView beginAnimations:@"shake_button" context:(__bridge void *)(view)];
    [UIView setAnimationRepeatAutoreverses:YES]; // important
    [UIView setAnimationRepeatCount:5];
    [UIView setAnimationDuration:0.06];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(shakeEnded:finished:context:)];
    
    view.transform = rightShake; // end here & auto-reverse
    
    [UIView commitAnimations];
}

- (void) shakeEnded:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if ([finished boolValue]) {
        UIView* item = (__bridge UIView *)context;
        item.transform = CGAffineTransformIdentity;
    }
}

@end
