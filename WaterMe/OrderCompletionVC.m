//
//  OrderCompletionVC.m
//  WaterMe
//
//  Created by Dev on 23/05/15.
//  Copyright (c) 2015 WaterMe. All rights reserved.
//

#import "OrderCompletionVC.h"
#import "ChooseAddressVC.h"
#import "AppDelegate.h"
#import "Constants.h"

@interface OrderCompletionVC ()

@end

@implementation OrderCompletionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
//    [[UIApplication sharedApplication] setStatusBarHidden:YES];
//    [self.navigationController.navigationBar setHidden:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)goToOrderPage:(id)sender {
    //[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    //NSLog(@"%@",self.navigationController.viewControllers);

//    if ([[self.navigationController.viewControllers firstObject] isEqual:[ChooseAddressVC class]]) {
//        [self.navigationController popToRootViewControllerAnimated:YES];
//    }
//    else {
//    if ([USER_DEFAULTS objectForKey:@"isLogin"]) {
        [self.navigationController popToRootViewControllerAnimated:YES];
//    }
//    else {
//        UIViewController *nextWindow = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ChooseAddressVC"];
//        [self.navigationController setViewControllers:[NSArray arrayWithObject:nextWindow] animated:YES];
//    }
//    }
}


@end
