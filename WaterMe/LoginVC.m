//
//  ViewController.m
//  WaterMe
//
//  Created by Dev on 07/05/15.
//  Copyright (c) 2015 WaterMe. All rights reserved.
//

#import "LoginVC.h"
#import "AFNHelper.h"
#import "Constants.h"
#import "AppDelegate.h"
#import <CleverTapSDK/CleverTap.h>
#import "Branch.h"
#import <Crashlytics/Crashlytics.h>
#import "ContentsVC.h"
#import "OrderSummaryVC.h"

@interface LoginVC ()

@property (weak, nonatomic) IBOutlet UITextField *numberTxt;
@property (weak, nonatomic) IBOutlet UIButton *btnVerify;
@property (weak, nonatomic) IBOutlet UITextField *lblCountryCode;
@property (weak, nonatomic) IBOutlet UILabel *lblCounryUnderline;
@property (weak, nonatomic) IBOutlet UILabel *lblPhoneUnderline;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIView *verificationView;
@property (weak, nonatomic) IBOutlet UITextField *codeTxt;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@end

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(returnKeyboard)];
    [tap setNumberOfTapsRequired:1];
    [self.view addGestureRecognizer:tap];
}

-(void)returnKeyboard {
    [self.view endEditing:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self.navigationController.navigationBar setHidden:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    if (self.navigationController.viewControllers.count == 0) {
        [self.backBtn setImage:[UIImage imageNamed:@"btn_down_expand"] forState:UIControlStateNormal];
    }
    else {
        [self.backBtn setImage:[UIImage imageNamed:@"back_btn"] forState:UIControlStateNormal];
    }
    
    [self.verificationView setHidden:YES];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    if ([textField isEqual:self.numberTxt])
        return newLength <= 10;
    else
        return newLength <=4;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setTabBtnBgInAct:(UIView*)vw
{
    for (UIButton *btn in [vw subviews])
    {
        if ([btn isKindOfClass:[UIButton class]]) {
            [btn setTitleColor:UIColorFromRGB(0x2d9aff) forState:UIControlStateNormal];
            [btn setBackgroundColor:[UIColor whiteColor]];
        }
    }
}

- (IBAction)resendVerificationCode:(id)sender {
    [self sendPhoneNumber];
}


- (IBAction)getVerificationCode:(id)sender {
    
    if ([self.verificationView isHidden]) {
        if (self.numberTxt.text.length < 10) {
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Enter a valid mobile number"];
        }
        else {
            [self sendPhoneNumber];
        }
    }
    else {
        if (self.codeTxt.text.length < 4) {
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Invalid Verification Code"];
        }
        else {
            [self sendVerificationCode];
        }
    }
}

-(void)sendPhoneNumber {
    NSMutableString *pageUrl = [NSMutableString stringWithFormat:@"%@?%@=%@",FILE_REGISTER,PARAM_PHONE_NUM,self.numberTxt.text];
    [USER_DEFAULTS setObject:self.numberTxt.text forKey:PARAM_PHONE_NUM];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error) {
        if([[response valueForKey:@"status"] boolValue] == true) {
            
            [self.lblDescription setText:[NSString stringWithFormat:@"Please enter the 4 digit code sent to +91 %@",[USER_DEFAULTS objectForKey:PARAM_PHONE_NUM]]];
            [self.verificationView setHidden:NO];
            [self.codeTxt becomeFirstResponder];
        }
        else {
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[response valueForKey:@"statusMessage"]];
        }
    }];
}

-(void)sendVerificationCode {
    [USER_DEFAULTS setObject:@"ios" forKey:PARAM_DEVICE_TYPE];
    [USER_DEFAULTS synchronize];
    
    NSString *deviceToken = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_DEVICE_TOKEN]];
    
    if (![USER_DEFAULTS objectForKey:PARAM_DEVICE_TOKEN]) {
        deviceToken = @"1234567890";
    }
    
    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
    
    [dictParams setObject:self.numberTxt.text forKey:PARAM_PHONE_NUM];
    [dictParams setObject:self.codeTxt.text forKey:PARAM_VERIFICATION_CODE];
    [dictParams setObject:deviceToken forKey:PARAM_DEVICE_TOKEN];
    [dictParams setObject:@"ios" forKey:PARAM_DEVICE_TYPE];
    
    [self.codeTxt resignFirstResponder];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_USER_CHECK withParamData:dictParams withBlock:^(id response, NSError *error) {
        if([[response valueForKey:@"status"] boolValue] == true) {
            
            NSDictionary *responseDict = [response valueForKey:@"data"];
            
            [USER_DEFAULTS setObject:[responseDict objectForKey:@"user_id"] forKey:PARAM_USER_ID];
            [USER_DEFAULTS setObject:[responseDict objectForKey:@"token"] forKey:PARAM_USER_TOKEN];
            [USER_DEFAULTS setBool:YES forKey:@"isLogin"];
            [USER_DEFAULTS synchronize];
            
            [self.verificationView setHidden:YES];
            
            if ([USER_DEFAULTS objectForKey:PARAM_TIME_SLOT]) {
                [self performSegueWithIdentifier:@"showAddressList" sender:self];
            }
            else {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            
            // CleverTap - Login Event
            [[CleverTap push] eventName:@"Login"];
            
            // Branch - Login
            [[Branch getInstance] setIdentity:[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_USER_ID]]];
            
            // Answers
            [Answers logLoginWithMethod:@"Phone Verification"
                                success:@YES
                       customAttributes:@{@"Customer Number":[NSString stringWithFormat:@"%@",self.numberTxt.text]}];
            
        }
        else {
            [self.codeTxt setText:@""];
            [self.codeTxt becomeFirstResponder];
            
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[response valueForKey:@"statusMessage"]];
        }
    }];
}

- (IBAction)enableVerificationBtn:(id)sender {
    if (self.codeTxt.text.length == 4) {
        [self.btnCheck setHidden:NO];
    }
    else {
        [self.btnCheck setHidden:YES];
    }
}

- (IBAction)closeLoginEvent:(id)sender {
    [self.view endEditing:YES];

    if (self.navigationController.viewControllers.count == 0) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
