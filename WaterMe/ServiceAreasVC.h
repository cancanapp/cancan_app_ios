//
//  ServiceAreasVC.h
//  WaterMe
//
//  Created by Dev on 19/07/15.
//  Copyright (c) 2015 WaterMe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceAreasVC : UIViewController <UITableViewDataSource, UITableViewDelegate> {

}

@property (strong, nonatomic) NSArray *locationsArr;

@end
