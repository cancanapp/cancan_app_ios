//
//  ChooseAddressVC.m
//  WaterMe
//
//  Created by Dev on 15/07/15.
//  Copyright (c) 2015 WaterMe. All rights reserved.
//

#import "ChooseAddressVC.h"
#import "Constants.h"
#import "LoginVC.h"
#import "AddAddressVC.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "FeedBackVC.h"
#import "SWRevealViewController.h"
#import <CleverTapSDK/CleverTap.h>
#import "Branch.h"
#import "KonotorUI.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface ChooseAddressVC () <SWRevealViewControllerDelegate>
{
    BOOL buttonFlashing;
    UIButton *btn;
}

@property (weak, nonatomic) IBOutlet UIScrollView *addressSclVw;

@property (weak, nonatomic) IBOutlet UIView *addressView1;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLbl1;
@property (weak, nonatomic) IBOutlet UILabel *flatNumLbl1;
@property (weak, nonatomic) IBOutlet UILabel *landmarkLbl1;
@property (weak, nonatomic) IBOutlet UILabel *localityLbl1;
@property (weak, nonatomic) IBOutlet UIView *selectionView1;
@property (weak, nonatomic) IBOutlet UIButton *selectionBtn1;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn1;
@property (strong, nonatomic) IBOutlet UIButton *editBtn1;

@property (weak, nonatomic) IBOutlet UIView *addressView2;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLbl2;
@property (weak, nonatomic) IBOutlet UILabel *flatNumLbl2;
@property (weak, nonatomic) IBOutlet UILabel *landmarkLbl2;
@property (weak, nonatomic) IBOutlet UILabel *localityLbl2;
@property (weak, nonatomic) IBOutlet UIView *selectionView2;
@property (weak, nonatomic) IBOutlet UIButton *selectionBtn2;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn2;
@property (strong, nonatomic) IBOutlet UIButton *editBtn2;

@property (weak, nonatomic) IBOutlet UIView *addressView3;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLbl3;
@property (weak, nonatomic) IBOutlet UILabel *flatNumLbl3;
@property (weak, nonatomic) IBOutlet UILabel *landmarkLbl3;
@property (weak, nonatomic) IBOutlet UILabel *localityLbl3;
@property (weak, nonatomic) IBOutlet UIView *selectionView3;
@property (weak, nonatomic) IBOutlet UIButton *selectionBtn3;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn3; //revealButtonItem
@property (strong, nonatomic) IBOutlet UIButton *editBtn3;

@property (weak, nonatomic) IBOutlet UIView *addressView4;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLbl4;
@property (weak, nonatomic) IBOutlet UILabel *flatNumLbl4;
@property (weak, nonatomic) IBOutlet UILabel *landmarkLbl4;
@property (weak, nonatomic) IBOutlet UILabel *localityLbl4;
@property (weak, nonatomic) IBOutlet UIView *selectionView4;
@property (weak, nonatomic) IBOutlet UIButton *selectionBtn4;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn5;
@property (strong, nonatomic) IBOutlet UIButton *editBtn4;

@property (weak, nonatomic) IBOutlet UIView *addressView5;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLbl5;
@property (weak, nonatomic) IBOutlet UILabel *flatNumLbl5;
@property (weak, nonatomic) IBOutlet UILabel *landmarkLbl5;
@property (weak, nonatomic) IBOutlet UILabel *localityLbl5;
@property (weak, nonatomic) IBOutlet UIView *selectionView5;
@property (weak, nonatomic) IBOutlet UIButton *selectionBtn5;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn4;
@property (strong, nonatomic) IBOutlet UIButton *editBtn5
;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
@property (weak, nonatomic) IBOutlet UIButton *addAddressBtn;
@property(nonatomic, strong) NSTimer *waitTimer;

@property (weak, nonatomic) IBOutlet UIButton *revealButtonItem; //btn

@end

@implementation ChooseAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    [self customSetup];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    [self checkSessionValidity];
}

-(void)checkSessionValidity {
    if(![USER_DEFAULTS boolForKey:@"isLogin"]) {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginVC *loginView = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        
        [self presentViewController:loginView
                           animated:YES
                         completion:nil];
    }
    else {
        [self customLayouts];
        [self getSavedAddress];
    }
}

/*
- (void)customSetup
{
    self.revealViewController.delegate = self;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController) {
        [self.revealButtonItem addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        [btn removeFromSuperview];
    } else {
        btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        [btn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [btn setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:btn];
        [self.view bringSubviewToFront:btn];
    }
}
*/

- (void)startShake {
    
    CGAffineTransform leftShake = CGAffineTransformMakeTranslation(-5, 0);
    CGAffineTransform rightShake = CGAffineTransformMakeTranslation(5, 0);
    
    self.addAddressBtn.transform = leftShake;  // starting point
    
    [UIView beginAnimations:@"shake_button" context:(__bridge void *)(self.addAddressBtn)];
    [UIView setAnimationRepeatAutoreverses:YES]; // important
    [UIView setAnimationRepeatCount:5];
    [UIView setAnimationDuration:0.06];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(shakeEnded:finished:context:)];
    
    self.addAddressBtn.transform = rightShake; // end here & auto-reverse
    
    [UIView commitAnimations];
}

- (void) shakeEnded:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if ([finished boolValue]) {
        UIView* item = (__bridge UIView *)context;
        item.transform = CGAffineTransformIdentity;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)customLayouts {
    [[self.addressView1 layer] setBorderWidth:0.5f];
    [[self.addressView1 layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    
    [[self.addressView2 layer] setBorderWidth:0.5f];
    [[self.addressView2 layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    
    [[self.addressView3 layer] setBorderWidth:0.5f];
    [[self.addressView3 layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    
    [[self.addressView4 layer] setBorderWidth:0.5f];
    [[self.addressView4 layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    
    [[self.addressView5 layer] setBorderWidth:0.5f];
    [[self.addressView5 layer] setBorderColor:[UIColor lightGrayColor].CGColor];
}

- (IBAction)addAddressEvent:(id)sender {
    
    [USER_DEFAULTS setObject:@"" forKey:PARAM_SERVICE_HEADER_TITLE];
    
    [self.waitTimer invalidate];
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddAddressVC *addAddress = [storyboard instantiateViewControllerWithIdentifier:@"AddAddressVC"];
    
    [self presentViewController:addAddress
                       animated:YES
                     completion:nil];
}

-(void)getSavedAddress {
    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
    
    NSString *userID = [USER_DEFAULTS objectForKey:PARAM_USER_ID];
    NSString *usertoken = [USER_DEFAULTS objectForKey:PARAM_USER_TOKEN];
    NSString *userCity = [USER_DEFAULTS objectForKey:PARAM_CITY];
    NSString *userLocality = [USER_DEFAULTS objectForKey:PARAM_LOCALITY];

    if (![USER_DEFAULTS objectForKey:PARAM_LOCALITY]) {
        userLocality = @"";
    }
    
    [dictParams setObject:userID forKey:PARAM_USER_ID];
    [dictParams setObject:usertoken forKey:PARAM_USER_TOKEN];
    [dictParams setObject:userCity forKey:PARAM_CITY];
    [dictParams setObject:userLocality forKey:PARAM_LOCALITY];
    
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Fetching your address list..."];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getDataFromPath:FILE_GET_ADDRESS withParamData:dictParams withBlock:^(id response, NSError *error) {
        if([[response valueForKey:@"status"] boolValue] == true) {
            
            NSArray *responseArr = [response valueForKey:@"data"];
            [self displayAddress:responseArr];
            
            if ([responseArr count] > 0) {
                [self selectAddressEvent:self.selectionBtn1];
            }
            
            [[AppDelegate sharedAppDelegate] hideLoadingView];
        }
        else {
            NSArray *emptyArr;
            [self displayAddress:emptyArr];
            
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            
            if ([[response valueForKey:@"statusMessage"] isEqualToString:@"Invalid user details"]) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"CanCan" message:@"Session Expired. Please try logging in again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
    }];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [USER_DEFAULTS removeObjectForKey:@"isLogin"];
    [USER_DEFAULTS removeObjectForKey:PARAM_USER_ID];
    [USER_DEFAULTS removeObjectForKey:PARAM_USER_TOKEN];
    
    [USER_DEFAULTS setObject:@"" forKey:PARAM_CITY];
    [USER_DEFAULTS setObject:@"" forKey:PARAM_LOCALITY];

    [USER_DEFAULTS removeObjectForKey:PARAM_PHONE_NUM];
    [USER_DEFAULTS removeObjectForKey:PARAM_ADDRESS_ID];
    [USER_DEFAULTS removeObjectForKey:PARAM_TIME_SLOT];
    
    [USER_DEFAULTS synchronize];
    
    // CleverTap - Logout Event
    [[CleverTap push] eventName:@"Logout"];
    
    // Branch - Logout
    [[Branch getInstance] logout];
    
    UIViewController *firstWindow = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SimpleLocalityVC"];
    [self.navigationController setViewControllers:[NSArray arrayWithObject:firstWindow] animated:YES];
}

-(void)resetDoneBtn {
    if ([self.addressView5 isHidden]) {
        [UIView animateWithDuration:0.5f animations:^{
            CGRect newRect = self.doneBtn.frame;
            newRect.origin.y = self.view.bounds.size.height - self.doneBtn.frame.size.height - 20;
            [self.doneBtn setFrame:newRect];
        } completion:^(BOOL finished) {
            [self.addressSclVw setContentOffset:CGPointZero animated:YES];
            [self.addressSclVw setScrollEnabled:NO];
        }];
    }
    else {
        [UIView animateWithDuration:0.5f animations:^{
            CGRect newRect = self.doneBtn.frame;
            newRect.origin.y = self.addressView5.frame.origin.y + self.addressView5.frame.size.height + 20;
            [self.doneBtn setFrame:newRect];
        } completion:^(BOOL finished) {
            [self.addressSclVw setScrollEnabled:YES];
            [self.addressSclVw setContentSize:CGSizeMake(self.view.bounds.size.width, self.doneBtn.frame.origin.y+self.doneBtn.frame.size.height+10)];
        }];
    }
}

-(void)displayAddress:(NSArray *)addressArray {
    if ([addressArray count] < 5) {
        [UIView animateWithDuration:0.5f animations:^{
            CGRect newRect = self.doneBtn.frame;
            newRect.origin.y = self.view.bounds.size.height - self.doneBtn.frame.size.height - 20;
            [self.doneBtn setFrame:newRect];
        } completion:^(BOOL finished) {
            [self.addressSclVw setContentOffset:CGPointZero animated:YES];
            [self.addressSclVw setScrollEnabled:NO];
            [self performSelector:@selector(resetDoneBtn) withObject:self];
        }];
    }
    else {
        [UIView animateWithDuration:0.5f animations:^{
            CGRect newRect = self.doneBtn.frame;
            newRect.origin.y = self.addressView5.frame.origin.y + self.addressView5.frame.size.height + 20;
            [self.doneBtn setFrame:newRect];
        } completion:^(BOOL finished) {
            [self.addressSclVw setScrollEnabled:YES];
            [self.addressSclVw setContentSize:CGSizeMake(self.view.bounds.size.width, self.doneBtn.frame.origin.y+self.doneBtn.frame.size.height+10)];
        }];
    }
    
    switch ([addressArray count]) {
        case 0:
        {
            [self.addressSclVw setHidden:YES];
            
            NSRunLoop *runloop = [NSRunLoop currentRunLoop];
            self.waitTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(startShake) userInfo:nil repeats:YES];
            [runloop addTimer:self.waitTimer forMode:NSRunLoopCommonModes];
            [runloop addTimer:self.waitTimer forMode:UITrackingRunLoopMode];
        }
            break;
        case 1:
            [self.addressSclVw setHidden:NO];
            [self.waitTimer invalidate];
            
            self.addressView1.hidden = NO;
            self.addressView2.hidden = YES;
            self.addressView3.hidden = YES;
            self.addressView4.hidden = YES;
            self.addressView5.hidden = YES;
            break;
        case 2:
            [self.addressSclVw setHidden:NO];
            [self.waitTimer invalidate];
            
            self.addressView1.hidden = NO;
            self.addressView2.hidden = NO;
            self.addressView3.hidden = YES;
            self.addressView4.hidden = YES;
            self.addressView5.hidden = YES;
            break;
        case 3:
            [self.addressSclVw setHidden:NO];
            [self.waitTimer invalidate];
            
            self.addressView1.hidden = NO;
            self.addressView2.hidden = NO;
            self.addressView3.hidden = NO;
            self.addressView4.hidden = YES;
            self.addressView5.hidden = YES;
            break;
        case 4:
            [self.addressSclVw setHidden:NO];
            [self.waitTimer invalidate];
            
            self.addressView1.hidden = NO;
            self.addressView2.hidden = NO;
            self.addressView3.hidden = NO;
            self.addressView4.hidden = NO;
            self.addressView5.hidden = YES;
            break;
        case 5:
            [self.waitTimer invalidate];

            self.addressView1.hidden = NO;
            self.addressView2.hidden = NO;
            self.addressView3.hidden = NO;
            self.addressView4.hidden = NO;
            self.addressView5.hidden = NO;
            
            [self.addressSclVw setHidden:NO];
            break;
            
        default:
            break;
    }
    
    for (int i=0; i<[addressArray count]; i++) {
        if (i==0) {
            self.nickNameLbl1.text = [NSString stringWithFormat:@"%@",[[addressArray objectAtIndex:i] valueForKey:@"label"]];
            self.flatNumLbl1.text = [NSString stringWithFormat:@"%@, %@",[[addressArray objectAtIndex:i] valueForKey:@"door_no"],[[addressArray objectAtIndex:i] valueForKey:@"street_name"]];
            self.landmarkLbl1.text = [NSString stringWithFormat:@"%@",[[addressArray objectAtIndex:i] valueForKey:@"landmark"]];
            
            NSArray *arr = [[[[addressArray objectAtIndex:i] valueForKey:@"locality"] capitalizedString] componentsSeparatedByString:@","];
            NSString *localityStr;
            
            for (int i=0; i<[arr count]; i++) {
                if (i==0) {
                    localityStr = [arr objectAtIndex:i];
                }
                else {
                    NSString *subStr = [arr objectAtIndex:i];
                    localityStr = [[subStr substringFromIndex:1] stringByAppendingString:[NSString stringWithFormat:@", %@",localityStr]];
                }
            }

            self.localityLbl1.text = [NSString stringWithFormat:@"%@, %@",localityStr,[[[addressArray objectAtIndex:i] valueForKey:@"city"] capitalizedString]];
            
            [self.addressView1 setTag:[[[addressArray objectAtIndex:i] valueForKey:@"address_id"] integerValue]];
            [USER_DEFAULTS setObject:[[addressArray objectAtIndex:i] valueForKey:@"address_id"] forKey:PARAM_ADDRESS_ID_1];
        }
        else if (i==1) {
            self.nickNameLbl2.text = [NSString stringWithFormat:@"%@",[[addressArray objectAtIndex:i] valueForKey:@"label"]];
            self.flatNumLbl2.text = [NSString stringWithFormat:@"%@, %@",[[addressArray objectAtIndex:i] valueForKey:@"door_no"],[[addressArray objectAtIndex:i] valueForKey:@"street_name"]];
            self.landmarkLbl2.text = [NSString stringWithFormat:@"%@",[[addressArray objectAtIndex:i] valueForKey:@"landmark"]];
           
            NSArray *arr = [[[[addressArray objectAtIndex:i] valueForKey:@"locality"] capitalizedString] componentsSeparatedByString:@","];
            NSString *localityStr;
            
            for (int i=0; i<[arr count]; i++) {
                if (i==0) {
                    localityStr = [arr objectAtIndex:i];
                }
                else {
                    NSString *subStr = [arr objectAtIndex:i];
                    localityStr = [[subStr substringFromIndex:1] stringByAppendingString:[NSString stringWithFormat:@", %@",localityStr]];
                }
            }
            
            self.localityLbl2.text = [NSString stringWithFormat:@"%@, %@",localityStr,[[[addressArray objectAtIndex:i] valueForKey:@"city"] capitalizedString]];
            
            [self.addressView2 setTag:[[[addressArray objectAtIndex:i] valueForKey:@"address_id"] integerValue]];
            [USER_DEFAULTS setObject:[[addressArray objectAtIndex:i] valueForKey:@"address_id"] forKey:PARAM_ADDRESS_ID_2];
        }
        else if (i==2) {
            self.nickNameLbl3.text = [NSString stringWithFormat:@"%@",[[addressArray objectAtIndex:i] valueForKey:@"label"]];
            self.flatNumLbl3.text = [NSString stringWithFormat:@"%@, %@",[[addressArray objectAtIndex:i] valueForKey:@"door_no"],[[addressArray objectAtIndex:i] valueForKey:@"street_name"]];
            self.landmarkLbl3.text = [NSString stringWithFormat:@"%@",[[addressArray objectAtIndex:i] valueForKey:@"landmark"]];
            
            NSArray *arr = [[[[addressArray objectAtIndex:i] valueForKey:@"locality"] capitalizedString] componentsSeparatedByString:@","];
            NSString *localityStr;
            
            for (int i=0; i<[arr count]; i++) {
                if (i==0) {
                    localityStr = [arr objectAtIndex:i];
                }
                else {
                    NSString *subStr = [arr objectAtIndex:i];
                    localityStr = [[subStr substringFromIndex:1] stringByAppendingString:[NSString stringWithFormat:@", %@",localityStr]];
                }
            }
            
            self.localityLbl3.text = [NSString stringWithFormat:@"%@, %@",localityStr,[[[addressArray objectAtIndex:i] valueForKey:@"city"] capitalizedString]];
            
            [self.addressView3 setTag:[[[addressArray objectAtIndex:i] valueForKey:@"address_id"] integerValue]];
            [USER_DEFAULTS setObject:[[addressArray objectAtIndex:i] valueForKey:@"address_id"] forKey:PARAM_ADDRESS_ID_3];
        }
        else if (i==3) {
            self.nickNameLbl4.text = [NSString stringWithFormat:@"%@",[[addressArray objectAtIndex:i] valueForKey:@"label"]];
            self.flatNumLbl4.text = [NSString stringWithFormat:@"%@, %@",[[addressArray objectAtIndex:i] valueForKey:@"door_no"],[[addressArray objectAtIndex:i] valueForKey:@"street_name"]];
            self.landmarkLbl4.text = [NSString stringWithFormat:@"%@",[[addressArray objectAtIndex:i] valueForKey:@"landmark"]];
            
            NSArray *arr = [[[[addressArray objectAtIndex:i] valueForKey:@"locality"] capitalizedString] componentsSeparatedByString:@","];
            NSString *localityStr;
            
            for (int i=0; i<[arr count]; i++) {
                if (i==0) {
                    localityStr = [arr objectAtIndex:i];
                }
                else {
                    NSString *subStr = [arr objectAtIndex:i];
                    localityStr = [[subStr substringFromIndex:1] stringByAppendingString:[NSString stringWithFormat:@", %@",localityStr]];
                }
            }
            
            self.localityLbl4.text = [NSString stringWithFormat:@"%@, %@",localityStr,[[[addressArray objectAtIndex:i] valueForKey:@"city"] capitalizedString]];
            
            [self.addressView4 setTag:[[[addressArray objectAtIndex:i] valueForKey:@"address_id"] integerValue]];
            [USER_DEFAULTS setObject:[[addressArray objectAtIndex:i] valueForKey:@"address_id"] forKey:PARAM_ADDRESS_ID_4];
        }
        else if (i==4) {
            self.nickNameLbl5.text = [NSString stringWithFormat:@"%@",[[addressArray objectAtIndex:i] valueForKey:@"label"]];
            self.flatNumLbl5.text = [NSString stringWithFormat:@"%@, %@",[[addressArray objectAtIndex:i] valueForKey:@"door_no"],[[addressArray objectAtIndex:i] valueForKey:@"street_name"]];
            self.landmarkLbl5.text = [NSString stringWithFormat:@"%@",[[addressArray objectAtIndex:i] valueForKey:@"landmark"]];
            
            NSArray *arr = [[[[addressArray objectAtIndex:i] valueForKey:@"locality"] capitalizedString] componentsSeparatedByString:@","];
            NSString *localityStr;
            
            for (int i=0; i<[arr count]; i++) {
                if (i==0) {
                    localityStr = [arr objectAtIndex:i];
                }
                else {
                    NSString *subStr = [arr objectAtIndex:i];
                    localityStr = [[subStr substringFromIndex:1] stringByAppendingString:[NSString stringWithFormat:@", %@",localityStr]];
                }
            }
            
            self.localityLbl5.text = [NSString stringWithFormat:@"%@, %@",localityStr,[[[addressArray objectAtIndex:i] valueForKey:@"city"] capitalizedString]];
            
            [self.addressView5 setTag:[[[addressArray objectAtIndex:i] valueForKey:@"address_id"] integerValue]];
            [USER_DEFAULTS setObject:[[addressArray objectAtIndex:i] valueForKey:@"address_id"] forKey:PARAM_ADDRESS_ID_5];
        }
    }
}

-(IBAction)deleteAddress:(id)sender {
    NSString *address_id = [NSString stringWithFormat:@"%ld",(long)[(UIButton *)sender superview].tag];
    
    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
    
    NSString *userID = [USER_DEFAULTS objectForKey:PARAM_USER_ID];
    NSString *usertoken = [USER_DEFAULTS objectForKey:PARAM_USER_TOKEN];
    
    [dictParams setObject:userID forKey:PARAM_USER_ID];
    [dictParams setObject:usertoken forKey:PARAM_USER_TOKEN];
    [dictParams setObject:address_id forKey:PARAM_ADDRESS_ID];
    
    [self resetDeleteLayout];
    
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Updating your address list..."];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_DELETE_ADDRESS withParamData:dictParams withBlock:^(id response, NSError *error) {
        if([[response valueForKey:@"status"] boolValue] == true) {
            [USER_DEFAULTS removeObjectForKey:PARAM_ADDRESS_ID];
            [self getSavedAddress];
        }
        else {
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[response valueForKey:@"statusMessage"]];
        }
    }];
}

- (IBAction)selectAddressEvent:(id)sender {
    self.selectionView1.backgroundColor = [UIColor colorWithRed:0.6039 green:0.6039 blue:0.6039 alpha:1.0];
    //[UIColor colorWithRed:0.1774 green:0.6038 blue:1.0 alpha:1.0];
    self.selectionView2.backgroundColor = [UIColor colorWithRed:0.6039 green:0.6039 blue:0.6039 alpha:1.0];
    //[UIColor colorWithRed:0.1774 green:0.6038 blue:1.0 alpha:1.0];
    self.selectionView3.backgroundColor = [UIColor colorWithRed:0.6039 green:0.6039 blue:0.6039 alpha:1.0];
    //[UIColor colorWithRed:0.1774 green:0.6038 blue:1.0 alpha:1.0];
    self.selectionView4.backgroundColor = [UIColor colorWithRed:0.6039 green:0.6039 blue:0.6039 alpha:1.0];
    //[UIColor colorWithRed:0.1774 green:0.6038 blue:1.0 alpha:1.0];
    self.selectionView5.backgroundColor = [UIColor colorWithRed:0.6039 green:0.6039 blue:0.6039 alpha:1.0];
    //[UIColor colorWithRed:0.1774 green:0.6038 blue:1.0 alpha:1.0];
    
    UIView *superView = [(UIButton *)sender superview];
    [USER_DEFAULTS setObject:[NSString stringWithFormat:@"%ld",(long)superView.tag] forKey:PARAM_ADDRESS_ID];
    
    UILabel *nickNameLbl = [superView viewWithTag:100];
    [USER_DEFAULTS setObject:nickNameLbl.text forKey:PARAM_NICKNAME];

    if ([superView isEqual:self.addressView1]) {
        self.selectionView1.backgroundColor = [UIColor colorWithRed:0.2227 green:0.8318 blue:0.4907 alpha:1.0];
    }
    else if ([superView isEqual:self.addressView2]) {
        self.selectionView2.backgroundColor = [UIColor colorWithRed:0.2227 green:0.8318 blue:0.4907 alpha:1.0];
    }
    else if ([superView isEqual:self.addressView3]) {
        self.selectionView3.backgroundColor = [UIColor colorWithRed:0.2227 green:0.8318 blue:0.4907 alpha:1.0];
    }
    else if ([superView isEqual:self.addressView4]) {
        self.selectionView4.backgroundColor = [UIColor colorWithRed:0.2227 green:0.8318 blue:0.4907 alpha:1.0];
    }
    else if ([superView isEqual:self.addressView5]) {
        self.selectionView5.backgroundColor = [UIColor colorWithRed:0.2227 green:0.8318 blue:0.4907 alpha:1.0];
    }
    
    if ([self.addressView5 isHidden]) {
        [UIView animateWithDuration:0.5f animations:^{
            CGRect newRect = self.doneBtn.frame;
            newRect.origin.y = self.view.bounds.size.height - self.doneBtn.frame.size.height - 20;
            [self.doneBtn setFrame:newRect];
        } completion:^(BOOL finished) {
            [self.addressSclVw setContentOffset:CGPointZero animated:YES];
            [self.addressSclVw setScrollEnabled:NO];
        }];
    }
    else {
        [UIView animateWithDuration:0.5f animations:^{
            CGRect newRect = self.doneBtn.frame;
            newRect.origin.y = self.addressView5.frame.origin.y + self.addressView5.frame.size.height + 20;
            [self.doneBtn setFrame:newRect];
        } completion:^(BOOL finished) {
            [self.addressSclVw setScrollEnabled:YES];
            [self.addressSclVw setContentSize:CGSizeMake(self.view.bounds.size.width, self.doneBtn.frame.origin.y+self.doneBtn.frame.size.height+10)];
        }];
    }

    [self resetDeleteLayout];
}

-(void)resetDeleteLayout {
    if ([self.selectionBtn1.backgroundColor isEqual:[UIColor lightTextColor]]) {
        [UIView animateWithDuration:0.5f animations:^{
            CGRect newRect = self.selectionBtn1.frame;
            newRect.size.width += 64;
            
            [self.deleteBtn1 setFrame:newRect];
            [self.selectionBtn1 setBackgroundColor:[UIColor clearColor]];
            
            [UIView animateWithDuration:0.5f animations:^{
                if ([self.addressView5 isHidden]) {
                    CGRect newRect = self.doneBtn.frame;
                    newRect.origin.y = self.view.bounds.size.height - self.doneBtn.frame.size.height - 20;
                    [self.doneBtn setFrame:newRect];
                }
                else {
                    [self.doneBtn setHidden:NO];
                }
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.5f animations:^{
                    [self.editBtn1 setHidden:NO];
                }];
            }];
            
        }];
    }
    else if ([self.selectionBtn2.backgroundColor isEqual:[UIColor lightTextColor]]) {
        [UIView animateWithDuration:0.5f animations:^{
            CGRect newRect = self.selectionBtn2.frame;
            newRect.size.width += 64;
            
            [self.deleteBtn2 setFrame:newRect];
            [self.selectionBtn2 setBackgroundColor:[UIColor clearColor]];
            
            [UIView animateWithDuration:0.5f animations:^{
                if ([self.addressView5 isHidden]) {
                    CGRect newRect = self.doneBtn.frame;
                    newRect.origin.y = self.view.bounds.size.height - self.doneBtn.frame.size.height - 20;
                    [self.doneBtn setFrame:newRect];
                }
                else {
                    [self.doneBtn setHidden:NO];
                }
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.5f animations:^{
                    [self.editBtn2 setHidden:NO];
                }];
            }];
            
        }];
    }
    else if ([self.selectionBtn3.backgroundColor isEqual:[UIColor lightTextColor]]) {
        [UIView animateWithDuration:0.5f animations:^{
            CGRect newRect = self.selectionBtn3.frame;
            newRect.size.width += 64;
            
            [self.deleteBtn3 setFrame:newRect];
            [self.selectionBtn3 setBackgroundColor:[UIColor clearColor]];
            
            [UIView animateWithDuration:0.5f animations:^{
                if ([self.addressView5 isHidden]) {
                    CGRect newRect = self.doneBtn.frame;
                    newRect.origin.y = self.view.bounds.size.height - self.doneBtn.frame.size.height - 20;
                    [self.doneBtn setFrame:newRect];
                }
                else {
                    [self.doneBtn setHidden:NO];
                }
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.5f animations:^{
                    [self.editBtn3 setHidden:NO];
                }];
            }];
            
        }];
    }
    else if ([self.selectionBtn4.backgroundColor isEqual:[UIColor lightTextColor]]) {
        [UIView animateWithDuration:0.5f animations:^{
            CGRect newRect = self.selectionBtn4.frame;
            newRect.size.width += 64;
            
            [self.deleteBtn4 setFrame:newRect];
            [self.selectionBtn4 setBackgroundColor:[UIColor clearColor]];
            
            [UIView animateWithDuration:0.5f animations:^{
                if ([self.addressView5 isHidden]) {
                    CGRect newRect = self.doneBtn.frame;
                    newRect.origin.y = self.view.bounds.size.height - self.doneBtn.frame.size.height - 20;
                    [self.doneBtn setFrame:newRect];
                }
                else {
                    [self.doneBtn setHidden:NO];
                }
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.5f animations:^{
                    [self.editBtn4 setHidden:NO];
                }];
            }];
            
        }];
    }
    else if ([self.selectionBtn5.backgroundColor isEqual:[UIColor lightTextColor]]) {
        [UIView animateWithDuration:0.5f animations:^{
            CGRect newRect = self.selectionBtn5.frame;
            newRect.size.width += 64;
            
            [self.deleteBtn5 setFrame:newRect];
            [self.selectionBtn5 setBackgroundColor:[UIColor clearColor]];
            
            [UIView animateWithDuration:0.5f animations:^{
                [self.doneBtn setHidden:NO];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.5f animations:^{
                    [self.editBtn5 setHidden:NO];
                }];
            }];
            
        }];
    }
}

- (IBAction)singleDeleteLayout:(id)sender {
    UIButton *editBtn = (UIButton *)sender;
    
    [self resetDeleteLayout];
    
    if ([editBtn tag] == 1) {
        [UIView animateWithDuration:0.5f animations:^{
            CGRect newRect = self.deleteBtn1.frame;
            newRect.origin.x -= 64;
            
            [self.deleteBtn1 setFrame:newRect];
            [self.editBtn1 setHidden:YES];
            [self.selectionBtn1 setBackgroundColor:[UIColor lightTextColor]];
            
            [UIView animateWithDuration:0.5f animations:^{
                if ([self.addressView5 isHidden]) {
                    CGRect newRect = self.doneBtn.frame;
                    newRect.origin.y = self.view.bounds.size.height + self.doneBtn.frame.size.height + 20;
                    [self.doneBtn setFrame:newRect];
                }
                else {
                    [self.doneBtn setHidden:YES];
                }
            } completion:nil];
            
        }];
    }
    else if ([editBtn tag] == 2) {
        [UIView animateWithDuration:0.5f animations:^{
            CGRect newRect = self.deleteBtn2.frame;
            newRect.origin.x -= 64;
            
            [self.deleteBtn2 setFrame:newRect];
            [self.editBtn2 setHidden:YES];
            [self.selectionBtn2 setBackgroundColor:[UIColor lightTextColor]];
            
            [UIView animateWithDuration:0.5f animations:^{
                if ([self.addressView5 isHidden]) {
                    CGRect newRect = self.doneBtn.frame;
                    newRect.origin.y = self.view.bounds.size.height + self.doneBtn.frame.size.height + 20;
                    [self.doneBtn setFrame:newRect];
                }
                else {
                    [self.doneBtn setHidden:YES];
                }
            } completion:nil];
            
        }];
    }
    else if ([editBtn tag] == 3) {
        [UIView animateWithDuration:0.5f animations:^{
            CGRect newRect = self.deleteBtn3.frame;
            newRect.origin.x -= 64;
            
            [self.deleteBtn3 setFrame:newRect];
            [self.editBtn3 setHidden:YES];
            [self.selectionBtn3 setBackgroundColor:[UIColor lightTextColor]];
            
            [UIView animateWithDuration:0.5f animations:^{
                if ([self.addressView5 isHidden]) {
                    CGRect newRect = self.doneBtn.frame;
                    newRect.origin.y = self.view.bounds.size.height + self.doneBtn.frame.size.height + 20;
                    [self.doneBtn setFrame:newRect];
                }
                else {
                    [self.doneBtn setHidden:YES];
                }
            } completion:nil];
            
        }];
    }
    else if ([editBtn tag] == 4) {
        [UIView animateWithDuration:0.5f animations:^{
            CGRect newRect = self.deleteBtn4.frame;
            newRect.origin.x -= 64;
            
            [self.deleteBtn4 setFrame:newRect];
            [self.editBtn4 setHidden:YES];
            [self.selectionBtn4 setBackgroundColor:[UIColor lightTextColor]];
            
            [UIView animateWithDuration:0.5f animations:^{
                if ([self.addressView5 isHidden]) {
                    CGRect newRect = self.doneBtn.frame;
                    newRect.origin.y = self.view.bounds.size.height + self.doneBtn.frame.size.height + 20;
                    [self.doneBtn setFrame:newRect];
                }
                else {
                    [self.doneBtn setHidden:YES];
                }
            } completion:nil];
            
        }];
    }
    else if ([editBtn tag] == 5) {
        [UIView animateWithDuration:0.5f animations:^{
            CGRect newRect = self.deleteBtn5.frame;
            newRect.origin.x -= 64;
            
            [self.deleteBtn5 setFrame:newRect];
            [self.editBtn5 setHidden:YES];
            [self.selectionBtn5 setBackgroundColor:[UIColor lightTextColor]];
            
            [UIView animateWithDuration:0.5f animations:^{
                [self.doneBtn setHidden:YES];
            } completion:nil];
            
        }];
    }
    
}

- (IBAction)showProductsView:(id)sender {
    // Konotor - Save User Data
    NSString *username = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_NICKNAME]];
    [Konotor setUserName:username];
    
    if ([USER_DEFAULTS objectForKey:PARAM_EMAIL]) {
        NSString *userEmail = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_EMAIL]];
        [Konotor setUserEmail:userEmail];
    }
    
    NSString *userIdentifier = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_USER_ID]];
    [Konotor setUserIdentifier:[NSString stringWithFormat:@"%@-%@",username,userIdentifier]];
    
    [Konotor setCustomUserProperty:[USER_DEFAULTS objectForKey:PARAM_CITY] forKey:@"city"];
    [Konotor setCustomUserProperty:[USER_DEFAULTS objectForKey:PARAM_LOCALITY] forKey:@"locality"];
    [Konotor setCustomUserProperty:@"true" forKey:@"loggedIn"];
    [Konotor setCustomUserProperty:@"3" forKey:@"transactionCount"];

    [self performSegueWithIdentifier:@"goToOrderSummary" sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    // TODO: Move this to where you establish a user session
    [self logUser];
}

- (void) logUser {
    // TODO: Use the current user's information
    // You can call any combination of these three methods
    [CrashlyticsKit setUserIdentifier:[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_USER_ID]]];
    [CrashlyticsKit setUserEmail:[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_EMAIL]]];
    [CrashlyticsKit setUserName:[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_NICKNAME]]];

    //CleverTap - Create Profile
    NSNumberFormatter *numFormat = [[NSNumberFormatter alloc] init];
    numFormat.numberStyle = NSNumberFormatterNoStyle;
    NSNumber *myNumber = [numFormat numberFromString:[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PHONE_NUM]]];
    
    NSDictionary *profile = @{@"Name" : [NSString stringWithFormat:@"%@(iOS)",[USER_DEFAULTS objectForKey:PARAM_NICKNAME]],  //String
                              @"Identity" : [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_USER_ID]],  //String or number
                              @"Phone" : myNumber,  //Phone(exclude the country code)
                              @"MSG-email" : @YES,  //Enable email notifications
                              @"MSG-push" : @YES,  //Enable push notifications
                              @"MSG-sms" : @YES  //Enable SMS notifications
                              };
    [[CleverTap push] profile:profile];
    [[CleverTap push] profile:@{@"Locality":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_LOCALITY]]}];
}


- (IBAction)backEvent:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
