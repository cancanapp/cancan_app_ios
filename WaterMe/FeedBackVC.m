//
//  FeedBackVC.m
//  CanCan
//
//  Created by AC on 12/08/15.
//  Copyright (c) 2015 CanCan Pvt. Ltd.,. All rights reserved.
//

#import "FeedBackVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AFNHelper.h"
#import <CleverTapSDK/CleverTap.h>

@interface FeedBackVC ()

@property (strong, nonatomic) IBOutlet UILabel *productsLbl;
@property (strong, nonatomic) IBOutlet UILabel *amontLbl;
@property (strong, nonatomic) IBOutlet UIImageView *smiley1;
@property (strong, nonatomic) IBOutlet UIImageView *smiley2;
@property (strong, nonatomic) IBOutlet UIImageView *smiley3;
@property (strong, nonatomic) IBOutlet UIImageView *smiley4;
@property (strong, nonatomic) IBOutlet UIImageView *smiley5;
@property (strong, nonatomic) IBOutlet UIView *smileyView;

@property (strong, nonatomic) IBOutlet UITextView *feedbackTxtVw;

@end

@implementation FeedBackVC
@synthesize dataArr;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [USER_DEFAULTS removeObjectForKey:PARAM_ORDER_RATING];
    
    [self setValuesOfOrder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setValuesOfOrder {
    [USER_DEFAULTS setObject:[dataArr valueForKey:PARAM_ORDER_ID] forKey:PARAM_ORDER_ID];
    
    NSString *productStr;
    
    if ([[dataArr valueForKey:@"no_of_cans"] isEqualToString:@"1"]) {
        productStr = [NSString stringWithFormat:@"%@ - %@ CAN",[dataArr valueForKey:@"product_name"],[dataArr valueForKey:@"no_of_cans"]];
    }
    else {
        productStr = [NSString stringWithFormat:@"%@ - %@ CANS",[dataArr valueForKey:@"product_name"],[dataArr valueForKey:@"no_of_cans"]];
    }
    
    self.productsLbl.text = productStr;
    self.amontLbl.text = [NSString stringWithFormat:@"₹%@",[dataArr valueForKey:@"amount"]];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [USER_DEFAULTS removeObjectForKey:PARAM_ORDER_RATING];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)selectExperienceEvent:(id)sender {
    UIButton *experienceBtn = (UIButton *)sender;

    self.smiley1.image = [UIImage imageNamed:@"extremelySatisfied_unSelected"];
    self.smiley2.image = [UIImage imageNamed:@"satisfied_unSelected"];
    self.smiley3.image = [UIImage imageNamed:@"ok_unSelected"];
    self.smiley4.image = [UIImage imageNamed:@"disSatisfied_unSelected"];
    self.smiley5.image = [UIImage imageNamed:@"extremelySad_unSelected"];

    if ([experienceBtn tag] == 1) {
        self.smiley1.image = [UIImage imageNamed:@"extremelySatisfied_selected"];
        self.feedbackTxtVw.text = @"Tell us, what you love about us!!!";
    }
    else if ([experienceBtn tag] == 2) {
        self.smiley2.image = [UIImage imageNamed:@"satisfied_selected"];
        self.feedbackTxtVw.text = @"Tell us, what you love about us!!!";
    }
    else if ([experienceBtn tag] == 3) {
        self.smiley3.image = [UIImage imageNamed:@"ok_selected"];
        self.feedbackTxtVw.text = @"We're sorry, tell us what didn't work";
    }
    else if ([experienceBtn tag] == 4) {
        self.smiley4.image = [UIImage imageNamed:@"disSatisfied_selected"];
        self.feedbackTxtVw.text = @"We're sorry, tell us what didn't work";
    }
    else if ([experienceBtn tag] == 5) {
        self.smiley5.image = [UIImage imageNamed:@"extremelySad_selected"];
        self.feedbackTxtVw.text = @"We're sorry, tell us what didn't work";
    }

    self.feedbackTxtVw.textColor = [UIColor darkGrayColor];
    self.feedbackTxtVw.textAlignment = NSTextAlignmentCenter;
    
    [USER_DEFAULTS setObject:[NSString stringWithFormat:@"%d",[experienceBtn tag]] forKey:PARAM_ORDER_RATING];
    [USER_DEFAULTS setObject:@"" forKey:PARAM_ORDER_FEEDBACK];

    [self.view endEditing:YES];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    self.feedbackTxtVw.textColor = [UIColor blackColor];
    self.feedbackTxtVw.textAlignment = NSTextAlignmentLeft;
    self.feedbackTxtVw.text = @"";

    return YES;
}

-(void)sendFeedBack {
    if (![USER_DEFAULTS objectForKey:PARAM_ORDER_RATING]) {
        [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Please tell us your experience with CanCan"];
        
        return;
    }
    
    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
    
    [USER_DEFAULTS synchronize];
    
    NSString *userID = [USER_DEFAULTS objectForKey:PARAM_USER_ID];
    NSString *usertoken = [USER_DEFAULTS objectForKey:PARAM_USER_TOKEN];
    NSString *userRating = [USER_DEFAULTS objectForKey:PARAM_ORDER_RATING];
    NSString *userOrderID = [USER_DEFAULTS objectForKey:PARAM_ORDER_ID];
    
    if ([self.feedbackTxtVw.textColor isEqual:[UIColor darkGrayColor]]) {
        [USER_DEFAULTS setObject:@"" forKey:PARAM_ORDER_FEEDBACK];
    }
    else {
        [USER_DEFAULTS setObject:self.feedbackTxtVw.text forKey:PARAM_ORDER_FEEDBACK];
    }
    
    NSString *userFeedback = [USER_DEFAULTS objectForKey:PARAM_ORDER_FEEDBACK];
    
    [dictParams setObject:userID forKey:PARAM_USER_ID];
    [dictParams setObject:usertoken forKey:PARAM_USER_TOKEN];
    [dictParams setObject:userRating forKey:PARAM_ORDER_RATING];
    [dictParams setObject:userOrderID forKey:PARAM_ORDER_ID];
    [dictParams setObject:userFeedback forKey:PARAM_ORDER_FEEDBACK];

    [self.view endEditing:YES];
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Processing..."];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_SAVE_FEEDBACK withParamData:dictParams withBlock:^(id response, NSError *error) {
        if([[response valueForKey:@"status"] boolValue] == true) {
            //[[AppDelegate sharedAppDelegate] hideLoadingView];
            
            [self dismissViewControllerAnimated:YES completion:^{
                //[[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[response valueForKey:@"statusMessage"]];
                
                // CleverTap - Added Feedback
                NSDictionary *props = @{@"Product name":[dataArr valueForKey:@"product_name"],
                                        @"Price":[dataArr valueForKey:@"amount"],
                                        @"Currency":@"INR",
                                        @"Quantity":[dataArr valueForKey:@"no_of_cans"],
                                        @"Rating":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_ORDER_RATING]],
                                        @"FeedBack":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_ORDER_FEEDBACK]]};
                [[CleverTap push] eventName:@"Feedback" eventProps:props];
            }];
        }
        else {
            [[AppDelegate sharedAppDelegate] hideLoadingView];

            [self dismissViewControllerAnimated:YES completion:^{
                //[[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[response valueForKey:@"statusMessage"]];
            }];
        }
    }];
}

- (IBAction)backEvent:(id)sender {
        [self sendFeedBack];
}

@end
