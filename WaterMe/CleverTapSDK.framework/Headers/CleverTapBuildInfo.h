//
//  CleverTapBuildInfo.h
//  CleverTapSDK
//
//  Copyright (c) 2015 WizRocket. All rights reserved.
//

#define WR_SDK_VERSION 1
#define WR_SDK_REVISION 9546
#define WR_SDK_RELEASE_DATE 20150903
