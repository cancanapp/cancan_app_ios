//
//  FeedBackVC.h
//  CanCan
//
//  Created by AC on 12/08/15.
//  Copyright (c) 2015 CanCan Pvt. Ltd.,. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedBackVC : UIViewController <UITextViewDelegate>

@property (strong, nonatomic) NSArray *dataArr;

@end
