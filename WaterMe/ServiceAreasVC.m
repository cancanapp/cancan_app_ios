//
//  ServiceAreasVC.m
//  WaterMe
//
//  Created by Dev on 19/07/15.
//  Copyright (c) 2015 WaterMe. All rights reserved.
//

#import "ServiceAreasVC.h"
#import "Constants.h"

@interface ServiceAreasVC ()


@property (weak, nonatomic) IBOutlet UILabel *headerlbl;
@property (weak, nonatomic) IBOutlet UITableView *locationTableView;

@end

@implementation ServiceAreasVC
@synthesize locationsArr;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.headerlbl setText:[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:@"serviceAreaHeader"]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Tableview Delegate

-(void)viewDidLayoutSubviews
{
    if ([self.locationTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.locationTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.locationTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.locationTableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"locationCell"];
    
    if(cell == nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    NSString *formatedStr= [NSString stringWithFormat:@"%@",[[locationsArr objectAtIndex:indexPath.row] capitalizedString]];
    cell.textLabel.font = [UIFont systemFontOfSize:16.0f weight:UIFontWeightLight];
    
    cell.textLabel.text=formatedStr;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.headerlbl.text isEqualToString:@"CHOOSE YOUR CITY"]) {
        [USER_DEFAULTS setObject:[locationsArr objectAtIndex:indexPath.row] forKey:PARAM_CITY];
    }
    else if ([self.headerlbl.text isEqualToString:@"CHOOSE YOUR LOCALITY"]) {
        [USER_DEFAULTS setObject:[locationsArr objectAtIndex:indexPath.row] forKey:PARAM_LOCALITY];
    }
    
    [USER_DEFAULTS synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [locationsArr count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.5f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerVw=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 0.5)];
    [footerVw setBackgroundColor:[UIColor lightGrayColor]];
    
    return footerVw;
}

- (IBAction)closeEvent:(id)sender {
    if ([self.headerlbl.text isEqualToString:@"CHOOSE YOUR CITY"]) {
        [USER_DEFAULTS setObject:@"" forKey:PARAM_CITY];
    }
    else if ([self.headerlbl.text isEqualToString:@"CHOOSE YOUR LOCALITY"]) {
        [USER_DEFAULTS setObject:@"" forKey:PARAM_LOCALITY];
    }

    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
