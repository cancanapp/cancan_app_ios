//
//  ContentsVC.m
//  WaterMe
//
//  Created by Dev on 14/05/15.
//  Copyright (c) 2015 WaterMe. All rights reserved.
//

#import "ContentsVC.h"
#import "SDWebImage/UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "AFHTTPRequestOperationManager.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import <Parse/Parse.h>
#import "SWRevealViewController.h"
#import <CleverTapSDK/CleverTap.h>
#import "UICountingLabel.h"
#import "FeedBackVC.h"
#import "Branch.h"

@interface ContentsVC () <SWRevealViewControllerDelegate> {
    int prodCount, prodPageNum;
    float prodPrice,totalPrice;
    NSArray *productImgArr, *productPriceArr, *productIDArr, *productNameArr, *productDepositArr, *productTimeslotArr, *productMinOrderArr;
    NSMutableArray *timeSlotArr, *timeSlotIdArr, *timeSlotStartArr;
    UIButton *btn;
}

@property (weak, nonatomic) IBOutlet UIPageControl *contentPages;
@property (weak, nonatomic) IBOutlet UIScrollView *contentSclVw;
@property (weak, nonatomic) IBOutlet UILabel *productNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *productCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *cansLbl;
@property (weak, nonatomic) IBOutlet UILabel *currencyTypeLbl;
@property (weak, nonatomic) IBOutlet UIView *deliveryTypeView;
@property (weak, nonatomic) IBOutlet UIButton *addProdBtn;
@property (weak, nonatomic) IBOutlet UIButton *removeProdBtn;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *datePickerBgView;
@property (weak, nonatomic) IBOutlet UIView *dateSelectionView;
@property (weak, nonatomic) IBOutlet UIButton *selectTimeBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelTimeSelectionBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectDate;
@property (weak, nonatomic) IBOutlet UISegmentedControl *dateSegmentControl;
@property (weak, nonatomic) IBOutlet UIPickerView *timePickerView;
@property (weak, nonatomic) IBOutlet UIView *btnBgView;
@property (weak, nonatomic) IBOutlet UIView *chooseDeliverySlotView;

@property (weak, nonatomic) IBOutlet UIButton *revealButtonItem;

@property (weak, nonatomic) IBOutlet UICountingLabel *productTotalLbl;

@end

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

@implementation ContentsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.dateSelectionView setHidden:YES];
    [self createDeliveryBtns];
    
    if([USER_DEFAULTS boolForKey:@"isLogin"]) {
        [self getLastOrderStatus];
    }    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self.navigationController.navigationBar setHidden:YES];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    timeSlotArr = [[NSMutableArray alloc]initWithCapacity:0];
    timeSlotIdArr = [[NSMutableArray alloc]initWithCapacity:0];
    timeSlotStartArr = [[NSMutableArray alloc]initWithCapacity:0];
    
    [self customSetup];
    [self getListOfProducts];
}

- (void)customSetup
{
    self.revealViewController.delegate = self;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController) {
        [self.revealButtonItem addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position {
    if(position == FrontViewPositionLeft) {
        [btn removeFromSuperview];
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    } else {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
        
        btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        [btn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [btn setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:btn];
        [self.view bringSubviewToFront:btn];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.contentSclVw.delegate = nil;
    
    // CleverTap - Preferred Time Slot
    NSDictionary *props = @{@"Time Slot":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_SLOT_TEXT]]};
    [[CleverTap push] eventName:@"Preferred Time Slot" eventProps:props];
    
    // CleverTap - Product Viewed
    NSDictionary *productProps = @{@"Product name":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_NAME]],
                                   @"Price":[NSString stringWithFormat:@"%.0f",prodPrice],
                                   @"Currency":@"INR",
                                   @"Area Name":[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_LOCALITY]]]};
    [[CleverTap push] eventName:@"Product viewed" eventProps:productProps];
    
    // CleverTap - Add to Cart
    NSDictionary *cartProps = @{@"Product name":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PRODUCT_NAME]],
                                @"Price":[NSString stringWithFormat:@"%.0f",prodPrice],
                                @"Currency":@"INR",
                                @"Quantity":self.productCountLbl.text,
                                @"Area Name":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_LOCALITY]]};
    [[CleverTap push] eventName:@"Add to Cart" eventProps:cartProps];
    
    [USER_DEFAULTS synchronize];
}

-(void)getListOfProducts {
    NSString *url=[NSString stringWithFormat:@"%@services/products",SERVICE_URL];
    
    [[AppDelegate sharedAppDelegate] showLoadingWithTitle:@"Loading Products..."];
    
    NSString *userCity = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_CITY]];
    NSString *userLocality = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_LOCALITY]];

    NSMutableDictionary *dictparams = [[NSMutableDictionary alloc]initWithCapacity:0];
    [dictparams setObject:userCity forKey:PARAM_CITY];
    [dictparams setObject:userLocality forKey:PARAM_LOCALITY];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:url parameters:dictparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if([[responseObject valueForKey:@"status"] boolValue] == true) {
            
            NSDictionary *productsDict = [responseObject valueForKey:@"data"];
            productImgArr = [productsDict valueForKey:@"image_url"];
            productIDArr = [productsDict valueForKey:@"product_id"];
            productNameArr = [productsDict valueForKey:@"product_name"];
            productPriceArr = [productsDict valueForKey:@"price"];
            
            productDepositArr = [productsDict valueForKey:@"refundable_deposit"];
            productTimeslotArr = [productsDict valueForKey:@"time_slot"];
            productMinOrderArr = [productsDict valueForKey:@"minimum_order"];
            
            [self setProductsView];
            
//            [self.productTotalLbl setText:@"40"];
//            [self.productNameLbl setText:@"Indiana Springs"];
//            //[self.productNameLbl setText:@"20L (Quality Tested)"];
//            [self.productCountLbl setText:@"1"];
//            [self.cansLbl setText:@"CAN"];
            
            [[AppDelegate sharedAppDelegate] hideLoadingView];
        }
        else {
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[responseObject valueForKey:@"statusMessage"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // handle Failure of AFHTTPREQUEST
        UIAlertView *alertVw=[[UIAlertView alloc]initWithTitle:@"CanCan" message:@"Please check your Internet connection." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alertVw.tag = 1;
        [alertVw show];
    }];
}

-(void)setProductsView {
    prodCount = [[productMinOrderArr firstObject] integerValue];
    prodPrice = [[productPriceArr firstObject] floatValue];
    
    [self.productTotalLbl setText:[NSString stringWithFormat:@"%.0f",prodPrice*prodCount]];
    [self.productNameLbl setText:[productNameArr firstObject]];
    [self.productCountLbl setText:[NSString stringWithFormat:@"%d",prodCount]];
    [self.cansLbl setText:@"CAN"];
    
    [USER_DEFAULTS setObject:[NSString stringWithFormat:@"%d",prodCount] forKey:PARAM_PRODUCT_COUNT];
    [USER_DEFAULTS setObject:[NSString stringWithFormat:@"%.0f",prodPrice*prodCount] forKey:PARAM_PRODUCT_AMOUNT];
    [USER_DEFAULTS setObject:[productImgArr firstObject] forKey:PARAM_PRODUCT_IMAGE];
    [USER_DEFAULTS setObject:[productIDArr firstObject] forKey:PARAM_PRODUCT_ID];
    [USER_DEFAULTS setObject:[productDepositArr firstObject] forKey:PARAM_PRODUCT_DEPOSIT];
    [USER_DEFAULTS setObject:[productNameArr firstObject] forKey:PARAM_PRODUCT_NAME];
    
    [self.removeProdBtn setUserInteractionEnabled:NO];
    [self.removeProdBtn setAlpha:0.6f];
    
    CGSize size = [[UIScreen mainScreen] bounds].size;
    CGFloat frameX = size.width;
    CGFloat frameY = self.contentSclVw.bounds.size.height; //padding for UIpageControl
    
    //NSArray *imgArr = @[@"Team",@"bisleri",@"aqua"];
    self.contentSclVw.pagingEnabled = YES;
    self.contentSclVw.delegate = self;
    self.contentSclVw.backgroundColor = [UIColor whiteColor];
    self.contentSclVw.showsHorizontalScrollIndicator = NO;
    self.contentSclVw.showsVerticalScrollIndicator = NO;
    self.contentSclVw.contentSize = CGSizeMake(frameX, frameY);
    
    for(int i = 0; i < [productImgArr count]; i++) {
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(frameX *i, 0.00, frameX, frameY)];
        [bgView setBackgroundColor:[UIColor clearColor]];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.00, 0.00, frameX, frameY)];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        
        NSString *imgName = [NSString stringWithFormat:@"%@",[productImgArr objectAtIndex:i]];
        [imageView setImageWithURL:[NSURL URLWithString:imgName] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        //[imageView setImage:[UIImage imageNamed:@"indiana_springs_230x300"]];
        
        [bgView addSubview:imageView];
        
        [self.contentSclVw addSubview:bgView];
    }
    
    self.contentSclVw.contentSize = CGSizeMake(frameX*[productImgArr count], frameY);
    self.contentPages.numberOfPages = self.contentSclVw.contentSize.width/frameX;
    
    // Time-Slot
    [timeSlotArr removeAllObjects];
    [timeSlotIdArr removeAllObjects];
    [timeSlotStartArr removeAllObjects];
    
    for (NSDictionary *dict in [productTimeslotArr firstObject]) {
        [timeSlotIdArr addObject:[dict valueForKey:@"slot"]];
        [timeSlotStartArr addObject:[dict valueForKey:@"start_time"]];
        [timeSlotArr addObject:[dict valueForKey:@"timeslot_interval"]];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    float width = scrollView.frame.size.width;
    float xPos = scrollView.contentOffset.x+10;
    
    //Calculate the page we are on based on x coordinate position and width of scroll view
    prodPageNum = (int)xPos/width;
    
    self.contentPages.currentPage = prodPageNum;
    
    self.productNameLbl.text = [NSString stringWithFormat:@"%@",[productNameArr objectAtIndex:prodPageNum]];
    //[self.productNameLbl setText:@"20L (Quality Tested)"];

    prodCount = [[productMinOrderArr objectAtIndex:prodPageNum] integerValue];
    
    [self.addProdBtn setUserInteractionEnabled:YES];
    [self.addProdBtn setAlpha:1.0f];
    
    [self.removeProdBtn setUserInteractionEnabled:NO];
    [self.removeProdBtn setAlpha:0.6f];
    NSString *countStr = [NSString stringWithFormat:@"%d",prodCount];
    [self.productCountLbl setText:countStr];
    if (prodCount > 1) {
        [self.cansLbl setText:@"CANS"];
    }
    else {
        [self.cansLbl setText:@"CAN"];
    }
    
    prodPrice = [[productPriceArr objectAtIndex:prodPageNum] floatValue];
    float orderPrice = [[productPriceArr objectAtIndex:prodPageNum] floatValue]*prodCount;
    [self.productTotalLbl setText:[NSString stringWithFormat:@"%.0f",orderPrice]];
    
    [USER_DEFAULTS setObject:[NSString stringWithFormat:@"%@",countStr] forKey:PARAM_PRODUCT_COUNT];
    [USER_DEFAULTS setObject:[NSString stringWithFormat:@"%.0f",orderPrice] forKey:PARAM_PRODUCT_AMOUNT];
    [USER_DEFAULTS setObject:[productImgArr objectAtIndex:prodPageNum] forKey:PARAM_PRODUCT_IMAGE];
    [USER_DEFAULTS setObject:[productIDArr objectAtIndex:prodPageNum] forKey:PARAM_PRODUCT_ID];
    [USER_DEFAULTS setObject:[productDepositArr objectAtIndex:prodPageNum] forKey:PARAM_PRODUCT_DEPOSIT];
    [USER_DEFAULTS setObject:[productNameArr objectAtIndex:prodPageNum] forKey:PARAM_PRODUCT_NAME];
    
    // Time-Slot
    [timeSlotArr removeAllObjects];
    [timeSlotIdArr removeAllObjects];
    [timeSlotStartArr removeAllObjects];
    
    for (NSDictionary *dict in [productTimeslotArr objectAtIndex:prodPageNum]) {
        [timeSlotIdArr addObject:[dict valueForKey:@"slot"]];
        [timeSlotStartArr addObject:[dict valueForKey:@"start_time"]];
        [timeSlotArr addObject:[dict valueForKey:@"timeslot_interval"]];
    }
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    self.dateSegmentControl.selectedSegmentIndex = 0;
    [self dateSegmentChanged:nil];
}

- (IBAction)addProductEvent:(id)sender {
    [self.addProdBtn setUserInteractionEnabled:YES];
    [self.addProdBtn setAlpha:1.0f];
    
    prodCount += 1;
    totalPrice = prodPrice*prodCount;
    
    NSString *countStr = [NSString stringWithFormat:@"%d",prodCount];
    [self.productCountLbl setText:countStr];
    [self.cansLbl setText:@"CANS"];
    
    NSString *totalProdPrice = [NSString stringWithFormat:@"%.0f",totalPrice];
    [self.productTotalLbl setText:totalProdPrice];
    
    if (prodCount == 10) {
        [self.addProdBtn setUserInteractionEnabled:NO];
        [self.addProdBtn setAlpha:0.6f];
    }
    
    [USER_DEFAULTS setObject:[NSString stringWithFormat:@"%@",countStr] forKey:PARAM_PRODUCT_COUNT];
    [USER_DEFAULTS setObject:totalProdPrice forKey:PARAM_PRODUCT_AMOUNT];
    
    [self.removeProdBtn setUserInteractionEnabled:YES];
    [self.removeProdBtn setAlpha:1.0f];
}

- (IBAction)removeProductEvent:(id)sender {
    [self.removeProdBtn setUserInteractionEnabled:YES];
    [self.removeProdBtn setAlpha:1.0f];
    
    prodCount -= 1;
    totalPrice = prodPrice*prodCount;
    
    NSString *countStr = [NSString stringWithFormat:@"%d",prodCount];
    [self.productCountLbl setText:countStr];
    
    NSString *totalProdPrice = [NSString stringWithFormat:@"%.0f",totalPrice];
    [self.productTotalLbl setText:totalProdPrice];
    
    if (prodCount == [[productMinOrderArr objectAtIndex:prodPageNum] integerValue]) {
        [self.removeProdBtn setUserInteractionEnabled:NO];
        [self.removeProdBtn setAlpha:0.6f];
        
        if (prodCount > 1)
            [self.cansLbl setText:@"CANS"];
        else
            [self.cansLbl setText:@"CAN"];
    }
    else {
        [self.cansLbl setText:@"CANS"];
    }
    
    [USER_DEFAULTS setObject:[NSString stringWithFormat:@"%@",countStr] forKey:PARAM_PRODUCT_COUNT];
    [USER_DEFAULTS setObject:totalProdPrice forKey:PARAM_PRODUCT_AMOUNT];
    
    [self.addProdBtn setUserInteractionEnabled:YES];
    [self.addProdBtn setAlpha:1.0f];
}


-(void)createDeliveryBtns {
    UIButton *deliverLaterBtn = [[UIButton alloc]initWithFrame:CGRectMake(1, 1, (self.view.bounds.size.width)-2, 48)];
    [deliverLaterBtn addTarget:self action:@selector(chooseDeliverySlotEvent:) forControlEvents:UIControlEventTouchUpInside];
    [deliverLaterBtn setTag:0];
    [deliverLaterBtn setBackgroundColor:UIColorFromRGB(0x39d47d)];
    [deliverLaterBtn setTitle:@"SELECT DELIVERY SLOT" forState:UIControlStateNormal];
    [deliverLaterBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    deliverLaterBtn.titleLabel.font = [UIFont systemFontOfSize:18.0f weight:UIFontWeightLight];
    [self.deliveryTypeView addSubview:deliverLaterBtn];
    
    
    UIButton *cancleTimePicker = [[UIButton alloc]initWithFrame:CGRectMake(1, 1, (self.view.bounds.size.width/2)-20, 48)];
    [cancleTimePicker addTarget:self action:@selector(cancelDateSelectionEvent:) forControlEvents:UIControlEventTouchUpInside];
    [cancleTimePicker setTag:0];
    [cancleTimePicker setBackgroundColor:UIColorFromRGB(0x39d47d)];
    [cancleTimePicker setTitle:@"CANCEL" forState:UIControlStateNormal];
    [cancleTimePicker setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    cancleTimePicker.titleLabel.font = [UIFont systemFontOfSize:18.0f  weight:UIFontWeightLight];
    [self.btnBgView addSubview:cancleTimePicker];
    
    
    UIButton *doneTimePicker = [[UIButton alloc]initWithFrame:CGRectMake((self.view.bounds.size.width/2)-18, 1, (self.view.bounds.size.width/2)-23, 48)];
    [doneTimePicker addTarget:self action:@selector(doneDateSelectionEvent:) forControlEvents:UIControlEventTouchUpInside];
    [doneTimePicker setTag:1];
    [doneTimePicker setBackgroundColor:UIColorFromRGB(0x39d47d)];
    [doneTimePicker setTitle:@"DONE" forState:UIControlStateNormal];
    [doneTimePicker setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    doneTimePicker.titleLabel.font = [UIFont systemFontOfSize:18.0  weight:UIFontWeightLight];
    [self.btnBgView addSubview:doneTimePicker];
    
    [[AppDelegate sharedAppDelegate] addShadowto:self.chooseDeliverySlotView];
}

-(void)chooseDeliverySlotEvent:(id)sender {
    
    if ([timeSlotStartArr count] == 0) {
        [self getListOfProducts];
        return;
    }
    else {
        [self checkTimeSlot:[timeSlotStartArr firstObject]];
    }
    
//    NSString *todayStr = [self strForDate:[NSDate date]];
//    
//    [USER_DEFAULTS setObject:todayStr forKey:PARAM_DELIVERY_DATE];
//    [USER_DEFAULTS setObject:@"Today" forKey:PARAM_DELIVERY_DAY];
    
//    if ([timeSlotIdArr count] == 0) {
//        [USER_DEFAULTS setObject:@"1" forKey:PARAM_TIME_SLOT];
//    }
//    else {
//        [USER_DEFAULTS setObject:[timeSlotIdArr firstObject] forKey:PARAM_TIME_SLOT];
//    }
    
//    if ([timeSlotArr count] == 0) {
//        [USER_DEFAULTS setObject:@"11AM - 1PM" forKey:PARAM_SLOT_TEXT];
//    }
//    else {
//        [USER_DEFAULTS setObject:[timeSlotArr firstObject] forKey:PARAM_SLOT_TEXT];
//    }
    
    [self.dateSelectionView setHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:UIStatusBarAnimationFade];
}

- (void)cancelDateSelectionEvent:(id)sender {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:UIStatusBarAnimationFade];
    
    [USER_DEFAULTS removeObjectForKey:PARAM_TIME_SLOT];
    [USER_DEFAULTS removeObjectForKey:PARAM_SLOT_TEXT];
    
    [self.dateSelectionView setHidden:YES];
}

- (void)doneDateSelectionEvent:(id)sender {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:UIStatusBarAnimationFade];
    
    [self.dateSelectionView setHidden:YES];
    [self goToAddressView];
}

-(void)goToAddressView {
    [USER_DEFAULTS synchronize];
    
    if ([USER_DEFAULTS boolForKey:@"isLogin"]) {
        [self performSegueWithIdentifier:@"goToAddressList" sender:self]; //goToOrderSummary
    }
    else {
        [self performSegueWithIdentifier:@"showLogin" sender:self];
    }
}


#pragma mark - UIPickerView Delegate and Datasource

- (void)pickerView:(UIPickerView *)pV didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (self.dateSegmentControl.selectedSegmentIndex == 0) {
        
        [self selectTodaySlotFor:[timeSlotStartArr objectAtIndex:row]];
        
        //        for (int i=0; i<[productTimeslotArr count]; i++) {
        //            <#statements#>
        //        }
        //        if (row == 0) {
        //            [self selectTodaySlotFor:@"02:30:00"];
        //        }
        //        else if (row == 1) {
        //            [self selectTodaySlotFor:@"06:30:00"];
        //        }
        //        else if (row == 2) {
        //            [self selectTodaySlotFor:@"10:30:00"];
        //        }
        //        else if (row == 3) {
        //            [self selectTodaySlotFor:@"14:30:00"];
        //        }
    }
    else {
        [USER_DEFAULTS setObject:[timeSlotIdArr objectAtIndex:row] forKey:PARAM_TIME_SLOT];
        [USER_DEFAULTS setObject:[timeSlotArr objectAtIndex:row] forKey:PARAM_SLOT_TEXT];
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return  timeSlotArr.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *strForTitle;
    strForTitle = [NSString stringWithFormat:@"%@",[timeSlotArr objectAtIndex:row]];
    
    return strForTitle;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 40.0;
}

- (IBAction)dateSegmentChanged:(id)sender {
    
    [self checkTimeSlot:[timeSlotStartArr objectAtIndex:0]];
}

-(void)selectTodaySlotFor:(NSString *)selectedSlot {
    NSString *todayStr = [self strForDate:[NSDate date]];
    
    NSString *time = [NSString stringWithFormat:@"%@ %@",todayStr,selectedSlot];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *date= [formatter dateFromString:time];
    NSDate *newDate = [date dateByAddingTimeInterval:-19800];
    
    NSDate *now = [NSDate date];
    
    NSComparisonResult result = [now compare:newDate];
    
    if(result == NSOrderedDescending) {
        //[self todayTimeSlot:selectedSlot];
        [self dateSegmentChanged:nil];
    }
    else if(result == NSOrderedAscending) {
        //NSLog(@"Chosen timeslot is - %@",[timeSlotIdArr objectAtIndex:[timeSlotStartArr indexOfObject:selectedSlot]]);
        
        [USER_DEFAULTS setObject:[timeSlotIdArr objectAtIndex:[timeSlotStartArr indexOfObject:selectedSlot]] forKey:PARAM_TIME_SLOT];
        [USER_DEFAULTS setObject:[timeSlotArr objectAtIndex:[timeSlotStartArr indexOfObject:selectedSlot]] forKey:PARAM_SLOT_TEXT];
        
    }
    else {
        //NSLog(@"Chosen timeslot is - %@",[timeSlotIdArr objectAtIndex:[timeSlotStartArr indexOfObject:selectedSlot]]);
        
        [USER_DEFAULTS setObject:[timeSlotIdArr objectAtIndex:[timeSlotStartArr indexOfObject:selectedSlot]] forKey:PARAM_TIME_SLOT];
        [USER_DEFAULTS setObject:[timeSlotArr objectAtIndex:[timeSlotStartArr indexOfObject:selectedSlot]] forKey:PARAM_SLOT_TEXT];
    }
}

-(NSString *)strForDate:(NSDate *) date {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setLocale:[NSLocale currentLocale]];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    [dateFormat setFormatterBehavior:NSDateFormatterBehaviorDefault];

    NSString *dateStr = [dateFormat stringFromDate:date];
    
    return dateStr;
}

-(void)checkTimeSlot:(NSString *)cutOff {
    if (self.dateSegmentControl.selectedSegmentIndex == 0) {
//        NSDateFormatter *todayDateFormatter = [[NSDateFormatter alloc] init];
//        [todayDateFormatter setDateFormat:@"yyyy-MM-dd"];
//        NSString *todayStr = [todayDateFormatter stringFromDate:[NSDate date]];
                
        NSString *todayStr = [self strForDate:[NSDate date]];
        
        NSString *time = [NSString stringWithFormat:@"%@ %@",todayStr,cutOff];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *date= [formatter dateFromString:time];
        NSDate *newDate = [date dateByAddingTimeInterval:-19800];
        NSDate *now = [NSDate date];
        
        NSComparisonResult result = [now compare:newDate];
        
        if(result == NSOrderedDescending) {
            if ([timeSlotStartArr count] == 1) {
                self.dateSegmentControl.selectedSegmentIndex = 1;
                [self dateSegmentChanged:nil];
                return;
            }
            cutOff = [timeSlotStartArr objectAtIndex:1];
            NSString *todayStr = [self strForDate:[NSDate date]];
            
            NSString *time = [NSString stringWithFormat:@"%@ %@",todayStr,cutOff];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSDate *date= [formatter dateFromString:time];
            NSDate *newDate = [date dateByAddingTimeInterval:-19800];
            NSDate *now = [NSDate date];
            
            NSComparisonResult result = [now compare:newDate];
            
            if(result == NSOrderedDescending) {
                if ([timeSlotStartArr count] == 2) {
                    self.dateSegmentControl.selectedSegmentIndex = 1;
                    [self dateSegmentChanged:nil];
                    return;
                }
                cutOff = [timeSlotStartArr objectAtIndex:2];
                NSString *todayStr = [self strForDate:[NSDate date]];
                
                NSString *time = [NSString stringWithFormat:@"%@ %@",todayStr,cutOff];
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
                [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                NSDate *date= [formatter dateFromString:time];
                NSDate *newDate = [date dateByAddingTimeInterval:-19800];
                NSDate *now = [NSDate date];
                
                NSComparisonResult result = [now compare:newDate];
                
                if(result == NSOrderedDescending) {
                    if ([timeSlotStartArr count] == 3) {
                        self.dateSegmentControl.selectedSegmentIndex = 1;
                        [self dateSegmentChanged:nil];
                        return;
                    }
                    cutOff = [timeSlotStartArr objectAtIndex:3];
                    NSString *todayStr = [self strForDate:[NSDate date]];
                    
                    NSString *time = [NSString stringWithFormat:@"%@ %@",todayStr,cutOff];
                    
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
                    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    
                    NSDate *date= [formatter dateFromString:time];
                    NSDate *newDate = [date dateByAddingTimeInterval:-19800];
                    NSDate *now = [NSDate date];
                    
                    NSComparisonResult result = [now compare:newDate];
                    
                    if(result == NSOrderedDescending) {
                        self.dateSegmentControl.selectedSegmentIndex = 1;
                        [self dateSegmentChanged:nil];
                    }
                    else if(result == NSOrderedAscending) {
                        [self.timePickerView reloadAllComponents];
                        [self.timePickerView selectRow:[timeSlotStartArr indexOfObject:cutOff] inComponent:0 animated:YES];
                        [USER_DEFAULTS setObject:[timeSlotIdArr objectAtIndex:[timeSlotStartArr indexOfObject:cutOff]] forKey:PARAM_TIME_SLOT];
                        [USER_DEFAULTS setObject:[timeSlotArr objectAtIndex:[timeSlotStartArr indexOfObject:cutOff]] forKey:PARAM_SLOT_TEXT];
                        
                        [USER_DEFAULTS setObject:todayStr forKey:PARAM_DELIVERY_DATE];
                        [USER_DEFAULTS setObject:@"Today" forKey:PARAM_DELIVERY_DAY];
                    }
                    else {
                        [self.timePickerView reloadAllComponents];
                        [self.timePickerView selectRow:[timeSlotStartArr indexOfObject:cutOff] inComponent:0 animated:YES];
                        [USER_DEFAULTS setObject:[timeSlotIdArr objectAtIndex:[timeSlotStartArr indexOfObject:cutOff]] forKey:PARAM_TIME_SLOT];
                        [USER_DEFAULTS setObject:[timeSlotArr objectAtIndex:[timeSlotStartArr indexOfObject:cutOff]] forKey:PARAM_SLOT_TEXT];
                        
                        [USER_DEFAULTS setObject:todayStr forKey:PARAM_DELIVERY_DATE];
                        [USER_DEFAULTS setObject:@"Today" forKey:PARAM_DELIVERY_DAY];
                    }
                }
                else if(result == NSOrderedAscending) {
                    [self.timePickerView reloadAllComponents];
                    [self.timePickerView selectRow:[timeSlotStartArr indexOfObject:cutOff] inComponent:0 animated:YES];
                    [USER_DEFAULTS setObject:[timeSlotIdArr objectAtIndex:[timeSlotStartArr indexOfObject:cutOff]] forKey:PARAM_TIME_SLOT];
                    [USER_DEFAULTS setObject:[timeSlotArr objectAtIndex:[timeSlotStartArr indexOfObject:cutOff]] forKey:PARAM_SLOT_TEXT];
                    
                    [USER_DEFAULTS setObject:todayStr forKey:PARAM_DELIVERY_DATE];
                    [USER_DEFAULTS setObject:@"Today" forKey:PARAM_DELIVERY_DAY];
                }
                else {
                    [self.timePickerView reloadAllComponents];
                    [self.timePickerView selectRow:[timeSlotStartArr indexOfObject:cutOff] inComponent:0 animated:YES];
                    [USER_DEFAULTS setObject:[timeSlotIdArr objectAtIndex:[timeSlotStartArr indexOfObject:cutOff]] forKey:PARAM_TIME_SLOT];
                    [USER_DEFAULTS setObject:[timeSlotArr objectAtIndex:[timeSlotStartArr indexOfObject:cutOff]] forKey:PARAM_SLOT_TEXT];
                }
            }
            else if(result == NSOrderedAscending) {
                [self.timePickerView reloadAllComponents];
                [self.timePickerView selectRow:[timeSlotStartArr indexOfObject:cutOff] inComponent:0 animated:YES];
                [USER_DEFAULTS setObject:[timeSlotIdArr objectAtIndex:[timeSlotStartArr indexOfObject:cutOff]] forKey:PARAM_TIME_SLOT];
                [USER_DEFAULTS setObject:[timeSlotArr objectAtIndex:[timeSlotStartArr indexOfObject:cutOff]] forKey:PARAM_SLOT_TEXT];
                
                [USER_DEFAULTS setObject:todayStr forKey:PARAM_DELIVERY_DATE];
                [USER_DEFAULTS setObject:@"Today" forKey:PARAM_DELIVERY_DAY];
            }
            else {
                [self.timePickerView reloadAllComponents];
                [self.timePickerView selectRow:[timeSlotStartArr indexOfObject:cutOff] inComponent:0 animated:YES];
                [USER_DEFAULTS setObject:[timeSlotIdArr objectAtIndex:[timeSlotStartArr indexOfObject:cutOff]] forKey:PARAM_TIME_SLOT];
                [USER_DEFAULTS setObject:[timeSlotArr objectAtIndex:[timeSlotStartArr indexOfObject:cutOff]] forKey:PARAM_SLOT_TEXT];
                
                [USER_DEFAULTS setObject:todayStr forKey:PARAM_DELIVERY_DATE];
                [USER_DEFAULTS setObject:@"Today" forKey:PARAM_DELIVERY_DAY];
            }
        }
        else if(result == NSOrderedAscending) {
            [self.timePickerView reloadAllComponents];
            [self.timePickerView selectRow:[timeSlotStartArr indexOfObject:cutOff] inComponent:0 animated:YES];
            [USER_DEFAULTS setObject:[timeSlotIdArr objectAtIndex:[timeSlotStartArr indexOfObject:cutOff]] forKey:PARAM_TIME_SLOT];
            [USER_DEFAULTS setObject:[timeSlotArr objectAtIndex:[timeSlotStartArr indexOfObject:cutOff]] forKey:PARAM_SLOT_TEXT];
            
            [USER_DEFAULTS setObject:todayStr forKey:PARAM_DELIVERY_DATE];
            [USER_DEFAULTS setObject:@"Today" forKey:PARAM_DELIVERY_DAY];
        }
        else {
            [self.timePickerView reloadAllComponents];
            [self.timePickerView selectRow:[timeSlotStartArr indexOfObject:cutOff] inComponent:0 animated:YES];
            [USER_DEFAULTS setObject:[timeSlotIdArr objectAtIndex:[timeSlotStartArr indexOfObject:cutOff]] forKey:PARAM_TIME_SLOT];
            [USER_DEFAULTS setObject:[timeSlotArr objectAtIndex:[timeSlotStartArr indexOfObject:cutOff]] forKey:PARAM_SLOT_TEXT];
            
            [USER_DEFAULTS setObject:todayStr forKey:PARAM_DELIVERY_DATE];
            [USER_DEFAULTS setObject:@"Today" forKey:PARAM_DELIVERY_DAY];
        }
    }
    else if (self.dateSegmentControl.selectedSegmentIndex == 1){
        [self.timePickerView reloadAllComponents];
        [self.timePickerView selectRow:0 inComponent:0 animated:YES];
        
        NSDateComponents* deltaComps = [[NSDateComponents alloc] init];
        [deltaComps setDay:1];
        NSDate* tomorrow = [[NSCalendar currentCalendar] dateByAddingComponents:deltaComps toDate:[NSDate date] options:0];
        
        NSString *tomorrowStr = [self strForDate:tomorrow];
        
        [USER_DEFAULTS setObject:tomorrowStr forKey:PARAM_DELIVERY_DATE];
        [USER_DEFAULTS setObject:@"Tomorrow" forKey:PARAM_DELIVERY_DAY];
        [USER_DEFAULTS setObject:[timeSlotIdArr firstObject] forKey:PARAM_TIME_SLOT];
        [USER_DEFAULTS setObject:[timeSlotArr firstObject] forKey:PARAM_SLOT_TEXT];
        
    }
    else if (self.dateSegmentControl.selectedSegmentIndex == 2) {
        [self.timePickerView reloadAllComponents];
        [self.timePickerView selectRow:0 inComponent:0 animated:YES];
        
        NSDateComponents* deltaComps = [[NSDateComponents alloc] init];
        [deltaComps setDay:2];
        NSDate* dayAfter = [[NSCalendar currentCalendar] dateByAddingComponents:deltaComps toDate:[NSDate date] options:0];
        
        NSString *dayAfterStr = [self strForDate:dayAfter];
        [USER_DEFAULTS setObject:dayAfterStr forKey:PARAM_DELIVERY_DATE];
        [USER_DEFAULTS setObject:@"Day After" forKey:PARAM_DELIVERY_DAY];
        [USER_DEFAULTS setObject:[timeSlotIdArr firstObject] forKey:PARAM_TIME_SLOT];
        [USER_DEFAULTS setObject:[timeSlotArr firstObject] forKey:PARAM_SLOT_TEXT];
    }
}

-(void)getLastOrderStatus {
    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
    
    NSString *userID = [USER_DEFAULTS objectForKey:PARAM_USER_ID];
    NSString *usertoken = [USER_DEFAULTS objectForKey:PARAM_USER_TOKEN];
    
    [dictParams setObject:userID forKey:PARAM_USER_ID];
    [dictParams setObject:usertoken forKey:PARAM_USER_TOKEN];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getDataFromPath:@"lastorder" withParamData:dictParams withBlock:^(id response, NSError *error) {
        if([[response valueForKey:@"status"] boolValue] == true) {
            NSArray *responseArr = [[response valueForKey:@"data"] firstObject];
            NSString *orderStatus = [NSString stringWithFormat:@"%@",[responseArr valueForKey:@"order_status"]];
            NSString *userRating = [NSString stringWithFormat:@"%@",[responseArr valueForKey:@"user_rating"]];
            
            if([orderStatus isEqualToString:@"1"] && [userRating isEqualToString:@"0"]) {
                UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                FeedBackVC *feedBack = [storyboard instantiateViewControllerWithIdentifier:@"FeedBackVC"];
                feedBack.dataArr = responseArr;
                
                [self presentViewController:feedBack
                                   animated:YES
                                 completion:nil];
            }
        }
        else {
            if ([[response valueForKey:@"statusMessage"] isEqualToString:@"Invalid user details"]) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"CanCan" message:@"Seems like you have logged in another device. Please try logging in again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                alert.tag = 2;
                [alert show];
            }
        }
    }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        [[AppDelegate sharedAppDelegate] hideLoadingView];
        [USER_DEFAULTS setBool:YES forKey:@"isChangeLocality"];
        [USER_DEFAULTS setObject:@"" forKey:PARAM_CITY];
        [USER_DEFAULTS setObject:@"" forKey:PARAM_LOCALITY];
        [USER_DEFAULTS synchronize];

        UIViewController *firstWindow = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SimpleLocalityVC"];
        [self.navigationController setViewControllers:[NSArray arrayWithObject:firstWindow] animated:YES];
    }
    else if (alertView.tag == 2){
        if (buttonIndex == 0) {
            [self clearCurrentSession];
        }
        else if (buttonIndex == 1) {
            //NSLog(@"No");
        }
    }
}

-(void)clearCurrentSession {
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Logging out..."];
    
    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
    
    NSString *userID = [USER_DEFAULTS objectForKey:PARAM_USER_ID];
    NSString *usertoken = [USER_DEFAULTS objectForKey:PARAM_USER_TOKEN];
    
    [dictParams setObject:userID forKey:PARAM_USER_ID];
    [dictParams setObject:usertoken forKey:PARAM_USER_TOKEN];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_SESSION_LOGOUT withParamData:dictParams withBlock:^(id response, NSError *error) {
            [USER_DEFAULTS removeObjectForKey:@"isLogin"];
            [USER_DEFAULTS removeObjectForKey:PARAM_USER_ID];
            [USER_DEFAULTS removeObjectForKey:PARAM_USER_TOKEN];
            
            [USER_DEFAULTS setObject:@"" forKey:PARAM_CITY];
            [USER_DEFAULTS setObject:@"" forKey:PARAM_LOCALITY];

            [USER_DEFAULTS removeObjectForKey:PARAM_PHONE_NUM];
            [USER_DEFAULTS removeObjectForKey:PARAM_ADDRESS_ID];
            [USER_DEFAULTS removeObjectForKey:PARAM_TIME_SLOT];
            
            // CleverTap - Logout Event
            [[CleverTap push] eventName:@"Logout"];
            
            // Branch - Logout
            [[Branch getInstance] logout];

            [USER_DEFAULTS synchronize];
            
            [[AppDelegate sharedAppDelegate] hideLoadingView];

            UIViewController *firstWindow = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SimpleLocalityVC"];
            [self.navigationController setViewControllers:[NSArray arrayWithObject:firstWindow] animated:YES];
    }];
}

@end
