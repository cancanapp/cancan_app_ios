//
//  SimpleLocalityVC.m
//  CanCan
//
//  Created by AC on 23/08/15.
//  Copyright (c) 2015 CanCan Pvt. Ltd.,. All rights reserved.
//

#import "SimpleLocalityVC.h"
#import "ServiceAreasVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "AFHTTPRequestOperationManager.h"
#import "PQFCustomLoaders.h"
#import <CleverTapSDK/CleverTap.h>

@interface SimpleLocalityVC () {
    NSArray *serviceAreasArr, *cityArr;
    NSMutableArray *localityArr;
}

@property (strong, nonatomic) IBOutlet UILabel *cityLbl;
@property (strong, nonatomic) IBOutlet UILabel *localityLbl;
@property (strong, nonatomic) IBOutlet UIView *getnotifiedView;
@property (nonatomic, strong) UIDynamicAnimator *animator;

@property (strong, nonatomic) IBOutlet UIScrollView *notifyMeSclVw;
@property (weak, nonatomic) IBOutlet UITextField *notifyMeEmailTxt;
@property (weak, nonatomic) IBOutlet UITextField *notifyMeCityTxt;
@property (weak, nonatomic) IBOutlet UITextField *notifyMeLocalityTxt;

@property (strong, nonatomic) IBOutlet UIView *loaderView;
@property (nonatomic, strong) PQFBallDrop *ballDrop;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *getLocalityLoader;

@end

@implementation SimpleLocalityVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [USER_DEFAULTS setObject:@"" forKey:PARAM_CITY];
    [USER_DEFAULTS setObject:@"" forKey:PARAM_LOCALITY];
    
    [self getServiceLocation];
    
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self.navigationController.navigationBar setHidden:YES];
    
    [self preLoadLocation];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
    //    [[AppDelegate sharedAppDelegate] hideLoadingView];
}

- (void)keyboardDidShow: (NSNotification *) notif{
    // Do something here
}

- (void)keyboardWillHide: (NSNotification *) notif{
    // Do something here
    [self.notifyMeSclVw setContentOffset:CGPointZero animated:YES];
}

-(void)preLoadLocation {
    if ([[USER_DEFAULTS objectForKey:PARAM_CITY] isEqualToString:@""]) {
        self.cityLbl.text = @"Choose your City";
        self.cityLbl.textColor = [UIColor lightGrayColor];
    }
    else {
        self.cityLbl.text = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_CITY]];
        self.cityLbl.textColor = [UIColor blackColor];
    }
    
    if ([[USER_DEFAULTS objectForKey:PARAM_LOCALITY] isEqualToString:@""]) {
        self.localityLbl.text = @"Choose your Locality";
        self.localityLbl.textColor = [UIColor lightGrayColor];
    }
    else {
        self.localityLbl.text = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_LOCALITY]];
        self.localityLbl.textColor = [UIColor blackColor];
        
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:@"Loading Products..."];
        
        [USER_DEFAULTS setObject:self.cityLbl.text forKey:PARAM_CITY];
        [USER_DEFAULTS setObject:self.localityLbl.text forKey:PARAM_LOCALITY];
        [USER_DEFAULTS synchronize];
        
        [USER_DEFAULTS setBool:NO forKey:@"isChangeLocality"];
        
        [self performSegueWithIdentifier:@"showProductList" sender:self];
        
        [USER_DEFAULTS removeObjectForKey:PARAM_ADDRESS_ID];
    }
    
    if ([[USER_DEFAULTS objectForKey:PARAM_SERVICE_HEADER_TITLE] isEqualToString:@"CHOOSE YOUR CITY"]) {
        self.localityLbl.text = @"Choose your Locality";
        self.localityLbl.textColor = [UIColor lightGrayColor];
    }
}

-(void)getServiceLocation {
    NSString *url=[NSString stringWithFormat:@"%@services/location",SERVICE_URL];
    [self.getLocalityLoader startAnimating];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if([[responseObject valueForKey:@"status"] boolValue] == true) {
            serviceAreasArr = [[responseObject valueForKey:@"data"] valueForKey:@"location"];
            
            cityArr = [serviceAreasArr valueForKey:@"city"];
            
            [USER_DEFAULTS setObject:[cityArr firstObject] forKey:PARAM_CITY];
            [USER_DEFAULTS synchronize];
            
            self.cityLbl.text = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_CITY]];
            self.cityLbl.textColor = [UIColor blackColor];

            localityArr = [[NSMutableArray alloc]initWithCapacity:0];
            
            NSDictionary *localityDict = [serviceAreasArr valueForKey:@"locality"];
            
            for (NSArray *arr in localityDict) {
                [localityArr addObject:arr];
            }
            
            [self.getLocalityLoader stopAnimating];
        }
        else {
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[responseObject valueForKey:@"statusMessage"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // handle Failure of AFHTTPREQUEST
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:@"CanCan" andMessage:@"Please check your Internet connection."];
    }];
}

- (IBAction)choseCityEvent:(id)sender {
    if ([cityArr count] < 1) {
        [self getServiceLocation];
    }
    else {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ServiceAreasVC *serviceAreas = [storyboard instantiateViewControllerWithIdentifier:@"ServiceAreasVC"];
        serviceAreas.locationsArr = cityArr;
        [USER_DEFAULTS setObject:@"CHOOSE YOUR CITY" forKey:PARAM_SERVICE_HEADER_TITLE];
        
        [self presentViewController:serviceAreas
                           animated:YES
                         completion:nil];
    }
}

- (IBAction)chooseLocalityEvent:(id)sender {
    if (![self.cityLbl.text isEqualToString:@"Choose your City"]) {
        NSString *cityStr = self.cityLbl.text;
        int localityIndex = (int)[cityArr indexOfObject:cityStr];
        NSArray *selectedLocalityArr = [localityArr objectAtIndex:localityIndex];
        
        if ([selectedLocalityArr count] < 1) {
            //[self getServiceLocation];
            //NSLog(@"Add person to waitList");
        }
        else {
            
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ServiceAreasVC *serviceAreas = [storyboard instantiateViewControllerWithIdentifier:@"ServiceAreasVC"];
            serviceAreas.locationsArr = selectedLocalityArr;
            
            [USER_DEFAULTS setObject:@"CHOOSE YOUR LOCALITY" forKey:PARAM_SERVICE_HEADER_TITLE];
            
            [self presentViewController:serviceAreas
                               animated:YES
                             completion:nil];
        }
    }
    else {
        [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Choose your City"];
    }
}

- (IBAction)getNotifiedEvent:(id)sender {
    [self toggleMenu];
}


- (IBAction)removeGetNotifiedView:(id)sender {
    [UIView animateWithDuration:0.5f
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^(void) {
                         CGRect newrect = self.getnotifiedView.frame;
                         newrect = CGRectMake(0,
                                              -self.view.bounds.size.height,
                                              self.view.bounds.size.width,
                                              self.view.bounds.size.height);
                         
                         [self.notifyMeCityTxt setUserInteractionEnabled:YES];
                         [self.getnotifiedView setFrame:newrect];
                     }
                     completion:^(BOOL finished) {
                         [_animator removeAllBehaviors];
                         [self.getnotifiedView removeFromSuperview];
                     }];
}

-(void)toggleMenu{
    self.getnotifiedView.frame = CGRectMake(0,
                                            -self.view.bounds.size.height,
                                            self.view.bounds.size.width,
                                            self.view.bounds.size.height);
    if ([USER_DEFAULTS objectForKey:PARAM_CITY]) {
        self.notifyMeCityTxt.text = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_CITY]];
        [self.notifyMeCityTxt setUserInteractionEnabled:YES];
    }
    else {
        [self.notifyMeCityTxt setUserInteractionEnabled:YES];
    }
    
    [self.view addSubview:self.getnotifiedView];
    
    _animator = [[UIDynamicAnimator alloc] initWithReferenceView:[[UIApplication sharedApplication] keyWindow]];
    
    UIGravityBehavior *gravity = [[UIGravityBehavior alloc] initWithItems:@[self.getnotifiedView]];
    [_animator addBehavior:gravity];
    
    UICollisionBehavior *collision = [[UICollisionBehavior alloc] initWithItems:@[self.getnotifiedView]];
    collision.translatesReferenceBoundsIntoBoundary = NO;
    [collision addBoundaryWithIdentifier:@"notificationEnd" fromPoint:CGPointMake(0, self.getnotifiedView.bounds.size.height) toPoint:CGPointMake([[UIScreen mainScreen] bounds].size.width, self.getnotifiedView.bounds.size.height)];
    [_animator addBehavior:collision];
    
    UIDynamicItemBehavior *elasticityBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[self.getnotifiedView]];
    elasticityBehavior.elasticity = 0.1f;
    [_animator addBehavior:elasticityBehavior];
}

- (IBAction)addToNotificationList:(id)sender {
    if(self.notifyMeEmailTxt.text.length<1 || self.notifyMeCityTxt.text.length<1 || self.notifyMeLocalityTxt.text.length<1) {
        if (self.notifyMeEmailTxt.text.length<1) {
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Please enter your Email"];
        }
        else if(self.notifyMeCityTxt.text.length<1) {
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Please enter your City"];
        }
        else if(self.notifyMeLocalityTxt.text.length<1) {
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Please enter your Locality"];
        }
    }
    else {
        NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
        
        [dictParams setObject:self.notifyMeCityTxt.text forKey:PARAM_CITY];
        [dictParams setObject:self.notifyMeLocalityTxt.text forKey:PARAM_LOCALITY];
        [dictParams setObject:self.notifyMeEmailTxt.text forKey:PARAM_EMAIL];
        
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Adding you to our VIP list..."];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_WAITING_LIST withParamData:dictParams withBlock:^(id response, NSError *error) {
            if([[response valueForKey:@"status"] boolValue] == true) {
                
                // CleverTap - Get Notified
                NSDictionary *props = @{@"Email":self.notifyMeEmailTxt.text,
                                        @"City":self.notifyMeCityTxt.text,
                                        @"Locality":self.notifyMeLocalityTxt};
                
                [[CleverTap push] eventName:@"Get Notified Email" eventProps:props];
                [[CleverTap push] profile:@{@"Email":self.notifyMeEmailTxt.text}];
                [[CleverTap push] profile:@{@"Locality":self.notifyMeLocalityTxt.text}];
                
                [[AppDelegate sharedAppDelegate] hideLoadingView];
                [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[response valueForKey:@"statusMessage"]];
                
                [self removeGetNotifiedView:nil];
            }
            else {
                [[AppDelegate sharedAppDelegate] hideLoadingView];
                [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[response valueForKey:@"statusMessage"]];
            }
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // CleverTap - Area Selection
    NSDictionary *productProps = @{@"City":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_CITY]],
                                   @"Locality":[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_LOCALITY]]};
    [[CleverTap push] eventName:@"Area Selection" eventProps:productProps];
}

@end
