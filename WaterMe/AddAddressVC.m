//
//  AddAddressVC.m
//  WaterMe
//
//  Created by Dev on 18/07/15.
//  Copyright (c) 2015 WaterMe. All rights reserved.
//

#import "AddAddressVC.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "AFHTTPRequestOperationManager.h"
#import "ServiceAreasVC.h"
#import "SWRevealViewController.h"
#import <CleverTapSDK/CleverTap.h>

@interface AddAddressVC () <SWRevealViewControllerDelegate>
{
    NSArray *serviceAreasArr, *cityArr;
    NSMutableArray *localityArr;
    UIButton *btn;
}

@property (weak, nonatomic) IBOutlet UIScrollView *bgScrollVw;
@property (weak, nonatomic) IBOutlet UIScrollView *getNotifiedSclVw;

@property (weak, nonatomic) IBOutlet UILabel *headerLbl;

@property (weak, nonatomic) IBOutlet UITextField *notifyEmailTxt;
@property (weak, nonatomic) IBOutlet UITextField *notifyCityTxt;
@property (weak, nonatomic) IBOutlet UITextField *notifyLocalityTxt;


@property (weak, nonatomic) IBOutlet UITextField *flatNumTxt;
@property (weak, nonatomic) IBOutlet UITextField *streetAddTxt;
@property (weak, nonatomic) IBOutlet UITextField *landmarkTxt;
@property (weak, nonatomic) IBOutlet UITextField *nickNameTxt;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelbtn;

@property (weak, nonatomic) IBOutlet UILabel *cityLbl;
@property (weak, nonatomic) IBOutlet UILabel *localityLbl;

@property (weak, nonatomic) IBOutlet UIButton *revealButtonItem;

@property (strong, nonatomic) IBOutlet UIButton *cityBtn;
@property (strong, nonatomic) IBOutlet UIButton *localityBtn;

@property (strong, nonatomic) IBOutlet UIButton *cityImgVw;
@property (strong, nonatomic) IBOutlet UIButton *localityImgVw;


@end

@implementation AddAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.bgScrollVw setHidden:NO];
    [self.getNotifiedSclVw setHidden:YES];
    [self.getNotifiedSclVw setScrollEnabled:NO];
    
    [self preLoadLocation];
    [self getServiceLocation];
    [self customLayout];
    
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)customLayout {
    self.cityLbl.text = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_CITY]];

    self.localityLbl.text = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_LOCALITY]];
    
    self.cityBtn.userInteractionEnabled = NO;
    self.localityBtn.userInteractionEnabled = NO;
    
    self.cityImgVw.hidden = YES;
    self.localityImgVw.hidden = YES;
}

//- (void)customSetup
//{
//    self.revealViewController.delegate = self;
//
//    SWRevealViewController *revealViewController = self.revealViewController;
//    if (revealViewController) {
//        [self.revealButtonItem addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
//        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//    }
//}
//
//- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position {
//    if(position == FrontViewPositionLeft) {
//        [btn removeFromSuperview];
//    } else {
//        btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
//        [btn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
//        [btn setBackgroundColor:[UIColor clearColor]];
//        [self.view addSubview:btn];
//        [self.view bringSubviewToFront:btn];
//    }
//}


-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)keyboardDidShow: (NSNotification *) notif{
    // Do something here
    [self.getNotifiedSclVw setScrollEnabled:YES];
}

- (void)keyboardWillHide: (NSNotification *) notif{
    // Do something here
    [self.getNotifiedSclVw setScrollEnabled:NO];
    
    [self.bgScrollVw setContentOffset:CGPointMake(0, 0) animated:YES];
    [self.getNotifiedSclVw setContentOffset:CGPointZero animated:YES];
}

-(void)preLoadLocation {
    if ([[USER_DEFAULTS objectForKey:PARAM_CITY] isEqualToString:@""]) {
        self.cityLbl.text = @"City";
        self.cityLbl.textColor = [UIColor lightGrayColor];
    }
    else {
        self.cityLbl.text = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_CITY]];
        self.cityLbl.textColor = [UIColor blackColor];
    }
    
    //    if (![[USER_DEFAULTS objectForKey:@"simple_city"] isEqualToString:@""]) {
    //        self.cityLbl.text = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:@"simple_city"]];
    //        self.cityLbl.textColor = [UIColor blackColor];
    //    }
    
    if ([[USER_DEFAULTS objectForKey:PARAM_LOCALITY] isEqualToString:@""]) {
        self.localityLbl.text = @"Locality";
        self.localityLbl.textColor = [UIColor lightGrayColor];
    }
    else {
        self.localityLbl.text = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_LOCALITY]];
        self.localityLbl.textColor = [UIColor blackColor];
    }
    
    //    if (![[USER_DEFAULTS objectForKey:@"simple_locality"] isEqualToString:@""]) {
    //        self.localityLbl.text = [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:@"simple_locality"]];
    //        self.localityLbl.textColor = [UIColor blackColor];
    //    }
    
    if ([[USER_DEFAULTS objectForKey:PARAM_SERVICE_HEADER_TITLE] isEqualToString:@"CHOOSE YOUR CITY"]) {
        self.localityLbl.text = @"Locality";
        self.localityLbl.textColor = [UIColor lightGrayColor];
    }
}

-(void)getServiceLocation {
    NSString *url=[NSString stringWithFormat:@"%@services/location",SERVICE_URL];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if([[responseObject valueForKey:@"status"] boolValue] == true) {
            serviceAreasArr = [[responseObject valueForKey:@"data"] valueForKey:@"location"];
            
            cityArr = [serviceAreasArr valueForKey:@"city"];
            localityArr = [[NSMutableArray alloc]initWithCapacity:0];
            
            NSDictionary *localityDict = [serviceAreasArr valueForKey:@"locality"];
            for (NSArray *arr in localityDict) {
                [localityArr addObject:arr];
            }
        }
        else {
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[responseObject valueForKey:@"statusMessage"]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // handle Failure of AFHTTPREQUEST
        [self dismissViewControllerAnimated:YES completion:nil];
        [[AppDelegate sharedAppDelegate] showAlertWithTitle:@"CanCan" andMessage:@"Please check your Internet connection."];
    }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)cancelAddAddressEvent:(id)sender {
    if ([self.headerLbl.text isEqualToString:@"ADD ADDRESS"]) {
//        if ([USER_DEFAULTS objectForKey:PARAM_ADDRESS_ID]) {
//            [USER_DEFAULTS setObject:@"" forKey:PARAM_CITY];
//            [USER_DEFAULTS setObject:@"" forKey:PARAM_LOCALITY];
//        }
        
        [USER_DEFAULTS setObject:@"" forKey:PARAM_SERVICE_HEADER_TITLE];
        
        [self.view endEditing:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self.headerLbl setText:@"ADD ADDRESS"];
        [self.doneBtn setHidden:NO];
        
        [self.bgScrollVw setContentOffset:CGPointZero];
        [self.bgScrollVw setHidden:NO];
        [self.getNotifiedSclVw setHidden:YES];
    }
}

- (IBAction)chooseCityEvent:(id)sender {
    [self.view endEditing:YES];
    
    if ([cityArr count] < 1) {
        [self getServiceLocation];
    }
    else {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ServiceAreasVC *serviceAreas = [storyboard instantiateViewControllerWithIdentifier:@"ServiceAreasVC"];
        serviceAreas.locationsArr = cityArr;
        [USER_DEFAULTS setObject:@"CHOOSE YOUR CITY" forKey:PARAM_SERVICE_HEADER_TITLE];
        
        [self presentViewController:serviceAreas
                           animated:YES
                         completion:nil];
    }
}

- (IBAction)chooseLocalityEvent:(id)sender {
    [self.view endEditing:YES];
    
    if (![self.cityLbl.text isEqualToString:@"City"]) {
        int localityIndex = [cityArr indexOfObject:self.cityLbl.text];
        NSArray *selectedLocalityArr = [localityArr objectAtIndex:localityIndex];
        
        if ([selectedLocalityArr count] < 1) {
            //[self getServiceLocation];
            //NSLog(@"Add person to waitList");
        }
        else {
            
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ServiceAreasVC *serviceAreas = [storyboard instantiateViewControllerWithIdentifier:@"ServiceAreasVC"];
            serviceAreas.locationsArr = selectedLocalityArr;
            
            [USER_DEFAULTS setObject:@"CHOOSE YOUR LOCALITY" forKey:PARAM_SERVICE_HEADER_TITLE];
            
            [self presentViewController:serviceAreas
                               animated:YES
                             completion:nil];
        }
    }
    else {
        [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Select your City"];
    }
}

- (IBAction)addAddressEvent:(id)sender {
    [self addNewAddress];
}


-(void)addNewAddress {
    if(self.flatNumTxt.text.length<1 || self.streetAddTxt.text.length<1 || self.landmarkTxt.text.length<1 || self.nickNameTxt.text.length<1||[self.cityLbl.text isEqualToString:@"City"] || [self.localityLbl.text isEqualToString:@"Locality"]) {
        if ([self.cityLbl.text isEqualToString:@"City"]) {
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Please choose your current city"];
        }
        else if ([self.localityLbl.text isEqualToString:@"Locality"]) {
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Please choose your locality"];
        }
        else if(self.nickNameTxt.text.length<1) {
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Please enter your Name"];
        }
        else if(self.flatNumTxt.text.length<1) {
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Please enter your flat/apartment number"];
        }
        else if(self.streetAddTxt.text.length<1) {
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Please enter your street address"];
        }
        else if(self.landmarkTxt.text.length<1) {
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Please enter a nearby landmark to your address"];
        }
    }
    else {
        NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
        
        NSString *userID = [USER_DEFAULTS objectForKey:PARAM_USER_ID];
        NSString *usertoken = [USER_DEFAULTS objectForKey:PARAM_USER_TOKEN];
        
        [dictParams setObject:userID forKey:PARAM_USER_ID];
        [dictParams setObject:usertoken forKey:PARAM_USER_TOKEN];
        [dictParams setObject:self.flatNumTxt.text forKey:PARAM_FLAT_NO];
        [dictParams setObject:self.streetAddTxt.text forKey:PARAM_STREET_ADDRESS];
        [dictParams setObject:self.landmarkTxt.text forKey:PARAM_LANDMARK];
        [dictParams setObject:self.nickNameTxt.text forKey:PARAM_NICKNAME];
        [dictParams setObject:self.cityLbl.text forKey:PARAM_CITY];
        [dictParams setObject:self.localityLbl.text forKey:PARAM_LOCALITY];
        [dictParams setObject:@"600041" forKey:PARAM_ZIPCODE];
        
        [USER_DEFAULTS setObject:self.nickNameTxt.text forKey:PARAM_NICKNAME];
        [USER_DEFAULTS setObject:self.flatNumTxt.text forKey:PARAM_FLAT_NO];
        [USER_DEFAULTS setObject:self.streetAddTxt.text forKey:PARAM_STREET_ADDRESS];
        [USER_DEFAULTS setObject:self.landmarkTxt.text forKey:PARAM_LANDMARK];
        [USER_DEFAULTS synchronize];
        
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Updating your address list..."];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_ADD_ADDRESS withParamData:dictParams withBlock:^(id response, NSError *error) {
            if([[response valueForKey:@"status"] boolValue] == true) {
//                [USER_DEFAULTS setObject:@"" forKey:PARAM_CITY];
//                [USER_DEFAULTS setObject:@"" forKey:PARAM_LOCALITY];
                [USER_DEFAULTS setObject:@"" forKey:PARAM_SERVICE_HEADER_TITLE];
                
                [self.view endEditing:YES];
                
                // CleverTap - Create Profile
                NSNumberFormatter *numFormat = [[NSNumberFormatter alloc] init];
                numFormat.numberStyle = NSNumberFormatterNoStyle;
                NSNumber *myNumber = [numFormat numberFromString:[NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_PHONE_NUM]]];
                
                NSDictionary *profile = @{@"Name" : self.nickNameTxt.text, // String
                                          @"Identity" : userID, // String or number
                                          @"Phone" : myNumber, // Phone(exclude the country code)
                                          @"MSG-email" : @YES, // Enable email notifications
                                          @"MSG-push" : @YES, // Enable push notifications
                                          @"MSG-sms" : @YES // Enable SMS notifications
                                          };
                [[CleverTap push] profile:profile];
                [[CleverTap push] profile:@{@"Locality":self.localityLbl.text}];
                
                // CleverTap - Add Address
                NSDictionary *props = @{@"City" : [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_CITY]],
                                        @"Locality" : [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_LOCALITY]],
                                        @"Flat" : [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_FLAT_NO]],
                                        @"Street" : [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_FLAT_NO]],
                                        @"Landmark" : [NSString stringWithFormat:@"%@",[USER_DEFAULTS objectForKey:PARAM_FLAT_NO]],
                                        };
                [[CleverTap push] eventName:@"Add Address" eventProps:props];
                
                
                [self dismissViewControllerAnimated:YES completion:^{
                    [[AppDelegate sharedAppDelegate] hideLoadingView];
                }];
            }
            else {
                [[AppDelegate sharedAppDelegate] hideLoadingView];
                [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[response valueForKey:@"statusMessage"]];
            }
        }];
    }
}

- (IBAction)showWaitList:(id)sender {
    [self.bgScrollVw setHidden:YES];
    [self.getNotifiedSclVw setHidden:NO];
    
    [self.headerLbl setText:@"GET NOTIFIED"];
    [self.doneBtn setHidden:YES];
}

- (IBAction)addToWaitList:(id)sender {
    [self waitList];
}

-(void) waitList {
    if(self.notifyEmailTxt.text.length<1 || self.notifyCityTxt.text.length<1 || self.notifyLocalityTxt.text.length<1) {
        if (self.notifyEmailTxt.text.length<1) {
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Please enter your Email"];
        }
        else if(self.notifyCityTxt.text.length<1) {
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Please enter your City"];
        }
        else if(self.notifyLocalityTxt.text.length<1) {
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:@"Please enter your Locality"];
        }
    }
    else {
        NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
        
        NSString *userID = [USER_DEFAULTS objectForKey:PARAM_USER_ID];
        NSString *usertoken = [USER_DEFAULTS objectForKey:PARAM_USER_TOKEN];
        
        [dictParams setObject:userID forKey:PARAM_USER_ID];
        [dictParams setObject:usertoken forKey:PARAM_USER_TOKEN];
        [dictParams setObject:self.notifyCityTxt.text forKey:PARAM_CITY];
        [dictParams setObject:self.notifyLocalityTxt.text forKey:PARAM_LOCALITY];
        [dictParams setObject:self.notifyEmailTxt.text forKey:PARAM_EMAIL];
        
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Adding you to our VIP list..."];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_WAITING_LIST withParamData:dictParams withBlock:^(id response, NSError *error) {
            if([[response valueForKey:@"status"] boolValue] == true) {
                
                // CleverTap - Get Notified
                NSDictionary *props = @{@"Email":self.notifyEmailTxt.text,
                                        @"City":self.notifyCityTxt.text,
                                        @"Locality":self.notifyLocalityTxt};
                [[CleverTap push] eventName:@"Get Notified Email" eventProps:props];
                [[CleverTap push] profile:@{@"Email":self.notifyEmailTxt.text}];
                
                
                [self dismissViewControllerAnimated:YES completion:^{
                    [[AppDelegate sharedAppDelegate] hideLoadingView];
                    [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[response valueForKey:@"statusMessage"]];
                }];
            }
            else {
                [[AppDelegate sharedAppDelegate] hideLoadingView];
                [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[response valueForKey:@"statusMessage"]];
            }
        }];
    }
}

@end
