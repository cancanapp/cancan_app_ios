//
//  AppDelegate.h
//  WaterMe
//
//  Created by Dev on 07/05/15.
//  Copyright (c) 2015 WaterMe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIVisualEffectView *viewLoading;
@property (strong, nonatomic) UIActivityIndicatorView *loader;

+(AppDelegate *)sharedAppDelegate;

//Show Alert
-(void)showAlertWithTitle:(NSString *)strTitle andMessage:(NSString *)strMsg;

// Add shadow
-(void)addShadowto:(UIView *)view;

//Add Loading view
-(void)showLoadingWithTitle:(NSString *)title;
-(void)hideLoadingView;

// Check Interent Connectivity
-(BOOL)Connected;

// Check Email Validity
-(BOOL)isValidEmailAddress:(NSString *)email;

@end

