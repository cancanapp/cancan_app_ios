//
//  OrderHistoryVC.m
//  CanCan
//
//  Created by AC on 25/08/15.
//  Copyright (c) 2015 CanCan Pvt. Ltd.,. All rights reserved.
//

#import "OrderHistoryVC.h"
#import "AFNHelper.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "SDWebImage/UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface OrderHistoryVC ()

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

@property (strong, nonatomic) IBOutlet UIScrollView *orderSclVw;
@property (strong, nonatomic) IBOutlet UILabel *headerLbl;

@property (strong, nonatomic) IBOutlet UIImageView *emptyImgVw;
@property (strong, nonatomic) IBOutlet UILabel *msgHeaderLbl;
@property (strong, nonatomic) IBOutlet UILabel *msgContentLbl;

@end

@implementation OrderHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.orderSclVw setBackgroundColor:[UIColor colorWithRed:0.8932 green:0.9091 blue:0.9162 alpha:1.0]];
    
    if ([[USER_DEFAULTS objectForKey:@"my_information_status"]isEqualToString:@"0"]) {
        [self.headerLbl setText:@"ORDER HISTORY"];
        
        [self.emptyImgVw setImage:[UIImage imageNamed:@"no_orders"]];
        [self.msgHeaderLbl setText:@"Order some Cans"];
        [self.msgContentLbl setText:@"Currently you have no orders to display."];
        
        [self getOrderHistory];
    }
    else {
        [self.headerLbl setText:@"NOTIFICATIONS"];
        [self.emptyImgVw setImage:[UIImage imageNamed:@"no_notifications"]];
        [self.msgHeaderLbl setText:@"No Notifications"];
        [self.msgContentLbl setText:@"We'll notify you as soon as you have new notifications."];

        [self getNotificatons];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getNotificatons { //get_news
    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
    
    NSString *userID = [USER_DEFAULTS objectForKey:PARAM_USER_ID];
    NSString *usertoken = [USER_DEFAULTS objectForKey:PARAM_USER_TOKEN];
    
    [dictParams setObject:userID forKey:PARAM_USER_ID];
    [dictParams setObject:usertoken forKey:PARAM_USER_TOKEN];
    
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Fetching latest news from CanCan..."];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_GET_NEWS withParamData:dictParams withBlock:^(id response, NSError *error) {
        if([[response valueForKey:@"status"] boolValue] == true) {
            NSArray *dataArr = [response objectForKey:@"data"];
            [self showCardFornotifications:dataArr];
            
            [[AppDelegate sharedAppDelegate] hideLoadingView];
        }
        else {
            [self.orderSclVw setHidden:YES];

            [[AppDelegate sharedAppDelegate] hideLoadingView];
            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[response valueForKey:@"statusMessage"]];
        }
    }];
}

-(void)showCardFornotifications:(NSArray *)notifsArr {
    
    if ([notifsArr count] == 0) {
        [self.orderSclVw setHidden:YES];
        return;
    }
    
    int x = 10;
    int y = 10;
    int vwHeight = 120;
    int vwWidth = self.view.bounds.size.width-2*x;
    //int imgWidth = 100;
    
    for (int i=0; i<[notifsArr count]; i++) {
        if (i == 0) {
            y += 64;
        }
        
        UIView *cardVw = [[UIView alloc]initWithFrame:CGRectMake(x, y, vwWidth, vwHeight)];
        cardVw.backgroundColor = [UIColor whiteColor];
        //[[AppDelegate sharedAppDelegate] addShadowto:cardVw];
        [[cardVw layer] setBorderWidth:0.5f];
        [[cardVw layer] setBorderColor:[UIColor lightGrayColor].CGColor]; //[UIColor colorWithRed:0.4264 green:0.8331 blue:0.4944 alpha:1.0]
        
        UILabel *newsHeaderLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, cardVw.bounds.size.width-20, 40)];
        //[newsHeaderLbl setBackgroundColor:[UIColor lightGrayColor]];
        [newsHeaderLbl setText:[[notifsArr objectAtIndex:i] valueForKey:@"news_title"]];
        [newsHeaderLbl setFont:[UIFont systemFontOfSize:18.0f weight:UIFontWeightMedium]];
        [newsHeaderLbl setTextColor:[UIColor blackColor]];
        [newsHeaderLbl setMinimumScaleFactor:12.0/[UIFont labelFontSize]];
        [newsHeaderLbl setAdjustsFontSizeToFitWidth:YES];
        [cardVw addSubview:newsHeaderLbl];
        
        UIView *dashedView = [[UIView alloc]initWithFrame:CGRectMake(10, 40, cardVw.frame.size.width-10, 0)];
        [dashedView setBackgroundColor:[UIColor clearColor]];
        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
        shapeLayer = [self createDashedLinesFrom:CGPointMake(0, 0) to:CGPointMake(cardVw.frame.size.width-10, 0)];
        [cardVw addSubview:dashedView];
        [dashedView.layer addSublayer:shapeLayer];
        
        UILabel *newsContentLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, cardVw.bounds.size.width-20, 60)];
        //[newsHeaderLbl setBackgroundColor:[UIColor lightGrayColor]];
        [newsContentLbl setText:[[notifsArr objectAtIndex:i] valueForKey:@"news_content"]];
        [newsContentLbl setFont:[UIFont systemFontOfSize:16.0f weight:UIFontWeightLight]];
        [newsHeaderLbl setMinimumScaleFactor:12.0/[UIFont labelFontSize]];
        [newsHeaderLbl setAdjustsFontSizeToFitWidth:YES];
        [newsContentLbl setTextColor:[UIColor darkGrayColor]];
        [newsContentLbl setNumberOfLines:5];
        [cardVw addSubview:newsContentLbl];

        [self.orderSclVw addSubview:cardVw];
        
        y += vwHeight+10;
    }
    
    [self.orderSclVw setContentSize:CGSizeMake(self.view.bounds.size.width, y)];
}

-(void)getOrderHistory {
    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
    
    NSString *userID = [USER_DEFAULTS objectForKey:PARAM_USER_ID];
    NSString *usertoken = [USER_DEFAULTS objectForKey:PARAM_USER_TOKEN];
    
    [dictParams setObject:userID forKey:PARAM_USER_ID];
    [dictParams setObject:usertoken forKey:PARAM_USER_TOKEN];
    
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Fetching your order history..."];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getDataFromPath:FILE_GET_ORDER_HISTORY withParamData:dictParams withBlock:^(id response, NSError *error) {
        if([[response valueForKey:@"status"] boolValue] == true) {
            //NSDictionary *responseDict = [response objectForKey:@"data"];
            NSArray *ordersArr = [response objectForKey:@"data"];
            [self createCard:ordersArr];
            
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            
        }
        else {
            [self.orderSclVw setHidden:YES];

            [[AppDelegate sharedAppDelegate] hideLoadingView];

            [[AppDelegate sharedAppDelegate]showAlertWithTitle:@"CanCan" andMessage:[response valueForKey:@"statusMessage"]];
        }
    }];
}

-(void)createCard:(NSArray *)detailsArr {
    if ([detailsArr count] == 0) {
        [self.orderSclVw setHidden:YES];
        return;
    }

    int x = 10;
    int y = 10;
    int vwHeight = 115;
    int vwWidth = self.view.bounds.size.width-2*x;
    int imgWidth = 100;
    
    for (int i=0; i<[detailsArr count]; i++) {
        if (i == 0) {
            y += 64;
        }
        
        UIView *cardVw = [[UIView alloc]initWithFrame:CGRectMake(x, y, vwWidth, vwHeight)];
        cardVw.backgroundColor = [UIColor whiteColor];
        //[[AppDelegate sharedAppDelegate] addShadowto:cardVw];
        [[cardVw layer] setBorderWidth:0.5f];
        [[cardVw layer] setBorderColor:[UIColor lightGrayColor].CGColor]; //[UIColor colorWithRed:0.4264 green:0.8331 blue:0.4944 alpha:1.0]
        
        UIImageView *canImgVw = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, imgWidth, cardVw.frame.size.height-10)];
        NSString *imgName = [NSString stringWithFormat:@"%@",[[detailsArr objectAtIndex:i] valueForKey:@"image_url"]];
        [canImgVw setImageWithURL:[NSURL URLWithString:imgName] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [canImgVw setContentMode:UIViewContentModeScaleAspectFit];
        //[[canImgVw layer] setBorderWidth:0.5f];
        //[[canImgVw layer] setBorderColor:[UIColor lightGrayColor].CGColor];
        [cardVw addSubview:canImgVw];
        
        UILabel *dateLbl = [[UILabel alloc]initWithFrame:CGRectMake(imgWidth+10, 28, cardVw.frame.size.width-(imgWidth+20), 25)];
        [dateLbl setBackgroundColor:[UIColor clearColor]];

        NSString *dateStr = [self createDate:[[detailsArr objectAtIndex:i] valueForKey:@"deliver_date"] withTimeSlot:[[detailsArr objectAtIndex:i] valueForKey:@"time_slot"]];
        
        [dateLbl setText:dateStr];
        [dateLbl setFont:[UIFont systemFontOfSize:15.0f weight:UIFontWeightLight]];
        [dateLbl setTextColor:[UIColor blackColor]];
        [cardVw addSubview:dateLbl];
        
        UILabel *productsLbl = [[UILabel alloc]initWithFrame:CGRectMake(imgWidth+10, 5, cardVw.frame.size.width-(imgWidth+20), 25)];
        [productsLbl setBackgroundColor:[UIColor clearColor]];
        NSString *canStr;
        if ([[[detailsArr objectAtIndex:i] valueForKey:@"no_of_cans"] isEqualToString:@"1"])
            canStr = @"CAN";
        else
            canStr = @"CANS";
        
        NSString *productStr = [NSString stringWithFormat:@"  %@ - %@ %@",[[detailsArr objectAtIndex:i] valueForKey:@"product_name"],[[detailsArr objectAtIndex:i] valueForKey:@"no_of_cans"],canStr];
        
        [productsLbl setText:productStr];
        [productsLbl setFont:[UIFont systemFontOfSize:15.0f weight:UIFontWeightLight]];
//        [productsLbl setMinimumScaleFactor:0.5f];
        productsLbl.adjustsFontSizeToFitWidth = YES;
        [productsLbl setTextColor:[UIColor blackColor]];
        [cardVw addSubview:productsLbl];
        
        UIView *dashedView = [[UIView alloc]initWithFrame:CGRectMake(imgWidth+10, 82, cardVw.frame.size.width-(imgWidth+20), 0)];
        [dashedView setBackgroundColor:[UIColor clearColor]];
        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
        shapeLayer = [self createDashedLinesFrom:CGPointMake(0, 0) to:CGPointMake(cardVw.frame.size.width-(imgWidth+20), 0)];
        [cardVw addSubview:dashedView];
        [dashedView.layer addSublayer:shapeLayer];
        
        UIImageView *orderStatusImgVw = [[UIImageView alloc]initWithFrame:CGRectMake(imgWidth+18, 58, 15, 15)];
        [orderStatusImgVw setContentMode:UIViewContentModeScaleAspectFit];
        
        UILabel *orderStatusStr = [[UILabel alloc]initWithFrame:CGRectMake(imgWidth+40, 53, cardVw.frame.size.width-(imgWidth+50), 25)];
        [orderStatusStr setBackgroundColor:[UIColor clearColor]];
        [orderStatusStr setFont:[UIFont systemFontOfSize:15.0f weight:UIFontWeightLight]];
        
        if ([[[detailsArr objectAtIndex:i] valueForKey:@"order_status"] isEqualToString:@"0"]) {
            [orderStatusImgVw setImage:[UIImage imageNamed:@"order_pending"]];
            
            [orderStatusStr setText:@"ORDER PENDING"];
            [orderStatusStr setTextColor:UIColorFromRGB(0xe6b347)]; //d13704
        }
        else if ([[[detailsArr objectAtIndex:i] valueForKey:@"order_status"] isEqualToString:@"1"]) {
            [orderStatusImgVw setImage:[UIImage imageNamed:@"order_delivered"]];
            
            [orderStatusStr setText:@"ORDER DELIVERED"];
            [orderStatusStr setTextColor:UIColorFromRGB(0x39d47d)];
        }
        else if ([[[detailsArr objectAtIndex:i] valueForKey:@"order_status"] isEqualToString:@"2"]) {
            [orderStatusImgVw setImage:[UIImage imageNamed:@"order_cancel"]];
            
            [orderStatusStr setText:@"ORDER CANCELLED"];
            [orderStatusStr setTextColor:UIColorFromRGB(0xd13704)];
        }
        
        [cardVw addSubview:orderStatusImgVw];
        [cardVw addSubview:orderStatusStr];
        
        UILabel *totalLbl = [[UILabel alloc]initWithFrame:CGRectMake(imgWidth+20, 85, 100, 25)];
        [totalLbl setBackgroundColor:[UIColor clearColor]];
        [totalLbl setFont:[UIFont systemFontOfSize:15.0f weight:UIFontWeightLight]];
        [totalLbl setTextColor:[UIColor blackColor]];
        [totalLbl setText:@"Total"];
        [cardVw addSubview:totalLbl];
        
        UILabel *billAmountLbl = [[UILabel alloc]initWithFrame:CGRectMake(imgWidth+120, 85, 100, 25)];
        [billAmountLbl setBackgroundColor:[UIColor clearColor]];
        [billAmountLbl setFont:[UIFont systemFontOfSize:15.0f weight:UIFontWeightLight]];
        [billAmountLbl setTextColor:[UIColor blackColor]];
        NSString *billAmount = [NSString stringWithFormat:@"%@",[[detailsArr objectAtIndex:i] valueForKey:@"amount"]];
        [billAmountLbl setText:[NSString stringWithFormat:@"₹%.0f",[billAmount floatValue]]];
        [cardVw addSubview:billAmountLbl];
        
        [self.orderSclVw addSubview:cardVw];
        
        y += vwHeight+10;
    }
    
    [self.orderSclVw setContentSize:CGSizeMake(self.view.bounds.size.width, y)];
}

-(NSString *)createDate:(NSString *)dateStr withTimeSlot:(NSString *)slotNum {
    if ([dateStr isEqualToString:@"0000-00-00"]) {
        NSString *combinedStr = @"Unavailable";
        return combinedStr;
    }

    NSString *todayStr = [NSString stringWithFormat:@"%@",dateStr];
    NSString *time = [NSString stringWithFormat:@"%@ 00:00:00",todayStr];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *date= [formatter dateFromString:time];
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date]; // Get necessary date components
    
    int monthNum = [components month]; //gives you month
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    NSString *monthName = [[df shortMonthSymbols] objectAtIndex:(monthNum-1)];
    
    [components day]; //gives you day
    [components year];
    
    NSString *selectedTimeSlot = slotNum;
//    if ([slotNum isEqualToString:@"1"]) {
//        selectedTimeSlot = @"8 - 10 AM";
//    }
//    else if ([slotNum isEqualToString:@"2"]) {
//        selectedTimeSlot = @"11 - 1 PM";
//    }
//    else if ([slotNum isEqualToString:@"3"]) {
//        selectedTimeSlot = @"2 - 5 PM";
//    }
//    else if ([slotNum isEqualToString:@"4"]) {
//        selectedTimeSlot = @"6 - 9 PM";
//    }
    
    NSString *combinedStr = [NSString stringWithFormat:@"  %@ %ld %ld %@",monthName,(long)[components day],(long)[components year],selectedTimeSlot];
    
    return combinedStr;
}

-(CAShapeLayer *)createDashedLinesFrom:(CGPoint)startPoint to:(CGPoint)endPoint {
    UIBezierPath *path = [UIBezierPath bezierPath];
    //draw a line
    [path moveToPoint:startPoint]; //add yourStartPoint here
    [path addLineToPoint:endPoint];// add yourEndPoint here
    //[path stroke];
    
    CGFloat dashPattern[] = {4,2}; //make your pattern here
    [path setLineDash:dashPattern count:4 phase:0];
    
    UIColor *fill = [UIColor colorWithRed:0.4914 green:0.4894 blue:0.4935 alpha:1.0];
    
    CAShapeLayer *shapelayer = [CAShapeLayer layer];
    shapelayer.strokeStart = 0.0;
    shapelayer.strokeColor = fill.CGColor;
    shapelayer.lineWidth = 1.0;
    shapelayer.lineJoin = kCALineJoinMiter;
    shapelayer.lineDashPattern = [NSArray arrayWithObjects:[NSNumber numberWithInt:10],[NSNumber numberWithInt:7], nil];
    shapelayer.lineDashPhase = 1.0f;
    shapelayer.path = path.CGPath;
    
    return shapelayer;
}

- (IBAction)backEvent:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*
 {
 "order_id": "206",
 "address_id": "137",
 "product_id": "1",
 "product_name": "Bisleri",
 "image_url": "http://54.172.255.2/static/bisleri.png",
 "amount": "80.00",
 "no_of_cans": "1",
 "time_slot": "3",
 "deliver_date": "2015-08-25",
 "delivered_on": "0000-00-00 00:00:00",
 "order_status": "0",
 "user_rating": "0"
 }
*/


@end
