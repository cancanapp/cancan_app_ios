//
//  main.m
//  WaterMe
//
//  Created by Dev on 07/05/15.
//  Copyright (c) 2015 WaterMe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
