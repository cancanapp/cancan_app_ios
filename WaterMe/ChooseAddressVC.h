//
//  ChooseAddressVC.h
//  WaterMe
//
//  Created by Dev on 15/07/15.
//  Copyright (c) 2015 WaterMe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseAddressVC : UIViewController <UIGestureRecognizerDelegate, UIAlertViewDelegate>

@end
